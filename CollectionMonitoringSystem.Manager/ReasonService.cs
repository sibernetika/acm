﻿using AutoMapper.QueryableExtensions;
using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Manager
{
    public class ReasonService : IDataService<Reason, ReasonModel>
    {
        private ArAgingContext context;
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public ReasonService()
        {
            context = new ArAgingContext();
        }        

        public async Task<Reason> GetEntity(int id)
        {
            return await context.Reason.SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IList<Reason>> GetEntityList()
        {
            return await context.Reason.Include(x => x.Status).AsNoTracking().ToListAsync();
        }

        public async Task<IList<ReasonModel>> GetEntityListJson()
        {
            return await context.Reason.Include(x => x.Status).AsNoTracking().ProjectTo<ReasonModel>().ToListAsync();
        }

        public async Task<int> InsertEntity(Reason entity)
        {
            entity.InsertedBy = "";
            entity.InsertedDate = DateTime.Now;
            context.Reason.Add(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> UpdateEntity(Reason entity)
        {
            Reason reason = await GetEntity(entity.Id);
            reason.Description = entity.Description;
            reason.StatusId = entity.StatusId;
            reason.UpdatedBy = "";
            reason.UpdatedDate = DateTime.Now;
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteEntity(int id)
        {
            int containReason = await context.Payment.AsNoTracking().Where(x => x.ReasonId == id).Select(x => x.ReasonId).FirstOrDefaultAsync();
            if (containReason > 0) return 2;

            Reason reason = await GetEntity(id);
            if (reason == null) return 0;

            context.Reason.Remove(reason);
            return await context.SaveChangesAsync();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
