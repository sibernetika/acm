﻿using AutoMapper.QueryableExtensions;
using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Globalization;

namespace CollectionMonitoringSystem.Manager
{
    public class ArAgingService
    {
        private readonly ArAgingContext _context;
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public int DataLength { get; set; }
        public ArAgingService()
        {
            _context = new ArAgingContext();
        }

        public List<SourceArModel> GetEntityList(int start, int length, out int totalData, out StringBuilder grandTotal, bool groupData = false, string search = "", bool sales = false, string area = "", string businessArea = "", string order = "", string orderDirection = "", ReportFilterModel paramModel = null)
        {
            DataLength = 0;
            totalData = 0;
            grandTotal = new StringBuilder();
            if (groupData)
            {
                try
                {
                    Expression<Func<SourceArGroup, bool>> areaCheck = x => true;
                    if (!string.IsNullOrEmpty(area) && (sales || (!sales && area != "Z"))) areaCheck = x => x.Area == area;
                    Expression<Func<SourceArGroup, bool>> storeCheck = x => true;
                    if (!string.IsNullOrEmpty(businessArea) && (sales || (!sales && area != "Z"))) storeCheck = x => x.BusinessArea == businessArea;

                    Func<SourceArGroup, decimal> orderAr = x => x.TotalOverDue;
                    if (order == "0")
                    {
                        orderAr = x => x.TotalOutstanding;
                        orderDirection = "desc";
                    }

                    #region Filter from advance search
                    Expression<Func<SourceArGroup, bool>> companyCodeCheck = x => true;
                    if (paramModel != null && paramModel.SelectedCompanyCode != null) companyCodeCheck = x => paramModel.SelectedCompanyCode.Contains(x.CompanyCode);
                    Expression<Func<SourceArGroup, bool>> customerCheck = x => true;
                    if (paramModel != null && paramModel.SelectedCustomerGroup != null) customerCheck = x => paramModel.SelectedCustomerGroup.Contains(x.CustomerGroup);
                    Expression<Func<SourceArGroup, bool>> areaChildCheck = x => true;
                    if (paramModel != null && paramModel.SelectedArea != null)
                    {
                        string[] areaChild = _context.BusinessArea.Where(x => paramModel.SelectedArea.Contains(x.AreaCode)).Select(x => x.BusinessAreaCode).ToArray();
                        if (areaChild != null) areaChildCheck = x => areaChild.Contains(x.BusinessArea);
                    }
                    Expression<Func<SourceArGroup, bool>> bareaCheck = x => true;
                    if (paramModel != null && paramModel.SelectedBusinessArea != null) bareaCheck = x => paramModel.SelectedBusinessArea.Contains(x.BusinessArea);

                    Expression<Func<SourceArGroup, bool>> sectorCheck = x => true;
                    if (paramModel != null && paramModel.selectedMarketSector != null)
                    {
                        var customerSector = _context.CustomerMarketSector.Where(x => paramModel.selectedMarketSector.Contains(x.MarketSectorId)).Select(x => x.CustomerNumber).ToList();
                        sectorCheck = x => customerSector.Contains(x.CustomerNumber);
                    }

                    Expression<Func<SourceArGroup, bool>> classCheck = x => true;
                    if (paramModel != null && paramModel.selectedClassification != null)
                    {
                        List<string> custFilter = new List<string>();
                        foreach (var cl in paramModel.selectedClassification)
                        {
                            custFilter.AddRange(_context.CustomerClassification.Where(ccc => ccc.ClassificationId.Contains(cl)).Select(kk => kk.CustomerNumber));
                        }

                        classCheck = x => custFilter.Contains(x.CustomerNumber);
                    }
                    #endregion

                    var filterQuery = orderDirection == "asc" ? _context.SourceArGroup.Where(x => x.CustomerGroup.ToLower() == search || x.CustomerNumber.Contains(search) || x.CustomerName.Contains(search) || x.BusinessArea.Contains(search)).Where(areaCheck).Where(storeCheck).Where(companyCodeCheck).Where(customerCheck).Where(areaChildCheck).Where(bareaCheck).Where(sectorCheck).Where(classCheck).OrderBy(orderAr).ToList()
                                                                : _context.SourceArGroup.Where(x => x.CustomerGroup.ToLower() == search || x.CustomerNumber.Contains(search) || x.CustomerName.Contains(search) || x.BusinessArea.Contains(search)).Where(areaCheck).Where(storeCheck).Where(companyCodeCheck).Where(customerCheck).Where(areaChildCheck).Where(bareaCheck).Where(sectorCheck).Where(classCheck).OrderByDescending(orderAr).ToList();

                    totalData = filterQuery.Count;
                    grandTotal.Append(filterQuery.Sum(x => x.NotYetDue).ToString() + "|" + filterQuery.Sum(x => x.Due).ToString() + "|" + filterQuery.Sum(x => x.NotYetDue + x.Due).ToString() + "|" + filterQuery.Sum(x => x.OverDue30).ToString() + "|" + filterQuery.Sum(x => x.OverDue60).ToString() + "|" + filterQuery.Sum(x => x.OverDue90).ToString() + "|" + filterQuery.Sum(x => x.OverDue120).ToString() + "|" + filterQuery.Sum(x => x.OverDue360).ToString() + "|" +
                             filterQuery.Sum(x => x.OverDueGreaterThan360).ToString() + "|" + filterQuery.Sum(x => x.UnApplied).ToString() + "|" + filterQuery.Sum(x => x.TotalOutstanding).ToString() + "|" + filterQuery.Sum(x => x.TotalOverDue).ToString() + "|" + filterQuery.Sum(x => x.TotalOverDue + x.UnApplied).ToString());

                    var query = from tblAging in filterQuery
                                join tblMarket in _context.CustomerMarketSector on tblAging.CustomerNumber equals tblMarket.CustomerNumber into y
                                join tblClassification in _context.CustomerClassification on tblAging.CustomerNumber equals tblClassification.CustomerNumber into z
                                join tblArea in _context.Area on tblAging.Area equals tblArea.AreaCode into a
                                //join tblBusArea in _context.BusinessArea on tblAging.BusinessArea equals tblBusArea.BusinessAreaCode into ba
                                join tblUpdate in _context.Payment.ToList().Where(qq => qq.IsActive) on new { tblAging.CustomerGroup, tblAging.CustomerNumber, tblAging.Area, tblAging.BusinessArea } equals new { tblUpdate.CustomerGroup, tblUpdate.CustomerNumber, tblUpdate.Area, tblUpdate.BusinessArea } into comment
                                from subMarket in y.DefaultIfEmpty()
                                from subClass in z.DefaultIfEmpty()
                                from subArea in a.DefaultIfEmpty()
                                    //from subBusArea in ba.DefaultIfEmpty()
                                from subComment in comment.DefaultIfEmpty()
                                select new SourceArModel
                                {
                                    CustomerGroup = tblAging.CustomerGroup,
                                    CustomerNumber = tblAging.CustomerNumber,
                                    MarketSector = subMarket?.MarketSector?.Description ?? "",
                                    Classification = subClass?.ClassificationName ?? "",
                                    Area = tblAging.Area,
                                    AreaName = subArea?.AreaName ?? "",
                                    BusinessArea = tblAging.BusinessArea, //subBusArea?.BusinessAreaName ?? tblAging.BusinessArea,
                                    CustomerName = tblAging.CustomerName,
                                    NotYetDue = tblAging.NotYetDue,
                                    Due = tblAging.Due,
                                    Current = (tblAging.NotYetDue + tblAging.Due),
                                    OverDue30 = tblAging.OverDue30,
                                    OverDue60 = tblAging.OverDue60,
                                    OverDue90 = tblAging.OverDue90,
                                    OverDue120 = tblAging.OverDue120,
                                    OverDue360 = tblAging.OverDue360,
                                    OverDueGreaterThan360 = tblAging.OverDueGreaterThan360,
                                    UnApplied = tblAging.UnApplied,
                                    TotalOverDue = tblAging.TotalOverDue,
                                    NetOverdue = tblAging.TotalOverDue + tblAging.UnApplied,
                                    TotalOutstanding = tblAging.TotalOutstanding,
                                    Description = subComment?.Description ?? ""
                                };

                    if (!string.IsNullOrEmpty(order))
                    {
                        if (orderDirection == "asc")
                        {
                            if (order == "1") return query.OrderBy(x => x.CustomerGroup).Skip(start).Take(length).ToList();
                            if (order == "2") return query.OrderBy(x => x.MarketSector).Skip(start).Take(length).ToList();
                            if (order == "3") return query.OrderBy(x => x.Classification).Skip(start).Take(length).ToList();
                            if (order == "4") return query.OrderBy(x => x.CustomerNumber).Skip(start).Take(length).ToList();
                            if (order == "5") return query.OrderBy(x => x.CustomerName).Skip(start).Take(length).ToList();
                            if (order == "6") return query.OrderBy(x => x.AreaName).Skip(start).Take(length).ToList();
                            if (order == "7") return query.OrderBy(x => x.BusinessArea).Skip(start).Take(length).ToList();
                            if (order == "8") return query.OrderBy(x => x.NotYetDue).Skip(start).Take(length).ToList();
                            if (order == "9") return query.OrderBy(x => x.Due).Take(length).ToList();
                            if (order == "10") return query.OrderBy(x => x.Current).Skip(start).Take(length).ToList();
                            if (order == "11") return query.OrderBy(x => x.OverDue30).Skip(start).Take(length).ToList();
                            if (order == "12") return query.OrderBy(x => x.OverDue60).Skip(start).Take(length).ToList();
                            if (order == "13") return query.OrderBy(x => x.OverDue90).Skip(start).Take(length).ToList();
                            if (order == "14") return query.OrderBy(x => x.OverDue120).Skip(start).Take(length).ToList();
                            if (order == "15") return query.OrderBy(x => x.OverDue360).Skip(start).Take(length).ToList();
                            if (order == "16") return query.OrderBy(x => x.OverDueGreaterThan360).Skip(start).Take(length).ToList();
                            if (order == "17") return query.OrderBy(x => x.UnApplied).Skip(start).Take(length).ToList();
                            if (order == "18") return query.OrderBy(x => x.TotalOutstanding).Skip(start).Take(length).ToList();
                            if (order == "19") return query.OrderBy(x => x.TotalOverDue).Skip(start).Take(length).ToList();
                            if (order == "20") return query.OrderBy(x => x.NetOverdue).Skip(start).Take(length).ToList();
                            if (order == "21") return query.OrderBy(x => x.Description).Skip(start).Take(length).ToList();
                        }
                        else
                        {
                            if (order == "1") return query.OrderByDescending(x => x.CustomerGroup).Skip(start).Take(length).ToList();
                            if (order == "2") return query.OrderByDescending(x => x.MarketSector).Skip(start).Take(length).ToList();
                            if (order == "3") return query.OrderByDescending(x => x.Classification).Skip(start).Take(length).ToList();
                            if (order == "4") return query.OrderByDescending(x => x.CustomerNumber).Skip(start).Take(length).ToList();
                            if (order == "5") return query.OrderByDescending(x => x.CustomerName).Skip(start).Take(length).ToList();
                            if (order == "6") return query.OrderByDescending(x => x.AreaName).Skip(start).Take(length).ToList();
                            if (order == "7") return query.OrderByDescending(x => x.BusinessArea).Skip(start).Take(length).ToList();
                            if (order == "8") return query.OrderByDescending(x => x.NotYetDue).Skip(start).Take(length).ToList();
                            if (order == "9") return query.OrderByDescending(x => x.Due).Skip(start).Take(length).ToList();
                            if (order == "10") return query.OrderByDescending(x => x.Current).Skip(start).Take(length).ToList();
                            if (order == "11") return query.OrderByDescending(x => x.OverDue30).Skip(start).Take(length).ToList();
                            if (order == "12") return query.OrderByDescending(x => x.OverDue60).Skip(start).Take(length).ToList();
                            if (order == "13") return query.OrderByDescending(x => x.OverDue90).Skip(start).Take(length).ToList();
                            if (order == "14") return query.OrderByDescending(x => x.OverDue120).Skip(start).Take(length).ToList();
                            if (order == "15") return query.OrderByDescending(x => x.OverDue360).Skip(start).Take(length).ToList();
                            if (order == "16") return query.OrderByDescending(x => x.OverDueGreaterThan360).Skip(start).Take(length).ToList();
                            if (order == "17") return query.OrderByDescending(x => x.UnApplied).Skip(start).Take(length).ToList();
                            if (order == "18") return query.OrderByDescending(x => x.TotalOutstanding).Skip(start).Take(length).ToList();
                            if (order == "19") return query.OrderByDescending(x => x.TotalOverDue).Skip(start).Take(length).ToList();
                            if (order == "20") return query.OrderByDescending(x => x.NetOverdue).Skip(start).Take(length).ToList();
                            if (order == "21") return query.OrderByDescending(x => x.Description).Skip(start).Take(length).ToList();
                        }
                    }

                    return query.Skip(start).Take(length).ToList();
                }
                catch (Exception error)
                {
                    IsError = true;
                    ErrorMessage = error.Message;
                }
                return new List<SourceArModel>();
            }
            return _context.SourceAr.AsNoTracking().ProjectTo<SourceArModel>().ToList();
        }

        public List<DetailByInvoiceModel> GetInvoiceList(int start, int length, out int totalData, string search, string order, string orderDirection, string customerGroup, string customerNumber, string area, string businessArea)
        {
            totalData = 0;

            Func<SourceAr, DateTime?> orderInvoice = x => x.InvoiceDate;

            try
            {
                var filter = orderDirection == "asc" ? _context.SourceAr.Where(x => x.InvoiceNumber.Contains(search)).Where(x => x.CustomerGroup == customerGroup && x.CustomerNumber == customerNumber && x.Area == area && x.BusinessArea == businessArea).OrderBy(orderInvoice).ToList() :
                    _context.SourceAr.Where(x => x.InvoiceNumber.Contains(search)).Where(x => x.CustomerGroup == customerGroup && x.CustomerNumber == customerNumber && x.Area == area && x.BusinessArea == businessArea).OrderByDescending(orderInvoice).ToList();
                totalData = filter.Count;
                var query = filter.GroupBy(x => new { x.InvoiceNumber, x.InvoiceDate }).Select(x =>
                                                        new DetailByInvoiceModel
                                                        {
                                                            InvoiceNumber = string.IsNullOrEmpty(x.Key.InvoiceNumber) ? "-" : x.Key.InvoiceNumber,
                                                            InvoiceDate = x.Key.InvoiceDate?.ToString("dd-MM-yyyy") ?? "",
                                                            NotYetDue = x.Sum(y => y.NotYetDue),
                                                            Due = x.Sum(y => y.Due),
                                                            OverDue30 = x.Sum(y => y.OverDue30),
                                                            OverDue60 = x.Sum(y => y.OverDue60),
                                                            OverDue90 = x.Sum(y => y.OverDue90),
                                                            OverDue120 = x.Sum(y => y.OverDue120),
                                                            OverDue360 = x.Sum(y => y.OverDue360),
                                                            OverDueGreaterThan360 = x.Sum(y => y.OverDueGreaterThan360),
                                                            Current = x.Sum(y => y.NotYetDue + y.Due),
                                                            UnApplied = x.Sum(y => y.UnApplied),
                                                            TotalOutstanding = x.Sum(y => y.TotalOutstanding),
                                                            TotalOverDue = x.Sum(y => y.TotalOverDue),
                                                            NetOverdue = x.Sum(y => y.TotalOverDue + y.UnApplied)
                                                        }).ToList();

                if (!string.IsNullOrEmpty(order))
                {
                    if (orderDirection == "asc")
                    {
                        if (order == "0") return query.OrderBy(x => x.InvoiceNumber).Skip(start).Take(length).ToList();
                        if (order == "2") return query.OrderBy(x => x.NotYetDue).Skip(start).Take(length).ToList();
                        if (order == "3") return query.OrderBy(x => x.Due).Take(length).ToList();
                        if (order == "4") return query.OrderBy(x => x.Current).Skip(start).Take(length).ToList();
                        if (order == "5") return query.OrderBy(x => x.OverDue30).Skip(start).Take(length).ToList();
                        if (order == "6") return query.OrderBy(x => x.OverDue60).Skip(start).Take(length).ToList();
                        if (order == "7") return query.OrderBy(x => x.OverDue90).Skip(start).Take(length).ToList();
                        if (order == "8") return query.OrderBy(x => x.OverDue120).Skip(start).Take(length).ToList();
                        if (order == "9") return query.OrderBy(x => x.OverDue360).Skip(start).Take(length).ToList();
                        if (order == "10") return query.OrderBy(x => x.OverDueGreaterThan360).Skip(start).Take(length).ToList();
                        if (order == "11") return query.OrderBy(x => x.UnApplied).Skip(start).Take(length).ToList();
                        if (order == "12") return query.OrderBy(x => x.TotalOutstanding).Skip(start).Take(length).ToList();
                        if (order == "13") return query.OrderBy(x => x.TotalOverDue).Skip(start).Take(length).ToList();
                        if (order == "14") return query.OrderBy(x => x.NetOverdue).Skip(start).Take(length).ToList();
                    }
                    else
                    {
                        if (order == "0") return query.OrderByDescending(x => x.InvoiceNumber).Skip(start).Take(length).ToList();
                        if (order == "2") return query.OrderByDescending(x => x.NotYetDue).Skip(start).Take(length).ToList();
                        if (order == "3") return query.OrderByDescending(x => x.Due).Take(length).ToList();
                        if (order == "4") return query.OrderByDescending(x => x.Current).Skip(start).Take(length).ToList();
                        if (order == "5") return query.OrderByDescending(x => x.OverDue30).Skip(start).Take(length).ToList();
                        if (order == "6") return query.OrderByDescending(x => x.OverDue60).Skip(start).Take(length).ToList();
                        if (order == "7") return query.OrderByDescending(x => x.OverDue90).Skip(start).Take(length).ToList();
                        if (order == "8") return query.OrderByDescending(x => x.OverDue120).Skip(start).Take(length).ToList();
                        if (order == "9") return query.OrderByDescending(x => x.OverDue360).Skip(start).Take(length).ToList();
                        if (order == "10") return query.OrderByDescending(x => x.OverDueGreaterThan360).Skip(start).Take(length).ToList();
                        if (order == "11") return query.OrderByDescending(x => x.UnApplied).Skip(start).Take(length).ToList();
                        if (order == "12") return query.OrderByDescending(x => x.TotalOutstanding).Skip(start).Take(length).ToList();
                        if (order == "13") return query.OrderByDescending(x => x.TotalOverDue).Skip(start).Take(length).ToList();
                        if (order == "14") return query.OrderByDescending(x => x.NetOverdue).Skip(start).Take(length).ToList();
                    }
                }

                return query.Skip(start).Take(length).ToList();
            }
            catch (Exception)
            {
                return new List<DetailByInvoiceModel>();
            }
        }

        public async Task<List<SourceArModel>> GetEntityListExcel(string search = "", bool sales = false, string area = "", string businessArea = "")
        {
            try
            {
                Expression<Func<SourceArGroup, bool>> areaCheck = x => true;
                if (!string.IsNullOrEmpty(area) && (sales || (!sales && area != "Z"))) areaCheck = x => x.Area == area;
                Expression<Func<SourceArGroup, bool>> storeCheck = x => true;
                if (!string.IsNullOrEmpty(businessArea) && (sales || (!sales && area != "Z"))) storeCheck = x => x.BusinessArea == businessArea;
                Expression<Func<SourceArGroup, bool>> searchCheck = x => true;
                if (!string.IsNullOrEmpty(search)) searchCheck = x => x.CustomerGroup.ToLower().Equals(search.ToLower()) || x.CustomerName.ToLower().Contains(search.ToLower()) || x.CustomerNumber.ToLower().Contains(search.ToLower()) || x.BusinessArea.ToLower().Contains(search.ToLower());

                var filterQuery = _context.SourceArGroup.Where(searchCheck).Where(areaCheck).Where(storeCheck).OrderByDescending(x => x.TotalOverDue).ToListAsync();

                var query = from tblAging in filterQuery.Result
                            join tblMarket in _context.CustomerMarketSector on tblAging.CustomerNumber equals tblMarket.CustomerNumber into y
                            join tblClassification in _context.CustomerClassification on tblAging.CustomerNumber equals tblClassification.CustomerNumber into z
                            join tblArea in _context.Area on tblAging.Area equals tblArea.AreaCode into a
                            join tblUpdate in _context.Payment.ToList().Where(qq => qq.IsActive) on new { tblAging.CustomerGroup, tblAging.CustomerNumber, tblAging.Area, tblAging.BusinessArea } equals new { tblUpdate.CustomerGroup, tblUpdate.CustomerNumber, tblUpdate.Area, tblUpdate.BusinessArea } into comment
                            from subMarket in y.DefaultIfEmpty()
                            from subClass in z.DefaultIfEmpty()
                            from subArea in a.DefaultIfEmpty()
                            from subComment in comment.DefaultIfEmpty()
                            select new SourceArModel
                            {
                                CustomerGroup = tblAging.CustomerGroup,
                                CustomerNumber = tblAging.CustomerNumber,
                                MarketSector = subMarket?.MarketSector?.Description ?? "",
                                Classification = subClass?.ClassificationName ?? "",
                                Area = tblAging.Area,
                                AreaName = subArea?.AreaName ?? "",
                                BusinessArea = tblAging.BusinessArea,
                                CustomerName = tblAging.CustomerName,
                                NotYetDue = tblAging.NotYetDue,
                                Due = tblAging.Due,
                                Current = tblAging.NotYetDue + tblAging.Due,
                                OverDue30 = tblAging.OverDue30,
                                OverDue60 = tblAging.OverDue60,
                                OverDue90 = tblAging.OverDue90,
                                OverDue120 = tblAging.OverDue120,
                                OverDue360 = tblAging.OverDue360,
                                OverDueGreaterThan360 = tblAging.OverDueGreaterThan360,
                                UnApplied = tblAging.UnApplied,
                                TotalOverDue = tblAging.TotalOverDue,
                                TotalOutstanding = tblAging.TotalOutstanding,
                                NetOverdue = (tblAging.TotalOverDue + tblAging.UnApplied),
                                Description = subComment?.Description ?? ""
                            };
                return query.ToList();
            }
            catch (Exception error)
            {
                IsError = true;
                ErrorMessage = error.Message;
            }
            return new List<SourceArModel>();
        }

        public async Task<List<SourceArModel>> GetEntityListDetail(string customerGroup, string customerNumber, string area, string businessArea)
        {
            Area areaDetail = await _context.Area.SingleOrDefaultAsync(x => x.AreaCode == area);
            string areaName = areaDetail?.AreaName ?? "";
            CustomerMarketSector customerSector = await _context.CustomerMarketSector.Include(x => x.MarketSector).SingleOrDefaultAsync(x => x.CustomerNumber == customerNumber);
            string marketSector = customerSector?.MarketSector?.Description ?? "";

            return
                (await
                    _context.SourceAr.AsNoTracking()
                        .Where(
                            x =>
                                x.CustomerGroup == customerGroup && x.CustomerNumber == customerNumber && x.Area == area &&
                                x.BusinessArea == businessArea)
                        .GroupBy(x => new { x.ProfitCenterId })
                        .ToListAsync()).Select(x => new SourceArModel
                        {
                            CustomerGroup = customerGroup,
                            CustomerNumber = customerNumber,
                            Area = area,
                            AreaName = areaName,
                            MarketSector = marketSector,
                            CustomerName = x.Select(y => y.CustomerName).FirstOrDefault(),
                            ProfitCenterName = x.Select(y => y.ProfitCenter.Description).FirstOrDefault(),
                            NotYetDue = x.Sum(y => y.NotYetDue),
                            AmountDocCur = x.Sum(y => y.AmountDocCur ?? 0),
                            BusinessArea = businessArea,
                            Due = x.Sum(y => y.Due),
                            Current = x.Sum(y => y.NotYetDue + y.Due),
                            OverDue30 = x.Sum(y => y.OverDue30),
                            OverDue60 = x.Sum(y => y.OverDue60),
                            OverDue90 = x.Sum(y => y.OverDue90),
                            OverDue120 = x.Sum(y => y.OverDue120),
                            OverDue360 = x.Sum(y => y.OverDue360),
                            OverDueGreaterThan360 = x.Sum(y => y.OverDueGreaterThan360),
                            UnApplied = x.Sum(y => y.UnApplied),
                            TotalOverDue = x.Sum(y => y.TotalOverDue),
                            TotalOutstanding = x.Sum(y => y.TotalOutstanding),
                            Aging = x.Select(y => y.Aging ?? 0).FirstOrDefault()
                        }).ToList<SourceArModel>();
        }

        public async Task<int> GetTotalData(string search)
        {
            return (await _context.SourceArGroup.Where(x => x.CustomerGroup.ToLower() == search || x.CustomerNumber.Contains(search) || x.CustomerName.Contains(search) || x.BusinessArea.Contains(search)).ToListAsync()).Count;
        }

        public async Task<List<FilterModel>> GetCustomerGroup()
        {
            return await _context.SourceAr.AsNoTracking().Select(x => new FilterModel { Value = x.CustomerGroup, Name = x.CustomerGroup }).Distinct().OrderBy(x => x.Value).ToListAsync();
        }

        public async Task<List<FilterModel>> GetCompanyCode()
        {
            return await _context.SourceAr.AsNoTracking().Select(x => new FilterModel { Id = x.CompanyCode, Value = x.CompanyCode.ToString(), Name = x.CompanyCode.ToString() }).Distinct().OrderBy(x => x.Id).ToListAsync();
        }

        public async Task<List<FilterModel>> GetAreaCode()
        {
            return await _context.Area.AsNoTracking().Where(x => x.Show).Select(x => new FilterModel { Id = x.Id, Value = x.AreaCode, Name = x.AreaName }).OrderBy(x => x.Value).ToListAsync();
        }

        public async Task<List<FilterModel>> GetBusinessAreaCode()
        {
            return await _context.BusinessArea.AsNoTracking().Select(x => new FilterModel { Id = x.Id, Value = x.BusinessAreaCode, Name = x.BusinessAreaCode + " - " + x.BusinessAreaName }).OrderBy(x => x.Value).ToListAsync();
        }

        public async Task<List<FilterModel>> GetProfitCenterCode()
        {
            return await _context.ProfitCenter.AsNoTracking().Select(x => new FilterModel { Id = x.Id, Value = x.ProfitCenterCode, Name = x.Description }).OrderBy(x => x.Value).ToListAsync();
        }

        public async Task<List<FilterModel>> GetStatusCode()
        {
            return await _context.ReasonStatus.AsNoTracking().Select(x => new FilterModel { Id = x.Id, Name = x.Description, Value = x.Description }).OrderBy(x => x.Id).ToListAsync();
        }

        public async Task<List<FilterModel>> GetMarketSector()
        {
            return await _context.MarketSector.AsNoTracking().Select(x => new FilterModel { Id = x.Id, Value = x.Id.ToString(), Name = x.Description }).OrderBy(x => x.Id).ToListAsync();
        }

        public async Task<List<FilterModel>> GetClassification()
        {
            return await _context.Classification.AsNoTracking().Select(x => new FilterModel { Id = x.Id, Value = x.Id.ToString(), Name = x.Description }).OrderBy(x => x.Id).ToListAsync();
        }

        public async Task<DateTime?> GetLastUpdateDate()
        {
            return await _context.SourceAr.AsNoTracking().MaxAsync(x => x.InsertedDate);
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
