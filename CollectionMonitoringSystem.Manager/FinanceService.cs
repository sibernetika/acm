﻿using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Manager
{
    public class FinanceService : IDataService<Finance, FinanceModel>
    {
        private ArAgingContext context;
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public FinanceService()
        {
            context = new ArAgingContext();
        }

        public async Task<Finance> GetEntity(int id)
        {
            return await context.Finance.SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IList<Finance>> GetEntityList()
        {
            return await context.Finance.AsNoTracking().ToListAsync();
        }

        public async Task<IList<FinanceModel>> GetEntityListJson()
        {
            return AutoMapper.Mapper.Map<List<FinanceModel>>(await context.Finance.AsNoTracking().ToListAsync());
        }

        public async Task<int> InsertEntity(Finance entity)
        {
            entity.InsertedBy = "";
            entity.InsertedDate = DateTime.Now;
            context.Finance.Add(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> UpdateEntity(Finance entity)
        {
            Finance finance = await GetEntity(entity.Id);
            finance.BusinessArea = entity.BusinessArea;
            finance.Name = entity.Name;
            finance.Email = entity.Email;
            finance.UpdatedBy = entity.UpdatedBy;
            finance.UpdatedDate = DateTime.Now;
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteEntity(int id)
        {
            Finance finance = await GetEntity(id);
            if (finance == null) return 0;

            context.Finance.Remove(finance);
            return await context.SaveChangesAsync();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
