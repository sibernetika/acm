﻿using AutoMapper.QueryableExtensions;
using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using CollectionMonitoringSystem.Context.Enums;
using System.Net;
using System.Web.Script.Serialization;
using System.IO;

namespace CollectionMonitoringSystem.Manager
{
    public class EmailService : IDataService<EmailNotification, EmailNotificationModel>
    {
        private ArAgingContext context;
        private ErrorLogService errorService;
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public EmailService()
        {
            context = new ArAgingContext();
            errorService = new ErrorLogService();
        }  

        public async Task<EmailNotification> GetEntity(int id)
        {
            return await context.EmailNotification.SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IList<EmailNotification>> GetEntityList()
        {
            return await context.EmailNotification.Include(x => x.EmailContent).AsNoTracking().ToListAsync();
        }

        public async Task<IList<EmailNotificationModel>> GetEntityListJson()
        {
            return await context.EmailNotification.Include(x => x.EmailContent).AsNoTracking().ProjectTo<EmailNotificationModel>().OrderBy(x => x.Overdue).ToListAsync();
        }

        public async Task SendEmailBeforeDue()
        {
            List<EmailSendModel> mailData = await GetContactToSendBeforeDue();
            EmailNotificationRecord entity = new EmailNotificationRecord();
            foreach (EmailSendModel model in mailData)
            {
                try
                {
                    if (model.Sales.IsNotified)
                    {
                        MailMessage message = new MailMessage();
                        message.From = new MailAddress("no-reply@trakindo.co.id");

                        //message.To.Add("andi.setiawan@trakindo.co.id");        //  
                        //message.To.Add("hendra.priatna@trakindo.co.id");       //  
                        //message.CC.Add("ict.bpm@trakindo.co.id");              //  -> Hapus setelah UAT 

                        if (string.IsNullOrEmpty(model.Sales.EmailAddress)) continue;
                        message.To.Add(model.Sales.EmailAddress);            //  
                        if (model.Supervisor.IsNotified)                     //  
                            message.CC.Add(model.Supervisor.EmailAddress);   //  -> Uncomment
                        if (model.Manager.IsNotified)                        //  
                            message.Bcc.Add(model.Manager.EmailAddress);     //  
                        message.Subject = model.Subject;
                        string mailBody = model.Body;
                        string summary =
                            "<table style='padding:5px;width:100%;border:1px solid black;text-align: center;'><thead><tr><th>Customer Group</th><th>Customer Number</th><th>Customer Name</th><th>Business Area</th><th>Outstanding</th><th>Due Date</th><th>Invoice Date</th><th>Invoice No.</th><th>Document No.</th></tr></thead><tbody>";
                        foreach (var summ in model.Summary)
                        {
                            summary += "<tr>";
                            summary += "<td>" + summ.CustomerGroup + "</td>";
                            summary += "<td>" + summ.CustomerNumber + "</td>";
                            summary += "<td>" + summ.CustomerName + "</td>";
                            summary += "<td>" + summ.BusinessArea + "</td>";
                            summary += "<td>" + summ.TotalOverdue.ToString("c") + "</td>";
                            summary += "<td>" + summ.DueDate + "</td>";
                            summary += "<td>" + summ.InvoiceDate + "</td>";
                            summary += "<td>" + summ.InvoiceNumber + "</td>";                                                        
                            summary += "<td>" + summ.DocumentNumber + "</td>";
                            summary += "</tr>";
                        }
                        summary += "</tbody></table>";
                        message.Body = mailBody.Replace("<<salesname>>", model.Sales.ContactName)
                            .Replace("<<summary>>", summary);
                        message.IsBodyHtml = true;
                        using (var smtp = new SmtpClient())
                        {
                            bool sendEmailSuccess = false;
                            try
                            {
                                await smtp.SendMailAsync(message);
                                sendEmailSuccess = true;
                            }
                            catch (Exception errorSmtp)
                            {
                                errorService.LogError(errorSmtp);
                            }

                            foreach (var ma in model.Summary)
                            {
                                int result =
                                    context.EmailNotificationRecord.Where(
                                        x => x.CustomerGroup == ma.CustomerGroup && x.Area == ma.Area)
                                        .Select(x => x.Id)
                                        .FirstOrDefault();
                                if (result == 0)
                                {
                                    entity = new EmailNotificationRecord();
                                    entity.CustomerGroup = ma.CustomerGroup;
                                    entity.Area = ma.Area;
                                    entity.IsSent = sendEmailSuccess;
                                    entity.SalesName = model.Sales.ContactName;
                                    entity.Description = sendEmailSuccess ? "" : "Failed to reach SMTP Server";
                                    int insertResult = await InsertRecord(entity);
                                }
                                else
                                {
                                    EmailNotificationRecord entityUpdate =
                                        context.EmailNotificationRecord.Where(
                                            x => x.CustomerGroup == ma.CustomerGroup && x.Area == ma.Area).FirstOrDefault();
                                    if (entityUpdate != null)
                                    {
                                        entityUpdate.IsSent = sendEmailSuccess;
                                        entityUpdate.LastNotified = DateTime.Now;
                                        entityUpdate.Description = sendEmailSuccess ? "" : "Failed to reach SMTP Server";
                                        int updateResult = await context.SaveChangesAsync();
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception error)
                {
                    errorService.LogError(error);

                    foreach (var ma in model.Summary)
                    {
                        int result = context.EmailNotificationRecord.Where(x => x.CustomerGroup == ma.CustomerGroup && x.Area == ma.Area).Select(x => x.Id).FirstOrDefault();
                        if (result == 0)
                        {
                            entity = new EmailNotificationRecord();
                            entity.CustomerGroup = ma.CustomerGroup;
                            entity.Area = ma.Area;
                            entity.IsSent = false;
                            entity.SalesName = model.Sales.ContactName;
                            int insertResult = await InsertRecord(entity);
                        }
                        else
                        {
                            EmailNotificationRecord entityUpdate = context.EmailNotificationRecord.SingleOrDefault(x => x.CustomerGroup == ma.CustomerGroup && x.Area == ma.Area);
                            if (entityUpdate != null)
                            {
                                entityUpdate.IsSent = false;
                                entityUpdate.LastNotified = DateTime.Now;
                                int updateResult = await context.SaveChangesAsync();
                            }
                        }
                    }
                }
            }
        }

        public async Task SendEmail()
        {
            List<EmailSendModel> mailData = await GetContactToSend();
            EmailNotificationRecord entity = new EmailNotificationRecord();
            foreach (EmailSendModel model in mailData)
            {
                try
                {
                    if (model.Sales.IsNotified)
                    {
                        MailMessage message = new MailMessage();
                        message.From = new MailAddress("no-reply@trakindo.co.id");
                        message.To.Add(model.Sales.EmailAddress);
                        if(model.Supervisor.IsNotified)
                            message.CC.Add(model.Supervisor.EmailAddress);
                        if(model.Manager.IsNotified)
                            message.Bcc.Add(model.Manager.EmailAddress);
                        message.Subject = model.Subject;
                        string mailBody = model.Body;
                        string summary =
                            "<table style='padding:5px;width:100%;border:1px solid black;text-align: center;'><thead><tr><th>Customer Group</th><th>Customer Number</th><th>Customer Name</th><th>Overdue</th><th>Detail Url</th></tr></thead><tbody>";
                        foreach (var summ in model.Summary)
                        {
                            summary += "<tr>";
                            summary += "<td>" + summ.CustomerGroup + "</td>";
                            summary += "<td>" + summ.CustomerNumber + "</td>";
                            summary += "<td>" + summ.CustomerName + "</td>";
                            summary += "<td>" + summ.TotalOverdue.ToString("c") + "</td>";
                            summary += "<td><a href='" + ConfigurationManager.AppSettings["DetailUrl"] + "?cg=" +
                                       summ.CustomerGroup + "&cn=" + summ.CustomerNumber + "&a=" + summ.Area + "&ba=" +
                                       summ.BusinessArea + "'>Detail</a></td>";
                            summary += "</tr>";
                        }
                        summary += "</tbody></table>";
                        message.Body = mailBody.Replace("<<salesname>>", model.Sales.ContactName)
                            .Replace("<<summary>>", summary);
                        message.IsBodyHtml = true;
                        using (var smtp = new SmtpClient())
                        {
                            bool sendEmailSuccess = false;
                            try
                            {
                                await smtp.SendMailAsync(message);
                                sendEmailSuccess = true;
                            }
                            catch(Exception errorSmtp)
                            {
                                errorService.LogError(errorSmtp);
                            }

                            foreach (var ma in model.Summary)
                            {
                                int result =
                                    context.EmailNotificationRecord.Where(
                                        x => x.CustomerGroup == ma.CustomerGroup && x.Area == ma.Area)
                                        .Select(x => x.Id)
                                        .FirstOrDefault();
                                if (result == 0)
                                {
                                    entity = new EmailNotificationRecord();
                                    entity.CustomerGroup = ma.CustomerGroup;
                                    entity.Area = ma.Area;
                                    entity.IsSent = sendEmailSuccess;
                                    entity.SalesName = model.Sales.ContactName;
                                    entity.Description = sendEmailSuccess ? "" : "Failed to reach SMTP Server";
                                    int insertResult = await InsertRecord(entity);
                                }
                                else
                                {
                                    EmailNotificationRecord entityUpdate =
                                        context.EmailNotificationRecord.Where(
                                            x => x.CustomerGroup == ma.CustomerGroup && x.Area == ma.Area).FirstOrDefault();
                                    if (entityUpdate != null)
                                    {
                                        entityUpdate.IsSent = sendEmailSuccess;
                                        entityUpdate.LastNotified = DateTime.Now;
                                        entityUpdate.Description = sendEmailSuccess ? "" : "Failed to reach SMTP Server";
                                        int updateResult = await context.SaveChangesAsync();
                                    }
                                }
                            }
                        }                        
                    }
                }
                catch(Exception error)
                {                    
                    errorService.LogError(error);

                    foreach (var ma in model.Summary)
                    {
                        int result = context.EmailNotificationRecord.Where(x => x.CustomerGroup == ma.CustomerGroup && x.Area == ma.Area).Select(x => x.Id).FirstOrDefault();
                        if (result == 0)
                        {
                            entity = new EmailNotificationRecord();
                            entity.CustomerGroup = ma.CustomerGroup;
                            entity.Area = ma.Area;
                            entity.IsSent = false;
                            entity.SalesName = model.Sales.ContactName;
                            int insertResult = await InsertRecord(entity);
                        }
                        else
                        {
                            EmailNotificationRecord entityUpdate = context.EmailNotificationRecord.SingleOrDefault(x => x.CustomerGroup == ma.CustomerGroup && x.Area == ma.Area);
                            if (entityUpdate != null)
                            {
                                entityUpdate.IsSent = false;
                                entityUpdate.LastNotified = DateTime.Now;
                                int updateResult = await context.SaveChangesAsync();
                            }
                        }
                    }
                }
            }
        }

        public async Task<List<EmailSendModel>> GetContactToSend()
        {
            List<EmailSendModel> mailData = new List<EmailSendModel>();

            #region Check configuration of email notification to take data

            List<EmailNotification> ListConfiguration = context.EmailNotification.Where(x => x.Overdue > 0).OrderBy(x => x.Overdue).ToList();
            int configCount = ListConfiguration.Count;
            int configCounter = 1;
            foreach (EmailNotification notification in ListConfiguration)
            {
                EmailContent content = context.EmailContent.SingleOrDefault(x => x.Id == notification.EmailContentId);
                if (content == null) continue;

                #region Get Data from source ar according to its overdue bracket
                List<SourceAr> ListData = new List<SourceAr>();
                if (configCounter == configCount)
                    ListData = context.SourceAr.ToList().Where(x => x.DueDate != null && (DateTime.Now.Date - x.DueDate.Value.Date).Days > notification.Overdue && x.TotalOverDue > 0).ToList();
                else
                    ListData = context.SourceAr.ToList().Where(x => x.DueDate != null &&
                                    (DateTime.Now.Date - x.DueDate.Value.Date).Days > notification.Overdue &&
                                    (DateTime.Now.Date - x.DueDate.Value.Date).Days <
                                    ListConfiguration[configCounter].Overdue && x.TotalOverDue > 0).ToList();
                configCounter++;
                #endregion
                #region Filter already sent data or data that needs to be re-send

                List<EmailNotificationRecord> notifiedRecord = context.EmailNotificationRecord.Where(x => x.IsSent).ToList();

                var queryFilter = from tableAging in ListData
                                  where !notifiedRecord.Select(x => x.CustomerGroup).Contains(tableAging.CustomerGroup)
                                  && !notifiedRecord.Select(x => x.Area).Contains(tableAging.Area)
                                  select tableAging;

                ListData = queryFilter.ToList();

                #endregion
                #region Divide PP and PS

                List<int> profitCenterPP = context.ProfitCenter.Where(x => x.ProfitCenterCategory == ProfitCenterCategory.PrimeProduct).Select(x => x.Id).ToList();
                List<int> profitCenterPS = context.ProfitCenter.Where(x => x.ProfitCenterCategory == ProfitCenterCategory.ProductSupport).Select(x => x.Id).ToList();
                List<SourceAr> ListDataPP = ListData.Where(x => profitCenterPP.Contains(x.ProfitCenterId)).ToList();
                List<SourceAr> ListDataPS = ListData.Where(x => profitCenterPS.Contains(x.ProfitCenterId)).ToList();

                #endregion
                #region Proses Data PP

                List<SalesMasterPP> tblSalesPP = context.SalesMasterPP.ToList();
                var dataPP = from tblPP in ListDataPP
                             join tblSlsPP in tblSalesPP on tblPP.DocumentNumber equals tblSlsPP.DocumentNumber into y
                             from dataSales in y.DefaultIfEmpty()
                             select new
                             {
                                 Sales = dataSales?.SalesName ?? "",
                                 SalesXupj = dataSales?.Xupj ?? "",
                                 SalesSuperior1 = dataSales?.SuperiorXupj1 ?? "",
                                 NotifySuperior1 = notification.NotifyToSupervisor,
                                 SalesSuperior2 = dataSales?.SuperiorXupj2 ?? "",
                                 NotifySuperior2 = notification.NotifyToManager,
                                 CustomerGroup = tblPP.CustomerGroup,
                                 Area = tblPP.Area,
                                 BusinessArea = tblPP.BusinessArea,
                                 CustomerName = tblPP.CustomerName,
                                 CustomerNumber = tblPP.CustomerNumber,
                                 TotalOverDue = tblPP.TotalOverDue
                             };

                var groupedPP = from dtPP in dataPP.ToList()
                                group dtPP by new { dtPP.Sales, dtPP.CustomerGroup, dtPP.CustomerNumber, dtPP.CustomerName, dtPP.Area, dtPP.BusinessArea } into dtGrpPP
                                select new
                                {
                                    Sales = dtGrpPP.Key.Sales,
                                    SalesXupj = dtGrpPP.Select(x => x.SalesXupj).FirstOrDefault(),
                                    SalesSuperior1 = dtGrpPP.Select(x => x.SalesSuperior1).FirstOrDefault(),
                                    NotifySuperior1 = dtGrpPP.Select(x => x.NotifySuperior1).FirstOrDefault(),
                                    SalesSuperior2 = dtGrpPP.Select(x => x.SalesSuperior2).FirstOrDefault(),
                                    NotifySuperior2 = dtGrpPP.Select(x => x.NotifySuperior2).FirstOrDefault(),
                                    CustomerGroup = dtGrpPP.Key.CustomerGroup,
                                    Area = dtGrpPP.Key.Area,
                                    BusinessArea = dtGrpPP.Key.BusinessArea,
                                    CustomerName = dtGrpPP.Key.CustomerName,
                                    CustomerNumber = dtGrpPP.Key.CustomerNumber,
                                    TotalOverDue = dtGrpPP.Sum(x => x.TotalOverDue)
                                };
                string salesTemp = "";
                EmailSendModel model = new EmailSendModel();
                foreach (var dataLoopPP in groupedPP.OrderBy(x => x.Sales).ToList())
                {
                    if (salesTemp != dataLoopPP.Sales)
                    {
                        model = new EmailSendModel();
                        EmailContactModel salesContact = new EmailContactModel();
                        salesContact.ContactName = dataLoopPP.Sales;
                        salesContact.UserId = dataLoopPP.SalesXupj;
                        salesContact.EmailAddress = GetUserProfileEmailById(dataLoopPP.SalesXupj);
                        model.Sales = salesContact;
                        model.Sales.IsNotified = notification.NotifyToSales;
                        model.Body = content.Content.Replace("&lt;", "<").Replace("&gt;", ">");
                        model.Subject = content.Subject;

                        if (notification.NotifyToSupervisor)
                        {
                            EmailContactModel supervisorContact = new EmailContactModel();
                            supervisorContact.EmailAddress = GetUserProfileEmailById(dataLoopPP.SalesSuperior1);
                            model.Supervisor = supervisorContact;
                            model.Supervisor.IsNotified = notification.NotifyToSupervisor;
                        }

                        if (notification.NotifyToManager)
                        {
                            EmailContactModel managerContact = new EmailContactModel();
                            managerContact.EmailAddress = GetUserProfileEmailById(dataLoopPP.SalesSuperior2);
                            model.Manager = managerContact;
                            model.Manager.IsNotified = notification.NotifyToManager;
                        }

                        if (notification.NotifyToFinance)
                        {
                            EmailContactModel managerContact = new EmailContactModel();
                            managerContact.EmailAddress = (await context.Finance?.FirstOrDefaultAsync(x => x.BusinessArea == dataLoopPP.BusinessArea && x.PositionType == PositionType.Finance))?.Email ?? "";
                            model.Finance = managerContact;
                            model.Finance.IsNotified = notification.NotifyToFinance;
                        }

                        if (notification.NotifyToGmSales)
                        {
                            EmailContactModel managerContact = new EmailContactModel();
                            managerContact.EmailAddress = (await context.Finance?.FirstOrDefaultAsync(x => x.BusinessArea == dataLoopPP.BusinessArea && x.PositionType == PositionType.GMSales))?.Email ?? "";
                            model.GmSales = managerContact;
                            model.GmSales.IsNotified = notification.NotifyToGmSales;
                        }

                        if (notification.NotifyToChief)
                        {
                            EmailContactModel managerContact = new EmailContactModel();
                            managerContact.EmailAddress = (await context.Finance?.FirstOrDefaultAsync(x => x.BusinessArea == dataLoopPP.BusinessArea && x.PositionType == PositionType.Chief))?.Email ?? "";
                            model.Chief = managerContact;
                            model.Chief.IsNotified = notification.NotifyToChief;
                        }

                        List<EmailSummaryModel> ListDataSummary = new List<EmailSummaryModel>();
                        foreach (var summary in groupedPP.Where(x => x.Sales == dataLoopPP.Sales).ToList())
                        {
                            EmailSummaryModel summaryModel = new EmailSummaryModel();
                            summaryModel.CustomerGroup = summary.CustomerGroup;
                            summaryModel.Area = summary.Area;
                            summaryModel.BusinessArea = summary.BusinessArea;
                            summaryModel.CustomerName = summary.CustomerName;
                            summaryModel.CustomerNumber = summary.CustomerNumber;
                            summaryModel.TotalOverdue = summary.TotalOverDue;
                            ListDataSummary.Add(summaryModel);
                        }
                        model.Summary = ListDataSummary;
                        mailData.Add(model);
                        salesTemp = dataLoopPP.Sales;
                    }
                }
                #endregion
                #region Proses Data PS

                List<SalesMasterPS> tblSalesPS = context.SalesMasterPS.ToList();
                var dataPS = from tblPS in ListDataPS
                             join tblSlsPS in tblSalesPS on tblPS.CustomerNumber equals tblSlsPS.CustomerNumber into y
                             from dataSales in y.DefaultIfEmpty()
                             select new
                             {
                                 Sales = dataSales?.SalesName ?? "",
                                 SalesXupj = dataSales?.Xupj ?? "",
                                 SalesSuperior1 = dataSales?.SuperiorXupj1 ?? "",
                                 NotifySuperior1 = notification.NotifyToSupervisor,
                                 SalesSuperior2 = dataSales?.SuperiorXupj2 ?? "",
                                 NotifySuperior2 = notification.NotifyToManager,
                                 CustomerGroup = tblPS.CustomerGroup,
                                 Area = tblPS.Area,
                                 BusinessArea = tblPS.BusinessArea,
                                 CustomerName = tblPS.CustomerName,
                                 CustomerNumber = tblPS.CustomerNumber,
                                 TotalOverDue = tblPS.TotalOverDue
                             };
                var groupedPS = from dtPS in dataPS.ToList()
                                group dtPS by new { dtPS.Sales, dtPS.CustomerGroup, dtPS.CustomerNumber, dtPS.CustomerName, dtPS.Area, dtPS.BusinessArea } into dtGrpPS
                                select new
                                {
                                    Sales = dtGrpPS.Key.Sales,
                                    SalesXupj = dtGrpPS.Select(x => x.SalesXupj).FirstOrDefault(),
                                    SalesSuperior1 = dtGrpPS.Select(x => x.SalesSuperior1).FirstOrDefault(),
                                    NotifySuperior1 = dtGrpPS.Select(x => x.NotifySuperior1).FirstOrDefault(),
                                    SalesSuperior2 = dtGrpPS.Select(x => x.SalesSuperior2).FirstOrDefault(),
                                    NotifySuperior2 = dtGrpPS.Select(x => x.NotifySuperior2).FirstOrDefault(),
                                    CustomerGroup = dtGrpPS.Key.CustomerGroup,
                                    Area = dtGrpPS.Key.Area,
                                    BusinessArea = dtGrpPS.Key.BusinessArea,
                                    CustomerName = dtGrpPS.Key.CustomerName,
                                    CustomerNumber = dtGrpPS.Key.CustomerNumber,
                                    TotalOverDue = dtGrpPS.Sum(x => x.TotalOverDue)
                                };
                string salesTempPS = "";
                EmailSendModel modelPS = new EmailSendModel();
                foreach (var dataLoopPS in groupedPS.OrderBy(x => x.Sales).ToList())
                {
                    if (salesTempPS != dataLoopPS.Sales)
                    {
                        modelPS = new EmailSendModel();
                        EmailContactModel salesContact = new EmailContactModel();
                        salesContact.ContactName = dataLoopPS.Sales;
                        salesContact.UserId = dataLoopPS.SalesXupj;
                        salesContact.EmailAddress = GetUserProfileEmailById(dataLoopPS.SalesXupj);
                        modelPS.Sales = salesContact;
                        modelPS.Sales.IsNotified = notification.NotifyToSales;
                        modelPS.Body = content.Content.Replace("&lt;", "<").Replace("&gt;", ">");
                        modelPS.Subject = content.Subject;

                        if (notification.NotifyToSupervisor)
                        {
                            EmailContactModel supervisorContact = new EmailContactModel();
                            supervisorContact.EmailAddress = GetUserProfileEmailById(dataLoopPS.SalesSuperior1);
                            modelPS.Supervisor = supervisorContact;
                            modelPS.Supervisor.IsNotified = notification.NotifyToSupervisor;
                        }

                        if (notification.NotifyToManager)
                        {
                            EmailContactModel managerContact = new EmailContactModel();
                            managerContact.EmailAddress = GetUserProfileEmailById(dataLoopPS.SalesSuperior2);
                            modelPS.Manager = managerContact;
                            modelPS.Manager.IsNotified = notification.NotifyToManager;
                        }

                        if (notification.NotifyToFinance)
                        {
                            EmailContactModel managerContact = new EmailContactModel();
                            managerContact.EmailAddress = (await context.Finance?.FirstOrDefaultAsync(x => x.BusinessArea == dataLoopPS.BusinessArea && x.PositionType == PositionType.Finance))?.Email ?? "";
                            model.Finance = managerContact;
                            model.Finance.IsNotified = notification.NotifyToFinance;
                        }

                        if (notification.NotifyToGmSales)
                        {
                            EmailContactModel managerContact = new EmailContactModel();
                            managerContact.EmailAddress = (await context.Finance?.FirstOrDefaultAsync(x => x.BusinessArea == dataLoopPS.BusinessArea && x.PositionType == PositionType.GMSales))?.Email ?? "";
                            model.GmSales = managerContact;
                            model.GmSales.IsNotified = notification.NotifyToGmSales;
                        }

                        if (notification.NotifyToChief)
                        {
                            EmailContactModel managerContact = new EmailContactModel();
                            managerContact.EmailAddress = (await context.Finance?.FirstOrDefaultAsync(x => x.BusinessArea == dataLoopPS.BusinessArea && x.PositionType == PositionType.Chief))?.Email ?? "";
                            model.Chief = managerContact;
                            model.Chief.IsNotified = notification.NotifyToChief;
                        }

                        List<EmailSummaryModel> ListDataSummary = new List<EmailSummaryModel>();
                        foreach (var summary in groupedPS.Where(x => x.Sales == dataLoopPS.Sales).ToList())
                        {
                            EmailSummaryModel summaryModel = new EmailSummaryModel();
                            summaryModel.CustomerGroup = summary.CustomerGroup;
                            summaryModel.Area = summary.Area;
                            summaryModel.BusinessArea = summary.BusinessArea;
                            summaryModel.CustomerName = summary.CustomerName;
                            summaryModel.CustomerNumber = summary.CustomerNumber;
                            summaryModel.TotalOverdue = summary.TotalOverDue;
                            ListDataSummary.Add(summaryModel);
                        }
                        modelPS.Summary = ListDataSummary;
                        mailData.Add(modelPS);
                        salesTempPS = dataLoopPS.Sales;
                    }
                }
                #endregion
            }

            #endregion
            
            return mailData;
        }

        public async Task<List<EmailSendModel>> GetContactToSendBeforeDue()
        {
            List<EmailSendModel> mailData = new List<EmailSendModel>();

            #region Check configuration of email notification before due to take data

            List<EmailNotification> ListConfiguration = context.EmailNotification.Where(x => x.Overdue < 0).OrderBy(x => x.Overdue).ToList();
            
            foreach (EmailNotification notification in ListConfiguration)
            {
                EmailContent content = context.EmailContent.SingleOrDefault(x => x.Id == notification.EmailContentId);
                if (content == null) continue;

                #region Get Data from source ar according to its beforedue bracket
                List<SourceAr> ListData = new List<SourceAr>();
                ListData = context.SourceAr.ToList().Where(x => x.DueDate != null && (x.DueDate.Value.Date - DateTime.Now.Date).Days == (notification.Overdue * -1) && x.TotalOutstanding > 0).ToList();                
                #endregion                
                #region Divide PP and PS

                List<int> profitCenterPP = context.ProfitCenter.Where(x => x.ProfitCenterCategory == ProfitCenterCategory.PrimeProduct).Select(x => x.Id).ToList();
                List<int> profitCenterPS = context.ProfitCenter.Where(x => x.ProfitCenterCategory == ProfitCenterCategory.ProductSupport).Select(x => x.Id).ToList();
                List<SourceAr> ListDataPP = ListData.Where(x => profitCenterPP.Contains(x.ProfitCenterId)).ToList();
                List<SourceAr> ListDataPS = ListData.Where(x => profitCenterPS.Contains(x.ProfitCenterId)).ToList();

                #endregion
                #region Proses Data PP

                List<SalesMasterPP> tblSalesPP = context.SalesMasterPP.ToList();
                var dataPP = from tblPP in ListDataPP
                             join tblSlsPP in tblSalesPP on tblPP.DocumentNumber equals tblSlsPP.DocumentNumber into y
                             from dataSales in y.DefaultIfEmpty()
                             select new
                             {
                                 Sales = dataSales?.SalesName ?? "",
                                 SalesXupj = dataSales?.Xupj ?? "",
                                 SalesSuperior1 = dataSales?.SuperiorXupj1 ?? "",
                                 NotifySuperior1 = notification.NotifyToSupervisor,
                                 SalesSuperior2 = dataSales?.SuperiorXupj2 ?? "",
                                 NotifyFinance = notification.NotifyToFinance,
                                 CustomerGroup = tblPP.CustomerGroup,
                                 Area = tblPP.Area,
                                 BusinessArea = tblPP.BusinessArea,
                                 CustomerName = tblPP.CustomerName,
                                 CustomerNumber = tblPP.CustomerNumber,
                                 TotalOutstanding = tblPP.TotalOutstanding,
                                 InvoiceNumber = tblPP.InvoiceNumber,
                                 InvoiceDate = tblPP.InvoiceDate,
                                 DueDate = tblPP.DueDate,
                                 DocumentNumber = tblPP.DocumentNumber
                             };

                var groupedPP = from dtPP in dataPP.ToList()
                                group dtPP by new { dtPP.Sales, dtPP.CustomerGroup, dtPP.CustomerNumber, dtPP.CustomerName, dtPP.Area, dtPP.BusinessArea } into dtGrpPP
                                select new
                                {
                                    Sales = dtGrpPP.Key.Sales,
                                    SalesXupj = dtGrpPP.Select(x => x.SalesXupj).FirstOrDefault() ?? "",
                                    SalesSuperior1 = dtGrpPP.Select(x => x.SalesSuperior1).FirstOrDefault() ?? "",
                                    NotifySuperior1 = dtGrpPP.Select(x => x.NotifySuperior1).FirstOrDefault(),
                                    SalesSuperior2 = dtGrpPP.Select(x => x.SalesSuperior2).FirstOrDefault() ?? "",
                                    NotifyFinance = dtGrpPP.Select(x => x.NotifyFinance).FirstOrDefault(),
                                    CustomerGroup = dtGrpPP.Key.CustomerGroup,
                                    Area = dtGrpPP.Key.Area,
                                    BusinessArea = dtGrpPP.Key.BusinessArea,
                                    CustomerName = dtGrpPP.Key.CustomerName,
                                    CustomerNumber = dtGrpPP.Key.CustomerNumber,
                                    TotalOutstanding = dtGrpPP.Sum(x => x.TotalOutstanding),
                                    InvoiceNumber = dtGrpPP.Select(x => x.InvoiceNumber).FirstOrDefault() ?? "",
                                    InvoiceDate = dtGrpPP.Select(x => x.InvoiceDate).FirstOrDefault()?.ToString("dd MMM yyyy") ?? "",
                                    DueDate = dtGrpPP.Select(x => x.DueDate).FirstOrDefault()?.ToString("dd MMM yyyy") ?? "",
                                    DocumentNumber = dtGrpPP.Select(x => x.DocumentNumber).FirstOrDefault() ?? ""
                                };
                string salesTemp = "";
                EmailSendModel model = new EmailSendModel();
                foreach (var dataLoopPP in groupedPP.OrderBy(x => x.Sales).ToList())
                {
                    if (salesTemp != dataLoopPP.Sales)
                    {
                        model = new EmailSendModel();
                        EmailContactModel salesContact = new EmailContactModel();
                        salesContact.ContactName = dataLoopPP.Sales;
                        salesContact.UserId = dataLoopPP.SalesXupj;
                        salesContact.EmailAddress = GetUserProfileEmailById(dataLoopPP.SalesXupj);
                        model.Sales = salesContact;
                        model.Sales.IsNotified = notification.NotifyToSales;
                        model.Body = content.Content.Replace("&lt;", "<").Replace("&gt;", ">");
                        model.Subject = content.Subject;

                        if (notification.NotifyToSupervisor)
                        {
                            EmailContactModel supervisorContact = new EmailContactModel();
                            supervisorContact.EmailAddress = GetUserProfileEmailById(dataLoopPP.SalesSuperior1);
                            model.Supervisor = supervisorContact;
                            model.Supervisor.IsNotified = notification.NotifyToSupervisor;
                        }

                        if (notification.NotifyToFinance)
                        {
                            EmailContactModel managerContact = new EmailContactModel();
                            managerContact.EmailAddress = (await context.Finance?.FirstOrDefaultAsync(x => x.BusinessArea == dataLoopPP.BusinessArea && x.PositionType == PositionType.Finance))?.Email ?? "";
                            model.Finance = managerContact;
                            model.Finance.IsNotified = notification.NotifyToFinance;
                        }

                        if (notification.NotifyToGmSales)
                        {
                            EmailContactModel managerContact = new EmailContactModel();
                            managerContact.EmailAddress = (await context.Finance?.FirstOrDefaultAsync(x => x.BusinessArea == dataLoopPP.BusinessArea && x.PositionType == PositionType.GMSales))?.Email ?? "";
                            model.GmSales = managerContact;
                            model.GmSales.IsNotified = notification.NotifyToGmSales;
                        }

                        if (notification.NotifyToChief)
                        {
                            EmailContactModel managerContact = new EmailContactModel();
                            managerContact.EmailAddress = (await context.Finance?.FirstOrDefaultAsync(x => x.BusinessArea == dataLoopPP.BusinessArea && x.PositionType == PositionType.Chief))?.Email ?? "";
                            model.Chief = managerContact;
                            model.Chief.IsNotified = notification.NotifyToChief;
                        }

                        List<EmailSummaryModel> ListDataSummary = new List<EmailSummaryModel>();
                        foreach (var summary in groupedPP.Where(x => x.Sales == dataLoopPP.Sales).ToList())
                        {
                            EmailSummaryModel summaryModel = new EmailSummaryModel();
                            summaryModel.CustomerGroup = summary.CustomerGroup;
                            summaryModel.Area = summary.Area;
                            summaryModel.BusinessArea = summary.BusinessArea;
                            summaryModel.CustomerName = summary.CustomerName;
                            summaryModel.CustomerNumber = summary.CustomerNumber;
                            summaryModel.TotalOverdue = summary.TotalOutstanding;
                            summaryModel.InvoiceNumber = summary.InvoiceNumber;
                            summaryModel.InvoiceDate = summary.InvoiceDate;
                            summaryModel.DueDate = summary.DueDate;
                            summaryModel.DocumentNumber = summary.DocumentNumber;
                            ListDataSummary.Add(summaryModel);
                        }
                        model.Summary = ListDataSummary;
                        mailData.Add(model);
                        salesTemp = dataLoopPP.Sales;

                        break; // -> Hapus setelah UAT
                    }
                }
                #endregion
                #region Proses Data PS

                List<SalesMasterPS> tblSalesPS = context.SalesMasterPS.ToList();
                var dataPS = from tblPS in ListDataPS
                             //join tblSlsPS in tblSalesPS on tblPS.CustomerNumber equals tblSlsPS.CustomerNumber into y
                             //from dataSales in y.DefaultIfEmpty()
                             select new
                             {
                                 Sales = "Hendra Priatna",//dataSales?.SalesName ?? "",
                                 SalesXupj = "",//dataSales?.Xupj ?? "",
                                 SalesSuperior1 = "",//dataSales?.SuperiorXupj1 ?? "",
                                 NotifySuperior1 = notification.NotifyToSupervisor,
                                 SalesSuperior2 = "",//dataSales?.SuperiorXupj2 ?? "",
                                 NotifyFinance = notification.NotifyToFinance,
                                 CustomerGroup = tblPS.CustomerGroup,
                                 Area = tblPS.Area,
                                 BusinessArea = tblPS.BusinessArea,
                                 CustomerName = tblPS.CustomerName,
                                 CustomerNumber = tblPS.CustomerNumber,
                                 TotalOutstanding = tblPS.TotalOutstanding,
                                 InvoiceNumber = tblPS.InvoiceNumber,
                                 InvoiceDate = tblPS.InvoiceDate,
                                 Duedate = tblPS.DueDate,
                                 DocumentNumber = tblPS.DocumentNumber
                             };
                var groupedPS = from dtPS in dataPS.ToList()
                                group dtPS by new { dtPS.Sales, dtPS.CustomerGroup, dtPS.CustomerNumber, dtPS.CustomerName, dtPS.Area, dtPS.BusinessArea } into dtGrpPS
                                select new
                                {
                                    Sales = dtGrpPS.Key.Sales,
                                    SalesXupj = dtGrpPS.Select(x => x.SalesXupj).FirstOrDefault() ?? "",
                                    SalesSuperior1 = dtGrpPS.Select(x => x.SalesSuperior1).FirstOrDefault() ?? "",
                                    NotifySuperior1 = dtGrpPS.Select(x => x.NotifySuperior1).FirstOrDefault(),
                                    SalesSuperior2 = dtGrpPS.Select(x => x.SalesSuperior2).FirstOrDefault() ?? "",
                                    NotifyFinance = dtGrpPS.Select(x => x.NotifyFinance).FirstOrDefault(),
                                    CustomerGroup = dtGrpPS.Key.CustomerGroup,
                                    Area = dtGrpPS.Key.Area,
                                    BusinessArea = dtGrpPS.Key.BusinessArea,
                                    CustomerName = dtGrpPS.Key.CustomerName,
                                    CustomerNumber = dtGrpPS.Key.CustomerNumber,
                                    TotalOutstanding = dtGrpPS.Sum(x => x.TotalOutstanding),
                                    InvoiceNumber = dtGrpPS.Select(x => x.InvoiceNumber).FirstOrDefault() ?? "",
                                    InvoiceDate = dtGrpPS.Select(x => x.InvoiceDate).FirstOrDefault()?.ToString("dd MMM yyyy") ?? "",
                                    DueDate = dtGrpPS.Select(x => x.Duedate).FirstOrDefault()?.ToString("dd MMM yyyy") ?? "",
                                    DocumentNumber = dtGrpPS.Select(x => x.DocumentNumber).FirstOrDefault() ?? ""
                                };
                string salesTempPS = "";
                EmailSendModel modelPS = new EmailSendModel();
                foreach (var dataLoopPS in groupedPS.OrderBy(x => x.Sales).ToList())
                {
                    if (salesTempPS != dataLoopPS.Sales)
                    {
                        modelPS = new EmailSendModel();
                        EmailContactModel salesContact = new EmailContactModel();
                        salesContact.ContactName = dataLoopPS.Sales;
                        salesContact.UserId = dataLoopPS.SalesXupj;
                        salesContact.EmailAddress = GetUserProfileEmailById(dataLoopPS.SalesXupj);
                        modelPS.Sales = salesContact;
                        modelPS.Sales.IsNotified = notification.NotifyToSales;
                        modelPS.Body = content.Content.Replace("&lt;", "<").Replace("&gt;", ">");
                        modelPS.Subject = content.Subject;

                        if (notification.NotifyToSupervisor)
                        {
                            EmailContactModel supervisorContact = new EmailContactModel();
                            supervisorContact.EmailAddress = GetUserProfileEmailById(dataLoopPS.SalesSuperior1);
                            model.Supervisor = supervisorContact;
                            model.Supervisor.IsNotified = notification.NotifyToSupervisor;
                        }

                        if (notification.NotifyToFinance)
                        {
                            EmailContactModel managerContact = new EmailContactModel();
                            managerContact.EmailAddress = (await context.Finance?.FirstOrDefaultAsync(x => x.BusinessArea == dataLoopPS.BusinessArea && x.PositionType == PositionType.Finance))?.Email ?? "";
                            model.Finance = managerContact;
                            model.Finance.IsNotified = notification.NotifyToFinance;
                        }

                        if (notification.NotifyToGmSales)
                        {
                            EmailContactModel managerContact = new EmailContactModel();
                            managerContact.EmailAddress = (await context.Finance?.FirstOrDefaultAsync(x => x.BusinessArea == dataLoopPS.BusinessArea && x.PositionType == PositionType.GMSales))?.Email ?? "";
                            model.GmSales = managerContact;
                            model.GmSales.IsNotified = notification.NotifyToGmSales;
                        }

                        if (notification.NotifyToChief)
                        {
                            EmailContactModel managerContact = new EmailContactModel();
                            managerContact.EmailAddress = (await context.Finance?.FirstOrDefaultAsync(x => x.BusinessArea == dataLoopPS.BusinessArea && x.PositionType == PositionType.Chief))?.Email ?? "";
                            model.Chief = managerContact;
                            model.Chief.IsNotified = notification.NotifyToChief;
                        }

                        List<EmailSummaryModel> ListDataSummary = new List<EmailSummaryModel>();
                        foreach (var summary in groupedPS.Where(x => x.Sales == dataLoopPS.Sales).ToList())
                        {
                            EmailSummaryModel summaryModel = new EmailSummaryModel();
                            summaryModel.CustomerGroup = summary.CustomerGroup;
                            summaryModel.Area = summary.Area;
                            summaryModel.BusinessArea = summary.BusinessArea;
                            summaryModel.CustomerName = summary.CustomerName;
                            summaryModel.CustomerNumber = summary.CustomerNumber;
                            summaryModel.TotalOverdue = summary.TotalOutstanding;
                            summaryModel.InvoiceNumber = summary.InvoiceNumber;
                            summaryModel.InvoiceDate = summary.InvoiceDate;
                            summaryModel.DueDate = summary.DueDate;
                            summaryModel.DocumentNumber = summary.DocumentNumber;
                            ListDataSummary.Add(summaryModel);
                        }
                        modelPS.Summary = ListDataSummary;
                        mailData.Add(modelPS);
                        salesTempPS = dataLoopPS.Sales;

                        break; // -> Hapus setelah UAT
                    }
                }
                #endregion
            }

            #endregion

            return mailData;
        }

        public async Task<int> InsertEntity(EmailNotification entity)
        {
            entity.InsertedDate = DateTime.Now;
            context.EmailNotification.Add(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> InsertRecord(EmailNotificationRecord entity)
        {
            entity.LastNotified = DateTime.Now;
            context.EmailNotificationRecord.Add(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> UpdateEntity(EmailNotification entity)
        {
            EmailNotification email = await GetEntity(entity.Id);
            email.EmailContentId = entity.EmailContentId;
            email.NotifyToSales = entity.NotifyToSales;
            email.NotifyToSupervisor = entity.NotifyToSupervisor;
            email.NotifyToManager = entity.NotifyToManager;
            email.Overdue = entity.Overdue;
            email.ResendEvery = entity.ResendEvery;
            email.UpdatedBy = entity.UpdatedBy;
            email.UpdatedDate = DateTime.Now;
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteEntity(int id)
        {
            EmailNotification email = await GetEntity(id);
            if (email == null) return 0;

            context.EmailNotification.Remove(email);
            return await context.SaveChangesAsync();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public string GetUserProfileEmailById(string id)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["UserPortal"]);

                ParameterModel param = new ParameterModel();
                param.userID = new string[] { id };
                var json = new JavaScriptSerializer().Serialize(param);
                var data = Encoding.ASCII.GetBytes(json);

                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                var email = "";
                if (responseString.Contains("Email") && responseString.Contains("trakindo.co.id"))
                    email = responseString.Substring(responseString.IndexOf("Email") + 10,
                     responseString.IndexOf(".co.id") - 4 - responseString.IndexOf("Email"));

                return email;
            }
            catch (Exception error)
            {
                ErrorLogService errorService = new ErrorLogService();
                errorService.LogError(error);
                errorService.Dispose();
                IsError = true;
                return "Failed establishing connection to portal";
            }
        }
    }
}
