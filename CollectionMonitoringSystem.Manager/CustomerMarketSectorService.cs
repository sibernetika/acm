﻿using AutoMapper.QueryableExtensions;
using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Manager
{
    public class CustomerMarketSectorService : IDataService<CustomerMarketSector, CustomerMarketSectorModel>
    {
        private ArAgingContext context;
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public CustomerMarketSectorService()
        {
            context = new ArAgingContext();
        }

        public async Task<CustomerMarketSector> GetEntity(int id)
        {
            return await context.CustomerMarketSector.SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IList<CustomerMarketSector>> GetEntityList()
        {
            return await context.CustomerMarketSector.Include(x => x.MarketSector).AsNoTracking().ToListAsync();
        }

        public async Task<IList<CustomerMarketSectorModel>> GetEntityListJson()
        {
            return await context.CustomerMarketSector.Include(x => x.MarketSector).AsNoTracking().ProjectTo<CustomerMarketSectorModel>().ToListAsync();
        }

        public async Task<List<CustomerMarketSectorModel>> GetEntityListJson(string search)
        {
            return await context.CustomerMarketSector.Include(x => x.MarketSector).Where(x => x.CustomerNumber.Contains(search) || x.CustomerName.Contains(search)).AsNoTracking().ProjectTo<CustomerMarketSectorModel>().ToListAsync();
        }

        public async Task<int> InsertEntity(CustomerMarketSector entity)
        {
            entity.InsertedBy = "";
            entity.InsertedDate = DateTime.Now;
            context.CustomerMarketSector.Add(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> UpdateEntity(CustomerMarketSector entity)
        {
            CustomerMarketSector customerMarketSector = await GetEntity(entity.Id);
            customerMarketSector.CustomerNumber = entity.CustomerNumber;
            customerMarketSector.CustomerName = entity.CustomerName;
            customerMarketSector.MarketSectorId = entity.MarketSectorId;
            customerMarketSector.UpdatedBy = "";
            customerMarketSector.UpdatedDate = DateTime.Now;
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteEntity(int id)
        {
            CustomerMarketSector customerMarketSector = await GetEntity(id);
            if (customerMarketSector == null) return 0;

            context.CustomerMarketSector.Remove(customerMarketSector);
            return await context.SaveChangesAsync();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
