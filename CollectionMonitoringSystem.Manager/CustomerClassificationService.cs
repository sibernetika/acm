﻿using AutoMapper.QueryableExtensions;
using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Manager
{
    public class CustomerClassificationService : IDataService<CustomerClassification, CustomerClassificationModel>
    {
        private ArAgingContext context;
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public CustomerClassificationService()
        {
            context = new ArAgingContext();
        }

        public async Task<CustomerClassification> GetEntity(int id)
        {
            return await context.CustomerClassification.SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IList<CustomerClassification>> GetEntityList()
        {
            return await context.CustomerClassification.AsNoTracking().ToListAsync();
        }

        public async Task<IList<CustomerClassificationModel>> GetEntityListJson()
        {
            List<CustomerClassificationModel> data = AutoMapper.Mapper.Map<List<CustomerClassificationModel>>(await context.CustomerClassification.AsNoTracking().ToListAsync());
            return data;
        }

        public async Task<int> InsertEntity(CustomerClassification entity)
        {
            string ClassName = "";
            int idCl;
            List<int> idClass = new List<int>();
            if (entity.ClassificationId.Length > 1)
            {
                idClass.AddRange(entity.ClassificationId.Split(',').Select(int.Parse).ToList());
                ClassName = string.Join(", ", context.Classification.ToList().Where(d => idClass.Contains(d.Id)).Select(e => e.Description));
            }               
            else
            {
                int.TryParse(entity.ClassificationId, out idCl);
                idClass.Add(idCl);
                ClassName = context.Classification.Where(d => d.Id == idCl).Select(d => d.Description).FirstOrDefault() ?? "";
            }

            entity.ClassificationName = ClassName;
            entity.InsertedBy = "";
            entity.InsertedDate = DateTime.Now;
            context.CustomerClassification.Add(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> UpdateEntity(CustomerClassification entity)
        {
            CustomerClassification customerClassification = await GetEntity(entity.Id);
            customerClassification.CustomerNumber = entity.CustomerNumber;
            customerClassification.CustomerName = entity.CustomerName;
            customerClassification.ClassificationId = entity.ClassificationId;
            string ClassName = "";
            if (entity.ClassificationId.Length > 1)
            {
                ClassName = string.Join(", ", context.Classification.ToList().Where(d => entity.ClassificationId.Split(',').Select(int.Parse).Contains(d.Id)).Select(e => e.Description));
            }
            else
            {
                ClassName = context.Classification.Where(d => d.Id == int.Parse(entity.ClassificationId)).Select(d => d.Description).FirstOrDefault() ?? "";
            }

            customerClassification.ClassificationName = ClassName;
            customerClassification.UpdatedBy = "";
            customerClassification.UpdatedDate = DateTime.Now;
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteEntity(int id)
        {
            CustomerClassification customerClassification = await GetEntity(id);
            if (customerClassification == null) return 0;

            context.CustomerClassification.Remove(customerClassification);
            return await context.SaveChangesAsync();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
