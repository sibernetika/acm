﻿using AutoMapper.QueryableExtensions;
using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Manager
{
    public class EmailContentService : IDataService<EmailContent, EmailContentModel>
    {
        private ArAgingContext context;
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public EmailContentService()
        {
            context = new ArAgingContext();
        }

        public async Task<EmailContent> GetEntity(int id)
        {
            return await context.EmailContent.SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IList<EmailContent>> GetEntityList()
        {
            return await context.EmailContent.AsNoTracking().ToListAsync();
        }

        public async Task<IList<EmailContentModel>> GetEntityListJson()
        {
            return await context.EmailContent.AsNoTracking().ProjectTo<EmailContentModel>().ToListAsync();
        }

        public async Task<int> InsertEntity(EmailContent entity)
        {
            entity.InsertedBy = "";
            entity.InsertedDate = DateTime.Now;
            context.EmailContent.Add(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> UpdateEntity(EmailContent entity)
        {
            EmailContent email = await GetEntity(entity.Id);
            email.Name = entity.Name;
            email.Subject = entity.Subject;
            email.Content = entity.Content;
            email.UpdatedBy = "";
            email.UpdatedDate = DateTime.Now;
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteEntity(int id)
        {
            int containEmail = await context.EmailNotification.Where(x => x.EmailContentId == id).Select(x => x.EmailContentId).FirstOrDefaultAsync();
            if (containEmail > 0) return 2;

            EmailContent email = await GetEntity(id);
            if (email == null) return 0;

            context.EmailContent.Remove(email);
            return await context.SaveChangesAsync();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
