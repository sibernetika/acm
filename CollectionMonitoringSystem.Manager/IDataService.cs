﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Manager
{
    public interface IDataService<T, M> where T : class where M : class
    {
        Task<IList<T>> GetEntityList();
        Task<IList<M>> GetEntityListJson();
        Task<T> GetEntity(int id);
        Task<int> InsertEntity(T entity);
        Task<int> UpdateEntity(T entity);
        Task<int> DeleteEntity(int id);
        bool IsError { get; set; }
        string ErrorMessage { get; set; }
        void Dispose();
    }
}
