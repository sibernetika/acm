﻿using AutoMapper.QueryableExtensions;
using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Enums;
using CollectionMonitoringSystem.Context.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Manager
{
    public class PaymentService
    {
        private ArAgingContext context;
        public PaymentService()
        {
            context = new ArAgingContext();
        }

        public async Task<Payment> GetEntity(int id)
        {
            return await context.Payment.SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IList<Payment>> GetEntityList()
        {
            return await context.Payment.AsNoTracking().ToListAsync();
        }

        public async Task<IList<Payment>> GetEntityList(string customerGroup, string customerNumber, string area, string businessArea, bool showHistory = false, PaymentType paymentType = PaymentType.Plan)
        {
            if (showHistory)
                return await context.Payment.Include(x => x.Reason).AsNoTracking().Where(x => x.CustomerGroup == customerGroup && x.CustomerNumber == customerNumber && x.Area == area && x.BusinessArea == businessArea && x.IsActive == false && x.PaymentType == paymentType).ToListAsync();

            return await context.Payment.Include(x => x.Reason).AsNoTracking().Where(x => x.CustomerGroup == customerGroup && x.CustomerNumber == customerNumber && x.Area == area && x.BusinessArea == businessArea && x.IsActive == true).ToListAsync();
        }

        public async Task<IList<PaymentModel>> GetEntityListJson(string customerGroup, string area, bool showHistory = false)
        {
            if (showHistory)
                return await context.Payment.AsNoTracking().Where(x => x.CustomerGroup == customerGroup && x.Area == area && x.IsActive == false).ProjectTo<PaymentModel>().ToListAsync();

            return await context.Payment.AsNoTracking().Where(x => x.CustomerGroup == customerGroup && x.Area == area && x.IsActive == true).ProjectTo<PaymentModel>().ToListAsync();
        }

        public async Task<IList<PaymentModel>> GetEntityListJson(string customerGroup, string area)
        {
            return await context.Payment.AsNoTracking().ProjectTo<PaymentModel>().ToListAsync();
        }

        public async Task<int> InsertEntity(Payment entity)
        {
            Payment updateLastData = await context.Payment.FirstOrDefaultAsync(x => x.IsActive && x.CustomerGroup == entity.CustomerGroup && x.Area == entity.Area && x.PaymentType == entity.PaymentType);
            if (updateLastData != null)
                updateLastData.IsActive = false;
            int update = await context.SaveChangesAsync();

            entity.InsertedBy = "";
            entity.InsertedDate = DateTime.Now;
            entity.UpdatedBy = "";
            entity.UpdatedDate = DateTime.Now;
            entity.IsActive = true;
            context.Payment.Add(entity);
            return await context.SaveChangesAsync();
        }
        
        public void Dispose()
        {
            context.Dispose();
        }
    }
}
