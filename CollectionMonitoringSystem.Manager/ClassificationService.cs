﻿using AutoMapper.QueryableExtensions;
using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Manager
{
    public class ClassificationService : IDataService<Classification, ClassificationModel>
    {
        private ArAgingContext context;
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }

        public ClassificationService()
        {
            context = new ArAgingContext();
        }

        public async Task<Classification> GetEntity(int id)
        {
            return await context.Classification.SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IList<Classification>> GetEntityList()
        {
            return await context.Classification.AsNoTracking().ToListAsync();
        }

        public async Task<IList<ClassificationModel>> GetEntityListJson()
        {
            return await context.Classification.AsNoTracking().ProjectTo<ClassificationModel>().ToListAsync();
        }

        public async Task<int> InsertEntity(Classification entity)
        {
            entity.InsertedBy = "";
            entity.InsertedDate = DateTime.Now;
            context.Classification.Add(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> UpdateEntity(Classification entity)
        {
            Classification classification = await GetEntity(entity.Id);
            classification.Description = entity.Description;
            classification.UpdatedBy = "";
            classification.UpdatedDate = DateTime.Now;
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteEntity(int id)
        {
            int containClassification = await context.CustomerClassification.Where(x => x.ClassificationId.Contains(id.ToString())).Select(x => x.Id).FirstOrDefaultAsync();
            if (containClassification > 0) return 2;

            Classification classification = await GetEntity(id);
            if (classification == null) return 0;

            context.Classification.Remove(classification);
            return await context.SaveChangesAsync();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
