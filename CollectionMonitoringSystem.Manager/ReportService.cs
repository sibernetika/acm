﻿using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CollectionMonitoringSystem.Manager.ExtensionHelper;

namespace CollectionMonitoringSystem.Manager
{
    public class ReportService
    {
        private ArAgingContext context;
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }

        public ReportService()
        {
            context = new ArAgingContext();
        }

        public async Task<List<SourceArModel>> GetReportArAging(ReportFilterModel model)
        {
            List<SourceArModel> reportData = new List<SourceArModel>();
            try
            {
                Expression<Func<SourceArGroup, bool>> companyCodeCheck = x => true;
                if (model.SelectedCompanyCode != null) companyCodeCheck = x => model.SelectedCompanyCode.Contains(x.CompanyCode);                
                Expression<Func<SourceArGroup, bool>> customerCheck = x => true;
                if (model.SelectedCustomerGroup != null) customerCheck = x => model.SelectedCustomerGroup.Contains(x.CustomerGroup);
                Expression<Func<SourceArGroup, bool>> areaChildCheck = x => true;
                if (model.SelectedArea != null) {
                    string[] areaChild = context.BusinessArea.Where(x => model.SelectedArea.Contains(x.AreaCode)).Select(x => x.BusinessAreaCode).ToArray();
                    if (areaChild != null) areaChildCheck = x => areaChild.Contains(x.BusinessArea);
                }
                Expression<Func<SourceArGroup, bool>> bareaCheck = x => true;
                if (model.SelectedBusinessArea != null) bareaCheck = x => model.SelectedBusinessArea.Contains(x.BusinessArea);

                Expression<Func<SourceArGroup, bool>> sectorCheck = x => true;
                if(model.selectedMarketSector != null)
                {
                    var customerSector = context.CustomerMarketSector.Where(x => model.selectedMarketSector.Contains(x.MarketSectorId)).Select(x => x.CustomerNumber).ToList();
                    sectorCheck = x => customerSector.Contains(x.CustomerNumber);
                }

                Expression<Func<SourceArGroup, bool>> classCheck = x => true;
                if(model.selectedClassification != null)
                {
                    List<string> custFilter = new List<string>();
                    foreach(var cl in model.selectedClassification)
                    {
                        custFilter.AddRange(context.CustomerClassification.Where(ccc => ccc.ClassificationId.Contains(cl)).Select(kk => kk.CustomerNumber));
                    }

                    classCheck = x => custFilter.Contains(x.CustomerNumber);
                }

                Func<SourceArGroup, decimal> orderbyLargestOverdue = x => x.TotalOverDue;
                if (!model.LargestOverdue)
                    orderbyLargestOverdue = x => x.TotalOutstanding;

                var filterQuery = context.SourceArGroup.Where(companyCodeCheck).Where(customerCheck).Where(areaChildCheck).Where(bareaCheck).Where(sectorCheck).Where(classCheck).OrderByDescending(orderbyLargestOverdue).ToList();

                var query = from tblAging in filterQuery
                            join tblMarket in context.CustomerMarketSector on tblAging.CustomerNumber equals tblMarket.CustomerNumber into y
                            join tblClassification in context.CustomerClassification on tblAging.CustomerNumber equals tblClassification.CustomerNumber into z
                            join tblArea in context.Area on tblAging.Area equals tblArea.AreaCode into a
                            join tblUpdate in context.Payment.ToList().Where(qq => qq.IsActive) on new { tblAging.CustomerGroup, tblAging.CustomerNumber, tblAging.Area, tblAging.BusinessArea } equals new { tblUpdate.CustomerGroup, tblUpdate.CustomerNumber, tblUpdate.Area, tblUpdate.BusinessArea } into comment
                            from subMarket in y.DefaultIfEmpty()
                            from subClass in z.DefaultIfEmpty()
                            from subArea in a.DefaultIfEmpty()
                            from subComment in comment.DefaultIfEmpty()
                            select new SourceArModel
                            {
                                CustomerGroup = tblAging.CustomerGroup,
                                CustomerNumber = tblAging.CustomerNumber,
                                MarketSector = subMarket?.MarketSector?.Description ?? "",
                                Classification = subClass?.ClassificationName ?? "",
                                Area = tblAging.Area,
                                AreaName = subArea?.AreaName ?? "",
                                BusinessArea = tblAging.BusinessArea,
                                CustomerName = tblAging.CustomerName,
                                NotYetDue = tblAging.NotYetDue,
                                Due = tblAging.Due,
                                Current = (tblAging.NotYetDue + tblAging.Due),
                                OverDue30 = tblAging.OverDue30,
                                OverDue60 = tblAging.OverDue60,
                                OverDue90 = tblAging.OverDue90,
                                OverDue120 = tblAging.OverDue120,
                                OverDue360 = tblAging.OverDue360,
                                OverDueGreaterThan360 = tblAging.OverDueGreaterThan360,
                                UnApplied = tblAging.UnApplied,
                                TotalOverDue = tblAging.TotalOverDue,
                                TotalOutstanding = tblAging.TotalOutstanding,
                                NetOverdue = (tblAging.TotalOverDue + tblAging.UnApplied),
                                Description = subComment?.Description ?? ""
                            };
                reportData = query.ToList();                
                
                if (model.TakeTop > 0)
                    reportData = reportData.GetRange(0, Math.Min(model.TakeTop, reportData.Count));
            }
            catch (Exception error)
            {
                IsError = true;
                ErrorMessage = error.Message;
            }

            return reportData;
        }

        public List<EmailNotificationRecordModel> GetEmailRecord()
        {
            var query = from tblNotif in context.EmailNotificationRecord.Where(x => !x.IsSent)
                        join tblArea in context.Area on tblNotif.Area equals tblArea.AreaCode into area
                        from detailArea in area.DefaultIfEmpty()
                        orderby tblNotif.LastNotified descending
                        select new EmailNotificationRecordModel
                        {
                            CustomerGroup = tblNotif.CustomerGroup,
                            Area = detailArea.AreaName ?? "",
                            SalesName = tblNotif.SalesName
                        };
            return query.ToList();
        }

        public List<ArConsolidatedChartModel> GetArConsolidatedByArea()
        {
            return context.SourceAr.ToList().GroupBy(x => x.Area).OrderBy(x => x.Key).Select(x => new ArConsolidatedChartModel { Name = context.Area.FirstOrDefault(a => a.AreaCode == x.Key)?.AreaName ?? "", OutStanding = x.Sum(y => y.TotalOutstanding), Overdue = x.Sum(y => y.TotalOverDue) }).ToList();
        }

        public List<ArConsolidatedChartModel> GetArConsolidatedByProfitCenter()
        {
            return context.SourceAr.ToList().GroupBy(x => x.ProfitCenterId).OrderBy(x => x.Key).Select(x => new ArConsolidatedChartModel { Name = (context.ProfitCenter.FirstOrDefault(a => a.Id == x.Key)?.ProfitCenterCode ?? "") + " - " + (context.ProfitCenter.FirstOrDefault(a => a.Id == x.Key)?.Description ?? ""), OutStanding = x.Sum(y => y.TotalOutstanding), Overdue = x.Sum(y => y.TotalOverDue)}).ToList();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
