﻿using AutoMapper.QueryableExtensions;
using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Manager
{
    public class ReasonStatusService : IDataService<ReasonStatus, ReasonStatusModel>
    {
        private ArAgingContext context;
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }

        public ReasonStatusService()
        {
            context = new ArAgingContext();
        }

        public async Task<ReasonStatus> GetEntity(int id)
        {
            return await context.ReasonStatus.SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IList<ReasonStatus>> GetEntityList()
        {
            return await context.ReasonStatus.AsNoTracking().ToListAsync();
        }

        public async Task<IList<ReasonStatusModel>> GetEntityListJson()
        {
            return await context.ReasonStatus.AsNoTracking().ProjectTo<ReasonStatusModel>().ToListAsync();
        }

        public async Task<int> InsertEntity(ReasonStatus entity)
        {
            entity.InsertedBy = "";
            entity.InsertedDate = DateTime.Now;
            context.ReasonStatus.Add(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> UpdateEntity(ReasonStatus entity)
        {
            ReasonStatus reasonStatus = await GetEntity(entity.Id);
            reasonStatus.Description = entity.Description;
            reasonStatus.UpdatedBy = "";
            reasonStatus.UpdatedDate = DateTime.Now;
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteEntity(int id)
        {
            int containStatus = await context.Reason.Where(x => x.StatusId == id).Select(x => x.StatusId).FirstOrDefaultAsync();
            if (containStatus > 0) return 2;

            ReasonStatus reasonStatus = await GetEntity(id);
            if (reasonStatus == null) return 0;

            context.ReasonStatus.Remove(reasonStatus);
            return await context.SaveChangesAsync();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
