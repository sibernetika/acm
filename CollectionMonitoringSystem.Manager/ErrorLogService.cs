﻿using CollectionMonitoringSystem.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Manager
{
    public class ErrorLogService
    {
        private ArAgingContext context;
        public ErrorLogService()
        {
            context = new ArAgingContext();
        }

        public int LogError(Exception error)
        {
            try
            {
                ErrorLog entity = new ErrorLog();
                entity.ErrorDate = DateTime.Now;
                entity.ErrorMessage = error.Message;
                entity.ErrorSource = error.Source;
                entity.ErrorStackTrace = error.ToString();
                entity.RaisedBy = "";
                context.ErrorLog.Add(entity);
                return context.SaveChanges();
            }
            catch(Exception er)
            {
                return 0;
            }
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
