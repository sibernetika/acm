﻿using AutoMapper.QueryableExtensions;
using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Manager
{
    public class ProfitCenterService : IDataService<ProfitCenter, ProfitCenterModel>
    {
        private ArAgingContext context;
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public ProfitCenterService()
        {
            context = new ArAgingContext();
        }

        public async Task<ProfitCenter> GetEntity(int id)
        {
            return await context.ProfitCenter.SingleOrDefaultAsync(x => x.Id == id);
        }
        public async Task<ProfitCenter> GetEntity(string profitCenterCode)
        {
            return await context.ProfitCenter.SingleOrDefaultAsync(x => x.ProfitCenterCode == profitCenterCode);
        }

        public async Task<IList<ProfitCenter>> GetEntityList()
        {
            return await context.ProfitCenter.AsNoTracking().ToListAsync();
        }

        public async Task<IList<ProfitCenterModel>> GetEntityListJson()
        {
            return AutoMapper.Mapper.Map<List<ProfitCenterModel>>(await context.ProfitCenter.AsNoTracking().ToListAsync());
        }

        public async Task<int> InsertEntity(ProfitCenter entity)
        {
            entity.InsertedBy = "";
            entity.InsertedDate = DateTime.Now;
            context.ProfitCenter.Add(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> UpdateEntity(ProfitCenter entity)
        {
            ProfitCenter profitCenter = await GetEntity(entity.Id);
            profitCenter.ProfitCenterCode = entity.ProfitCenterCode;
            profitCenter.Description = entity.Description;
            profitCenter.ProfitCenterCategory = entity.ProfitCenterCategory;
            profitCenter.UpdatedBy = "";
            profitCenter.UpdatedDate = DateTime.Now;
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteEntity(int id)
        {
            int containReason = await context.SourceAr.AsNoTracking().Where(x => x.ProfitCenterId == id).Select(x => x.ProfitCenterId).FirstOrDefaultAsync();
            if (containReason > 0) return 2;

            ProfitCenter profitCenter = await GetEntity(id);
            if (profitCenter == null) return 0;

            context.ProfitCenter.Remove(profitCenter);
            return await context.SaveChangesAsync();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
