﻿using AutoMapper.QueryableExtensions;
using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Manager
{
    public class EmailBeforeDueService : IDataService<EmailNotificationBeforeDue, EmailNotificationBeforeDueModel>
    {
        private ArAgingContext context;
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }

        public EmailBeforeDueService()
        {
            context = new ArAgingContext();
        }

        public async Task<int> DeleteEntity(int id)
        {
            EmailNotificationBeforeDue email = await GetEntity(id);
            if (email == null) return 0;

            context.EmailNotificationBeforeDue.Remove(email);
            return await context.SaveChangesAsync();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public async Task<EmailNotificationBeforeDue> GetEntity(int id)
        {
            return await context.EmailNotificationBeforeDue.SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IList<EmailNotificationBeforeDue>> GetEntityList()
        {
            return await context.EmailNotificationBeforeDue.Include(x => x.EmailContent).AsNoTracking().ToListAsync();
        }

        public async Task<IList<EmailNotificationBeforeDueModel>> GetEntityListJson()
        {
            return await context.EmailNotificationBeforeDue.Include(x => x.EmailContent).AsNoTracking().ProjectTo<EmailNotificationBeforeDueModel>().ToListAsync();
        }

        public async Task<int> InsertEntity(EmailNotificationBeforeDue entity)
        {
            entity.InsertedDate = DateTime.Now;
            context.EmailNotificationBeforeDue.Add(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> UpdateEntity(EmailNotificationBeforeDue entity)
        {
            EmailNotificationBeforeDue email = await GetEntity(entity.Id);
            email.EmailContentId = entity.EmailContentId;
            email.NotifyToSales = entity.NotifyToSales;
            email.NotifyToSupervisor = entity.NotifyToSupervisor;
            email.NotifyToFinance = entity.NotifyToFinance;
            email.BeforeDue = entity. BeforeDue;
            email.UpdatedBy = entity.UpdatedBy;
            email.UpdatedDate = DateTime.Now;
            return await context.SaveChangesAsync();
        }

        public async Task SendEmail()
        {
            throw new NotImplementedException();
        }
    }
}
