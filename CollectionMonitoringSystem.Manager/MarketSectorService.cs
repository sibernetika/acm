﻿using AutoMapper.QueryableExtensions;
using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Manager
{
    public class MarketSectorService : IDataService<MarketSector, MarketSectorModel>
    {
        private ArAgingContext context;
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }

        public MarketSectorService()
        {
            context = new ArAgingContext();
        }

        public async Task<MarketSector> GetEntity(int id)
        {
            return await context.MarketSector.SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IList<MarketSector>> GetEntityList()
        {
            return await context.MarketSector.AsNoTracking().ToListAsync();
        }

        public async Task<IList<MarketSectorModel>> GetEntityListJson()
        {
            return await context.MarketSector.AsNoTracking().ProjectTo<MarketSectorModel>().ToListAsync();
        }

        public async Task<int> InsertEntity(MarketSector entity)
        {
            entity.InsertedBy = "";
            entity.InsertedDate = DateTime.Now;
            context.MarketSector.Add(entity);
            return await context.SaveChangesAsync();
        }

        public async Task<int> UpdateEntity(MarketSector entity)
        {
            MarketSector marketSector = await GetEntity(entity.Id);
            marketSector.Description = entity.Description;
            marketSector.UpdatedBy = "";
            marketSector.UpdatedDate = DateTime.Now;
            return await context.SaveChangesAsync();
        }

        public async Task<int> DeleteEntity(int id)
        {
            int containMarket = await context.CustomerMarketSector.Where(x => x.MarketSectorId == id).Select(x => x.MarketSectorId).FirstOrDefaultAsync();
            if (containMarket > 0) return 2;

            MarketSector marketSector = await GetEntity(id);
            if (marketSector == null) return 0;

            context.MarketSector.Remove(marketSector);
            return await context.SaveChangesAsync();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
