﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context
{
    public class ArAgingContext : DbContext
    {
        public ArAgingContext() : base("AcmARAgingContext") {
            Database.SetInitializer<ArAgingContext>(null);
        }

        public DbSet<ReasonStatus> ReasonStatus { get; set; }
        public DbSet<Reason> Reason { get; set; }
        public DbSet<ProfitCenter> ProfitCenter { get; set; }
        public DbSet<EmailContent> EmailContent { get; set; }
        public DbSet<EmailNotification> EmailNotification { get; set; }
        public DbSet<EmailNotificationBeforeDue> EmailNotificationBeforeDue { get; set; }
        public DbSet<SourceAr> SourceAr { get; set; }
        public DbSet<SourceArGroup> SourceArGroup { get; set; }
        public DbSet<EmailNotificationRecord> EmailNotificationRecord { get; set; }
        public DbSet<Payment> Payment { get; set; }
        public DbSet<Finance> Finance { get; set; }
        public DbSet<Area> Area { get; set; }
        public DbSet<BusinessArea> BusinessArea { get; set; }
        public DbSet<Classification> Classification { get; set; }
        public DbSet<MarketSector> MarketSector { get; set; }
        public DbSet<CustomerClassification> CustomerClassification { get; set; }
        public DbSet<CustomerMarketSector> CustomerMarketSector { get; set; }
        public DbSet<ErrorLog> ErrorLog { get; set; }
        public DbSet<SalesMasterPP> SalesMasterPP { get; set; }
        public DbSet<SalesMasterPS> SalesMasterPS { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());
            modelBuilder.HasDefaultSchema("Acm");

            modelBuilder.Properties().Where(x => x.PropertyType == typeof(decimal)).Configure(x => { x.HasPrecision(19, 2); x.IsRequired(); });

            base.OnModelCreating(modelBuilder);
        }
    }
}
