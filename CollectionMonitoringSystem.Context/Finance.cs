﻿using CollectionMonitoringSystem.Context.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context
{
    public class Finance : DefaultEntities
    {
        [Required]
        public string BusinessArea { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public PositionType PositionType { get; set; }
        public string PositionName { get; set; }
        [Required]
        public string Email { get; set; } 
    }
}
