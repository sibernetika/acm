﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context.Models
{
    public class ReportModel
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int CompanyCode { get; set; }
        public string AccountGroup { get; set; }
        public string Area { get; set; }
        public string CustomerGroup { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerName { get; set; }
        public string BusinessArea { get; set; }
        public string DocumentNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string Insdustry { get; set; }
        public string Classification { get; set; }
        public int ProfitCenterId { get; set; }
        public string ProfitCenterName { get; set; }
        public int Aging { get; set; }
        public DateTime DueDate { get; set; }
        public string Currency { get; set; }
        public decimal AmountDocCur { get; set; }
        public decimal NotYetDue { get; set; }
        public decimal Due { get; set; }
        public decimal Current { get; set; }
        public decimal OverDue30 { get; set; }
        public decimal OverDue60 { get; set; }
        public decimal OverDue90 { get; set; }
        public decimal OverDue120 { get; set; }
        public decimal OverDue360 { get; set; }
        public decimal OverDueGreaterThan360 { get; set; }
        public decimal UnApplied { get; set; }
        public decimal DownPayment { get; set; }
        public decimal TotalOverDue { get; set; }
        public decimal TotalOutstanding { get; set; }
        public DateTime InsertedDate { get; set; }
    }
}
