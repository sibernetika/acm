﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context.Models
{
    public class ReportArConsolidatedModel
    {
        public List<ArConsolidatedChartModel> CurrentMonth { get; set; }
        public List<ArConsolidatedChartModel> PreviousMonth { get; set; }
    }
}
