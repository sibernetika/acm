﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context.Models
{
    public class ArDetailModel
    {
        public string CustomerGroup { get; set; }
        public string CustomerNumber { get; set; }
        public string Area { get; set; }
        public string BusinessArea { get; set; }
        public List<SourceArModel> SourceData { get; set; }
    }
}
