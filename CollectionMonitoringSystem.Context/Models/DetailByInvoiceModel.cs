﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context.Models
{
    public class DetailByInvoiceModel
    {
        public string InvoiceNumber { get; set; }
        public string InvoiceDate { get; set; }
        public decimal NotYetDue { get; set; }
        public decimal Due { get; set; }
        public decimal Current { get; set; }
        public decimal OverDue30 { get; set; }
        public decimal OverDue60 { get; set; }
        public decimal OverDue90 { get; set; }
        public decimal OverDue120 { get; set; }
        public decimal OverDue360 { get; set; }
        public decimal OverDueGreaterThan360 { get; set; }
        public decimal UnApplied { get; set; }
        public decimal TotalOverDue { get; set; }
        public decimal NetOverdue { get; set; }
        public decimal TotalOutstanding { get; set; }
    }
}
