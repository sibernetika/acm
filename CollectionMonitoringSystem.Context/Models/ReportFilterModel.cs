﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context.Models
{
    public class ReportFilterModel
    {
        public List<FilterModel> CustomerGroup { get; set; }
        public string[] SelectedCustomerGroup { get; set; }
        public List<FilterModel> CompanyCode { get; set; }
        public int[] SelectedCompanyCode { get; set; }
        public List<FilterModel> Area { get; set; }
        public string[] SelectedArea { get; set; }
        public List<FilterModel> BusinessArea { get; set; }
        public string[] SelectedBusinessArea { get; set; }
        public List<FilterModel> ProfitCenter { get; set; }
        public int[] SelectedProfitCenter { get; set; }
        public List<FilterModel> MarketSector { get; set; }
        public int[] selectedMarketSector { get; set; }
        public List<FilterModel> Status { get; set; }
        public List<FilterModel> Classification { get; set; }
        public string[] selectedClassification { get; set; }
        public int TakeTop { get; set; }
        public bool LargestOverdue { get; set; }
        public List<SourceArModel> ReportData { get; set; }       
        public string LastUpdate { get; set; }
    }

    public class FilterModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Name { get; set; }
    }
}
