﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context.Models
{
    public class CustomerMarketSectorModel
    {
        public int Id { get; set; }
        [Required]
        public string CustomerNumber { get; set; }
        [Required]
        public string CustomerName { get; set; }
        [Required(ErrorMessage = "Market Sector is required. (If empty, please add a new sector first)")]
        public int? MarketSectorId { get; set; }
        public string MarketSectorDescription { get; set; }
        public string InsertedBy { get; set; }
        public DateTime? InsertedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
