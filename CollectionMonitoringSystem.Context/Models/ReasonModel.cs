﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context.Models
{
    public class ReasonModel
    {
        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int? StatusId { get; set; }
        public string StatusDescription { get; set; }        
        public string InsertedBy { get; set; }
        public DateTime? InsertedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
