﻿using CollectionMonitoringSystem.Context.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context.Models
{
    public class PaymentModel
    {
        public int Id { get; set; }
        public string CustomerGroup { get; set; }
        public string CustomerNumber { get; set; }
        public string Area { get; set; }
        public string BusinessArea { get; set; }
        public string Description { get; set; }
        public PaymentType PaymentType { get; set; }
        public string PaymentTypeName { get; set; }
        public int ReasonId { get; set; }
        public string ReasonName { get; set; }
        public bool IsActive { get; set; }
        public decimal Week1 { get; set; }
        public decimal Week2 { get; set; }
        public decimal Week3 { get; set; }
        public decimal Week4 { get; set; }
        public string InsertedBy { get; set; }
        public DateTime? InsertedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
