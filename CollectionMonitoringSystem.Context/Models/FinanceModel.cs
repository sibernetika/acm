﻿using CollectionMonitoringSystem.Context.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context.Models
{
    public class FinanceModel
    {
        public int Id { get; set; }
        [Required]
        public string BusinessArea { get; set; }
        [Required(ErrorMessage = "Please select employee name from the list")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Position Type is required.")]
        public PositionType? PositionType { get; set; }
        public string PositionTypeName { get; set; }
        public string PositionName { get; set; }
        [Required]
        public string Email { get; set; }
        public string InsertedBy { get; set; }
        public DateTime? InsertedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
