﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context.Models
{
    public class ParameterModel
    {
        public string[] userID { get; set; }
        public string ipAddress { get; set; }
        public string userAgent { get; set; }
    }
}
