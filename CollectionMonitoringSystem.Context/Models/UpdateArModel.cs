﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context.Models
{
    public class UpdateArModel
    {
        [Required]
        public string PersonInCharge { get; set; }
        public int ReasonId { get; set; }
        public string ReasonName { get; set; }
        public string StatusName { get; set; }
        public string CustomerGroup { get; set; }
        public string Area { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string ActionPlan { get; set; }
        public decimal SumTotalOutstanding { get; set; }
        public decimal SumTotalOverdue { get; set; }
        public DateTime InsertedDate { get; set; }
    }
}
