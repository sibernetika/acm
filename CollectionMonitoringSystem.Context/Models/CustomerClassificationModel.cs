﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context.Models
{
    public class CustomerClassificationModel
    {
        public int Id { get; set; }
        [Required]
        [StringLength(25)]
        public string CustomerNumber { get; set; }
        [Required]
        [StringLength(250)]
        public string CustomerName { get; set; }
        public string ClassificationId { get; set; }
        public string ClassificationName { get; set; }
        [Required]
        public int[] ClassificationIdList { get; set; }
        public string InsertedBy { get; set; }
        public DateTime? InsertedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }        
    }
}
