﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context.Models
{
    public class EmailNotificationRecordModel
    {
        public int Id { get; set; }
        public string CustomerGroup { get; set; }
        public string Area { get; set; }
        public DateTime LastNotified { get; set; }
        public bool IsSent { get; set; }
        public string SalesName { get; set; }
        public string Description { get; set; }
    }
}
