﻿using CollectionMonitoringSystem.Context.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context.Models
{
    public class ProfitCenterModel
    {
        public int Id { get; set; }
        [Required]
        [StringLength(4, MinimumLength = 4, ErrorMessage = "Profit center code has to be in 4 digits format")]
        public string ProfitCenterCode { get; set; }
        [Required]
        [StringLength(50)]
        public string Description { get; set; }
        [Required]
        public ProfitCenterCategory ProfitCenterCategory { get; set; }
        public string ProfitCenterCategoryName { get; set; }
        public string InsertedBy { get; set; }
        public DateTime? InsertedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
