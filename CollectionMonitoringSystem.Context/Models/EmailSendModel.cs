﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context.Models
{
    public class EmailSendModel
    {
        public EmailContactModel Sales { get; set; }
        public EmailContactModel Supervisor { get; set; }
        public EmailContactModel Manager { get; set; }
        public EmailContactModel Finance { get; set; }
        public EmailContactModel GmSales { get; set; }
        public EmailContactModel Chief { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public List<EmailSummaryModel> Summary { get; set; }
    }

    public class EmailContactModel
    {
        public string UserName { get; set; }
        public string UserId { get; set; }
        public string ContactName { get; set; }
        public string EmailAddress { get; set; }
        public bool IsNotified { get; set; }
    }

    public class EmailSummaryModel
    {
        public string CustomerGroup { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerName { get; set; }
        public string Area { get; set; }
        public string BusinessArea { get; set; }
        public decimal TotalOverdue { get; set; }
        public string DocumentNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceDate { get; set; }
        public string DueDate { get; set; }
    }
}
