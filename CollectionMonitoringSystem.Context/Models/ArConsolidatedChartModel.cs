﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context.Models
{
    public class ArConsolidatedChartModel
    {
        public string Name { get; set; }
        public decimal OutStanding { get; set; }
        public decimal Overdue { get; set; }
    }
}
