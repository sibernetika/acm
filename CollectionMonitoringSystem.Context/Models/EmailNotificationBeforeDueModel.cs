﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context.Models
{
    public class EmailNotificationBeforeDueModel
    {
        public int Id { get; set; }
        [Required]
        public int BeforeDue { get; set; }
        public bool NotifyToSales { get; set; } = false;
        public bool NotifyToSupervisor { get; set; } = false;
        public bool NotifyToFinance { get; set; } = false;
        [Required(ErrorMessage = "This field is required")]
        public int? EmailContentId { get; set; }
        public string TemplateName { get; set; }
        public string InsertedBy { get; set; }
        public DateTime? InsertedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
