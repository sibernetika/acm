﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CollectionMonitoringSystem.Context
{
    public class Area
    {
        [Key]
        public int Id { get; set; }
        [StringLength(2)]
        [Index("IDXAREACODE", IsClustered = false, IsUnique = false)]
        public string AreaCode { get; set; }
        [StringLength(2)]
        public string AreaParentCode { get; set; }
        [StringLength(100)]
        public string AreaName { get; set; }
        public bool Show { get; set; }
    }
}
