﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context
{
    public class EmailNotificationBeforeDue : DefaultEntities
    {
        [Required]
        public int BeforeDue { get; set; }
        [Required]
        public bool NotifyToSales { get; set; } = false;
        [Required]
        public bool NotifyToSupervisor { get; set; } = false;
        [Required]
        public bool NotifyToFinance { get; set; } = false;
        [Required]
        public int EmailContentId { get; set; }
        public virtual EmailContent EmailContent { get; set; }
    }
}
