﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context
{
    public class SalesMaster
    {
        public int Id { get; set; }
        [StringLength(20)]
        [Index("IDXSALESMASTERDOCNUM", IsClustered = false, IsUnique = false, Order = 1)]
        [Index("IDXSALESMASTERSALES", IsClustered = false, IsUnique = false, Order = 1)]
        public string DocumentNumber { get; set; }
        [StringLength(20)]
        [Index("IDXSALESMASTERSALES", IsClustered = false, IsUnique = false, Order = 2)]
        public string Xupj { get; set; }
        [Index("IDXSALESMASTERDOCNUM", IsClustered = false, IsUnique = false, Order = 2)]
        public int? SalesId { get; set; }
        [StringLength(20)]
        public string SalesName { get; set; }
        [StringLength(100)]
        public string SalesEmail { get; set; }
        public int? SuperiorId1 { get; set; }
        [StringLength(20)]
        public string SuperiorXupj1 { get; set; }
        [StringLength(100)]
        public string SuperiorEmail1 { get; set; }
        public int? SuperiorId2 { get; set; }
        [StringLength(20)]
        public string SuperiorXupj2 { get; set; }
        [StringLength(100)]
        public string SuperiorEmail2 { get; set; }
    }
}
