﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context
{
    public class EmailNotification : DefaultEntities
    {
        [Required]
        public int Overdue { get; set; }
        public int ResendEvery { get; set; } = 0;
        public bool NotifyToSales { get; set; } = false;
        public bool NotifyToSupervisor { get; set; } = false;
        public bool NotifyToManager { get; set; } = false;
        public bool NotifyToFinance { get; set; } = false;
        public bool NotifyToGmSales { get; set; } = false;
        public bool NotifyToChief { get; set; } = false;
        public int EmailContentId { get; set; }
        public virtual EmailContent EmailContent { get; set; }
    }
}
