namespace CollectionMonitoringSystem.Context.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ArAgingContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ArAgingContext context)
        {
            context.ReasonStatus.AddOrUpdate(
                new ReasonStatus { Id = 1, Description = "In-Progress (-)", InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new ReasonStatus { Id = 2, Description = "In-Progress (=)", InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new ReasonStatus { Id = 3, Description = "In-Progress (+)", InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new ReasonStatus { Id = 4, Description = "No", InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new ReasonStatus { Id = 5, Description = "Yes", InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now }
                );

            context.Reason.AddOrUpdate(
                new Reason { Id = 1, Description = "Promised payment with guarantee", StatusId = 5, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new Reason { Id = 2, Description = "Promised payment without guarantee", StatusId = 3, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new Reason { Id = 3, Description = "Dispute with customer", StatusId = 2, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new Reason { Id = 4, Description = "Product problem", StatusId = 2, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new Reason { Id = 5, Description = "Cash flow problem", StatusId = 1, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new Reason { Id = 6, Description = "No financing available", StatusId = 1, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new Reason { Id = 7, Description = "Customer lost tender/project", StatusId = 1, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new Reason { Id = 8, Description = "Unable to reach customer", StatusId = 1, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new Reason { Id = 9, Description = "Customer bankrupt", StatusId = 4, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new Reason { Id = 10, Description = "Somatie/Legal Action", StatusId = 4, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new Reason { Id = 11, Description = "No Commitment", StatusId = 4, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new Reason { Id = 12, Description = "Write Off/Provision", StatusId = 4, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now }
                );

            context.ProfitCenter.AddOrUpdate(
                new ProfitCenter { Id = 1, ProfitCenterCode = "1010", Description = "Machine", ProfitCenterCategory = Enums.ProfitCenterCategory.PrimeProduct, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new ProfitCenter { Id = 2, ProfitCenterCode = "1020", Description = "Engine", ProfitCenterCategory = Enums.ProfitCenterCategory.PrimeProduct, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new ProfitCenter { Id = 3, ProfitCenterCode = "1030", Description = "Parts", ProfitCenterCategory = Enums.ProfitCenterCategory.ProductSupport, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new ProfitCenter { Id = 4, ProfitCenterCode = "1040", Description = "CSA Parts", ProfitCenterCategory = Enums.ProfitCenterCategory.ProductSupport, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new ProfitCenter { Id = 5, ProfitCenterCode = "1050", Description = "Used Equipment", ProfitCenterCategory = Enums.ProfitCenterCategory.PrimeProduct, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new ProfitCenter { Id = 6, ProfitCenterCode = "1060", Description = "Service", ProfitCenterCategory = Enums.ProfitCenterCategory.ProductSupport, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new ProfitCenter { Id = 7, ProfitCenterCode = "1070", Description = "MARC", ProfitCenterCategory = Enums.ProfitCenterCategory.Others, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new ProfitCenter { Id = 8, ProfitCenterCode = "1080", Description = "CSA Service", ProfitCenterCategory = Enums.ProfitCenterCategory.ProductSupport, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new ProfitCenter { Id = 9, ProfitCenterCode = "1090", Description = "Rental", ProfitCenterCategory = Enums.ProfitCenterCategory.Others, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new ProfitCenter { Id = 10, ProfitCenterCode = "1999", Description = "Others", ProfitCenterCategory = Enums.ProfitCenterCategory.Others, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now }
                );

            string contentEmail = "<p>Dear &lt;&lt;salesname&gt;&gt;,&nbsp;</p><p><br></p><p>This email is to remind you about your customer overdue payment. Here are the summary:</p><p>&lt;&lt;summary&gt;&gt;</p><p><br></p><p>Thank you.</p><p><br></p>";
            context.EmailContent.AddOrUpdate(
                new EmailContent { Id = 1, Name = "Template 1", Subject = "Overdue", Content = contentEmail, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now }
                );

            context.EmailNotification.AddOrUpdate(
                new EmailNotification { Id = 1, Overdue = 30, ResendEvery = 10, NotifyToSales = true, NotifyToSupervisor = false, NotifyToManager = false, EmailContentId = 1, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new EmailNotification { Id = 2, Overdue = 60, ResendEvery = 9, NotifyToSales = true, NotifyToSupervisor = false, NotifyToManager = false, EmailContentId = 1, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new EmailNotification { Id = 3, Overdue = 90, ResendEvery = 7, NotifyToSales = true, NotifyToSupervisor = true, NotifyToManager = false, EmailContentId = 1, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new EmailNotification { Id = 4, Overdue = 120, ResendEvery = 5, NotifyToSales = true, NotifyToSupervisor = true, NotifyToManager = false, EmailContentId = 1, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now },
                new EmailNotification { Id = 5, Overdue = 360, ResendEvery = 2, NotifyToSales = true, NotifyToSupervisor = true, NotifyToManager = true, EmailContentId = 1, InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now }
                );

            context.Area.AddOrUpdate(
                new Area { Id = 1, AreaCode = "A", AreaName = "Java" },
                new Area { Id = 1, AreaCode = "B", AreaName = "East Indonesia" },
                new Area { Id = 1, AreaCode = "C", AreaName = "Tembagapura" },
                new Area { Id = 1, AreaCode = "E", AreaName = "North Sumatera" },
                new Area { Id = 1, AreaCode = "F", AreaName = "South Sumatera" },
                new Area { Id = 1, AreaCode = "G", AreaName = "North Kalimantan" },
                new Area { Id = 1, AreaCode = "H", AreaName = "South Kalimantan" },
                new Area { Id = 1, AreaCode = "R", AreaName = "Batu Hijau" },
                new Area { Id = 1, AreaCode = "W", AreaName = "Product Support" },
                new Area { Id = 1, AreaCode = "X", AreaName = "Other" },
                new Area { Id = 1, AreaCode = "Z", AreaName = "Head Office" }
                );

            context.Classification.AddOrUpdate(
                new Classification { Id = 1, Description = "Major Customer", InsertedBy = "ARSystem", InsertedDate = DateTime.Now, UpdatedBy = "ARSystem", UpdatedDate = DateTime.Now }
                );
            
        }
    }
}
