namespace CollectionMonitoringSystem.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatePaymentAddIsActive : DbMigration
    {
        public override void Up()
        {
            DropIndex("Ara.Payment", "IDXFILTERPAYMENT");
            AddColumn("Ara.Payment", "IsActive", c => c.Boolean(nullable: false));
            CreateIndex("Ara.Payment", new[] { "CustomerGroup", "Area", "PaymentType", "IsActive" }, name: "IDXFILTERPAYMENT");
        }
        
        public override void Down()
        {
            DropIndex("Ara.Payment", "IDXFILTERPAYMENT");
            DropColumn("Ara.Payment", "IsActive");
            CreateIndex("Ara.Payment", new[] { "CustomerGroup", "Area", "PaymentType" }, name: "IDXFILTERPAYMENT");
        }
    }
}
