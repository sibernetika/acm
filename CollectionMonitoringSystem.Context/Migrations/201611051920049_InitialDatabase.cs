namespace CollectionMonitoringSystem.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Ara.EmailContent",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 20),
                        Subject = c.String(nullable: false, maxLength: 100),
                        Content = c.String(),
                        InsertedBy = c.String(maxLength: 50),
                        InsertedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Ara.EmailNotification",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Overdue = c.Int(nullable: false),
                        ResendEvery = c.Int(nullable: false),
                        NotifyToSales = c.Boolean(nullable: false),
                        NotifyToSupervisor = c.Boolean(nullable: false),
                        NotifyToManager = c.Boolean(nullable: false),
                        EmailContentId = c.Int(nullable: false),
                        InsertedBy = c.String(maxLength: 50),
                        InsertedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Ara.EmailContent", t => t.EmailContentId)
                .Index(t => t.EmailContentId);
            
            CreateTable(
                "Ara.EmailNotificationRecord",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CustomerGroup = c.String(nullable: false, maxLength: 25),
                        Area = c.String(nullable: false, maxLength: 1),
                        LastNotified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.CustomerGroup, t.Area, t.LastNotified }, name: "IDXFILTERNOTIFICATIONRECORD");
            
            CreateTable(
                "Ara.Payment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CustomerGroup = c.String(nullable: false, maxLength: 25),
                        Area = c.String(nullable: false, maxLength: 1),
                        PaymentType = c.Int(nullable: false),
                        Week1 = c.Decimal(nullable: false, precision: 19, scale: 2),
                        Week2 = c.Decimal(nullable: false, precision: 19, scale: 2),
                        Week3 = c.Decimal(nullable: false, precision: 19, scale: 2),
                        Week4 = c.Decimal(nullable: false, precision: 19, scale: 2),
                        InsertedBy = c.String(maxLength: 50),
                        InsertedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.CustomerGroup, t.Area, t.PaymentType }, name: "IDXFILTERPAYMENT");
            
            CreateTable(
                "Ara.ProfitCenter",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProfitCenterCode = c.String(nullable: false, maxLength: 4),
                        Description = c.String(nullable: false, maxLength: 50),
                        ProfitCenterCategory = c.Int(nullable: false),
                        InsertedBy = c.String(maxLength: 50),
                        InsertedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.ProfitCenterCode, unique: true, name: "IDXProfitCenterCode");
            
            CreateTable(
                "Ara.Reason",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 250),
                        StatusId = c.Int(nullable: false),
                        InsertedBy = c.String(maxLength: 50),
                        InsertedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Ara.ReasonStatus", t => t.StatusId)
                .Index(t => t.StatusId);
            
            CreateTable(
                "Ara.ReasonStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 50),
                        InsertedBy = c.String(maxLength: 50),
                        InsertedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Ara.SourceAr",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Year = c.Int(nullable: false),
                        Month = c.Int(nullable: false),
                        CompanyCode = c.Int(nullable: false),
                        AccountGroup = c.String(nullable: false, maxLength: 10),
                        Area = c.String(nullable: false, maxLength: 1),
                        CustomerGroup = c.String(nullable: false, maxLength: 25),
                        CustomerNumber = c.String(nullable: false, maxLength: 25),
                        CustomerName = c.String(nullable: false, maxLength: 150),
                        BusinessArea = c.String(nullable: false, maxLength: 10),
                        Insdustry = c.String(maxLength: 100),
                        ProfitCenterId = c.Int(nullable: false),
                        Aging = c.Int(nullable: false),
                        AmountDocCur = c.Decimal(nullable: false, precision: 19, scale: 2),
                        NotYetDue = c.Decimal(nullable: false, precision: 19, scale: 2),
                        Due = c.Decimal(nullable: false, precision: 19, scale: 2),
                        OverDue30 = c.Decimal(nullable: false, precision: 19, scale: 2),
                        OverDue60 = c.Decimal(nullable: false, precision: 19, scale: 2),
                        OverDue90 = c.Decimal(nullable: false, precision: 19, scale: 2),
                        OverDue120 = c.Decimal(nullable: false, precision: 19, scale: 2),
                        OverDue360 = c.Decimal(nullable: false, precision: 19, scale: 2),
                        OverDueGreaterThan360 = c.Decimal(nullable: false, precision: 19, scale: 2),
                        UnApplied = c.Decimal(nullable: false, precision: 19, scale: 2),
                        TotalOverDue = c.Decimal(nullable: false, precision: 19, scale: 2),
                        TotalOutstanding = c.Decimal(nullable: false, precision: 19, scale: 2),
                        InsertedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Ara.ProfitCenter", t => t.ProfitCenterId)
                .Index(t => new { t.InsertedDate, t.CompanyCode, t.Year, t.Month, t.CustomerGroup, t.Area, t.ProfitCenterId }, name: "IDXFILTERSOURCEAR")
                .Index(t => new { t.InsertedDate, t.Year, t.Month, t.CustomerGroup, t.Area }, name: "IDXFILTERSOURCEAR2")
                .Index(t => new { t.InsertedDate, t.CustomerGroup }, name: "IDXFILTERCUSTOMERGROUP")
                .Index(t => t.ProfitCenterId, name: "IDXFILTERSOURCEARPROFITCENTER");
            
            CreateTable(
                "Ara.UpdateAr",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PersonInCharge = c.String(nullable: false, maxLength: 50),
                        ReasonId = c.Int(nullable: false),
                        CustomerGroup = c.String(nullable: false, maxLength: 25),
                        Area = c.String(nullable: false, maxLength: 1),
                        Description = c.String(),
                        ActionPlan = c.String(),
                        SumTotalOutstanding = c.Decimal(nullable: false, precision: 19, scale: 2),
                        SumTotalOverdue = c.Decimal(nullable: false, precision: 19, scale: 2),
                        InsertedBy = c.String(maxLength: 50),
                        InsertedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Ara.Reason", t => t.ReasonId)
                .Index(t => t.ReasonId, name: "IDXREASONINUPDATEAR")
                .Index(t => new { t.CustomerGroup, t.Area }, name: "IDXFILTERUPDATEAR");
            
        }
        
        public override void Down()
        {
            DropForeignKey("Ara.UpdateAr", "ReasonId", "Ara.Reason");
            DropForeignKey("Ara.SourceAr", "ProfitCenterId", "Ara.ProfitCenter");
            DropForeignKey("Ara.Reason", "StatusId", "Ara.ReasonStatus");
            DropForeignKey("Ara.EmailNotification", "EmailContentId", "Ara.EmailContent");
            DropIndex("Ara.UpdateAr", "IDXFILTERUPDATEAR");
            DropIndex("Ara.UpdateAr", "IDXREASONINUPDATEAR");
            DropIndex("Ara.SourceAr", "IDXFILTERSOURCEARPROFITCENTER");
            DropIndex("Ara.SourceAr", "IDXFILTERCUSTOMERGROUP");
            DropIndex("Ara.SourceAr", "IDXFILTERSOURCEAR2");
            DropIndex("Ara.SourceAr", "IDXFILTERSOURCEAR");
            DropIndex("Ara.Reason", new[] { "StatusId" });
            DropIndex("Ara.ProfitCenter", "IDXProfitCenterCode");
            DropIndex("Ara.Payment", "IDXFILTERPAYMENT");
            DropIndex("Ara.EmailNotificationRecord", "IDXFILTERNOTIFICATIONRECORD");
            DropIndex("Ara.EmailNotification", new[] { "EmailContentId" });
            DropTable("Ara.UpdateAr");
            DropTable("Ara.SourceAr");
            DropTable("Ara.ReasonStatus");
            DropTable("Ara.Reason");
            DropTable("Ara.ProfitCenter");
            DropTable("Ara.Payment");
            DropTable("Ara.EmailNotificationRecord");
            DropTable("Ara.EmailNotification");
            DropTable("Ara.EmailContent");
        }
    }
}
