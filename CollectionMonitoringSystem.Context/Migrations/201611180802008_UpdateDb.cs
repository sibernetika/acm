namespace CollectionMonitoringSystem.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDb : DbMigration
    {
        public override void Up()
        {
            DropIndex("Acm.SalesMaster", "IDXSALESMASTERDOCNUM");
            DropIndex("Acm.SourceAr", "IDXDUEDATE");
            AlterColumn("Acm.SalesMaster", "SalesId", c => c.Int());
            AlterColumn("Acm.SalesMaster", "SuperiorId1", c => c.Int());
            AlterColumn("Acm.SalesMaster", "SuperiorId2", c => c.Int());
            AlterColumn("Acm.SourceAr", "DueDate", c => c.DateTime());
            AlterColumn("Acm.SourceAr", "AmountDocCur", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("Acm.SourceAr", "InsertedDate", c => c.DateTime());
            CreateIndex("Acm.SalesMaster", new[] { "DocumentNumber", "SalesId" }, name: "IDXSALESMASTERDOCNUM");
            CreateIndex("Acm.SourceAr", "DueDate", name: "IDXDUEDATE");
        }
        
        public override void Down()
        {
            DropIndex("Acm.SourceAr", "IDXDUEDATE");
            DropIndex("Acm.SalesMaster", "IDXSALESMASTERDOCNUM");
            AlterColumn("Acm.SourceAr", "InsertedDate", c => c.DateTime(nullable: false));
            AlterColumn("Acm.SourceAr", "AmountDocCur", c => c.Decimal(nullable: false, precision: 19, scale: 2));
            AlterColumn("Acm.SourceAr", "DueDate", c => c.DateTime(nullable: false));
            AlterColumn("Acm.SalesMaster", "SuperiorId2", c => c.Int(nullable: false));
            AlterColumn("Acm.SalesMaster", "SuperiorId1", c => c.Int(nullable: false));
            AlterColumn("Acm.SalesMaster", "SalesId", c => c.Int(nullable: false));
            CreateIndex("Acm.SourceAr", "DueDate", name: "IDXDUEDATE");
            CreateIndex("Acm.SalesMaster", new[] { "DocumentNumber", "SalesId" }, name: "IDXSALESMASTERDOCNUM");
        }
    }
}
