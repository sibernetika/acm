namespace CollectionMonitoringSystem.Context.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Acm.Area",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AreaCode = c.String(maxLength: 1),
                        AreaName = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.AreaCode, name: "IDXAREACODE");
            
            CreateTable(
                "Acm.Classification",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 150),
                        InsertedBy = c.String(maxLength: 50),
                        InsertedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Acm.CustomerClassification",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CustomerNumber = c.String(nullable: false, maxLength: 25),
                        CustomerName = c.String(nullable: false, maxLength: 250),
                        ClassificationId = c.String(),
                        InsertedBy = c.String(maxLength: 50),
                        InsertedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.CustomerNumber, name: "IDXCUSTOMERCLASSIFICATIONNUMBER");
            
            CreateTable(
                "Acm.CustomerMarketSector",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CustomerNumber = c.String(nullable: false, maxLength: 25),
                        CustomerName = c.String(nullable: false, maxLength: 250),
                        MarketSectorId = c.Int(nullable: false),
                        InsertedBy = c.String(maxLength: 50),
                        InsertedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Acm.MarketSector", t => t.MarketSectorId)
                .Index(t => t.CustomerNumber, name: "IDXCUSTOMERMARKETSECTORNUMBER")
                .Index(t => t.MarketSectorId);
            
            CreateTable(
                "Acm.MarketSector",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 150),
                        InsertedBy = c.String(maxLength: 50),
                        InsertedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Acm.EmailContent",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 20),
                        Subject = c.String(nullable: false, maxLength: 100),
                        Content = c.String(),
                        InsertedBy = c.String(maxLength: 50),
                        InsertedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Acm.EmailNotification",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Overdue = c.Int(nullable: false),
                        ResendEvery = c.Int(nullable: false),
                        NotifyToSales = c.Boolean(nullable: false),
                        NotifyToSupervisor = c.Boolean(nullable: false),
                        NotifyToManager = c.Boolean(nullable: false),
                        EmailContentId = c.Int(nullable: false),
                        InsertedBy = c.String(maxLength: 50),
                        InsertedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Acm.EmailContent", t => t.EmailContentId)
                .Index(t => t.EmailContentId);
            
            CreateTable(
                "Acm.EmailNotificationRecord",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CustomerGroup = c.String(nullable: false, maxLength: 25),
                        Area = c.String(nullable: false, maxLength: 1),
                        LastNotified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.CustomerGroup, t.Area, t.LastNotified }, name: "IDXFILTERNOTIFICATIONRECORD");
            
            CreateTable(
                "Acm.Payment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CustomerGroup = c.String(nullable: false, maxLength: 25),
                        CustomerNumber = c.String(nullable: false, maxLength: 25),
                        Area = c.String(nullable: false, maxLength: 1),
                        BusinessArea = c.String(nullable: false, maxLength: 10),
                        PaymentType = c.Int(nullable: false),
                        ReasonId = c.Int(nullable: false),
                        Description = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        Week1 = c.Decimal(nullable: false, precision: 19, scale: 2),
                        Week2 = c.Decimal(nullable: false, precision: 19, scale: 2),
                        Week3 = c.Decimal(nullable: false, precision: 19, scale: 2),
                        Week4 = c.Decimal(nullable: false, precision: 19, scale: 2),
                        InsertedBy = c.String(maxLength: 50),
                        InsertedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Acm.Reason", t => t.ReasonId)
                .Index(t => new { t.CustomerGroup, t.CustomerNumber, t.Area, t.BusinessArea, t.PaymentType, t.ReasonId }, name: "IDXFILTERPAYMENT");
            
            CreateTable(
                "Acm.Reason",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 250),
                        StatusId = c.Int(nullable: false),
                        InsertedBy = c.String(maxLength: 50),
                        InsertedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Acm.ReasonStatus", t => t.StatusId)
                .Index(t => t.StatusId);
            
            CreateTable(
                "Acm.ReasonStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 50),
                        InsertedBy = c.String(maxLength: 50),
                        InsertedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Acm.ProfitCenter",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProfitCenterCode = c.String(nullable: false, maxLength: 4),
                        Description = c.String(nullable: false, maxLength: 50),
                        ProfitCenterCategory = c.Int(nullable: false),
                        InsertedBy = c.String(maxLength: 50),
                        InsertedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 50),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.ProfitCenterCode, unique: true, name: "IDXProfitCenterCode");
            
            CreateTable(
                "Acm.SalesMaster",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocumentNumber = c.String(maxLength: 20),
                        Xupj = c.String(maxLength: 20),
                        SalesId = c.Int(nullable: false),
                        SalesName = c.String(maxLength: 20),
                        SalesEmail = c.String(maxLength: 100),
                        SuperiorId1 = c.Int(nullable: false),
                        SuperiorXupj1 = c.String(maxLength: 20),
                        SuperiorEmail1 = c.String(maxLength: 100),
                        SuperiorId2 = c.Int(nullable: false),
                        SuperiorXupj2 = c.String(maxLength: 20),
                        SuperiorEmail2 = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.DocumentNumber, t.SalesId }, name: "IDXSALESMASTERDOCNUM")
                .Index(t => new { t.DocumentNumber, t.Xupj }, name: "IDXSALESMASTERSALES");
            
            CreateTable(
                "Acm.SourceAr",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Year = c.Int(nullable: false),
                        Month = c.Int(nullable: false),
                        CompanyCode = c.Int(nullable: false),
                        AccountGroup = c.String(nullable: false, maxLength: 10),
                        Area = c.String(nullable: false, maxLength: 1),
                        CustomerGroup = c.String(nullable: false, maxLength: 25),
                        CustomerNumber = c.String(nullable: false, maxLength: 25),
                        CustomerName = c.String(nullable: false, maxLength: 150),
                        BusinessArea = c.String(nullable: false, maxLength: 10),
                        DocumentNumber = c.String(maxLength: 20),
                        DocumentNumberSequence = c.Int(nullable: false),
                        DocumentType = c.String(maxLength: 20),
                        InvoiceNumber = c.String(maxLength: 100),
                        InvoiceDate = c.DateTime(),
                        ProfitCenterId = c.Int(nullable: false),
                        Aging = c.Int(nullable: false),
                        DueDate = c.DateTime(nullable: false),
                        Currency = c.String(maxLength: 50),
                        AmountDocCur = c.Decimal(nullable: false, precision: 19, scale: 2),
                        NotYetDue = c.Decimal(nullable: false, precision: 19, scale: 2),
                        Due = c.Decimal(nullable: false, precision: 19, scale: 2),
                        OverDue30 = c.Decimal(nullable: false, precision: 19, scale: 2),
                        OverDue60 = c.Decimal(nullable: false, precision: 19, scale: 2),
                        OverDue90 = c.Decimal(nullable: false, precision: 19, scale: 2),
                        OverDue120 = c.Decimal(nullable: false, precision: 19, scale: 2),
                        OverDue360 = c.Decimal(nullable: false, precision: 19, scale: 2),
                        OverDueGreaterThan360 = c.Decimal(nullable: false, precision: 19, scale: 2),
                        UnApplied = c.Decimal(nullable: false, precision: 19, scale: 2),
                        DownPayment = c.Decimal(nullable: false, precision: 19, scale: 2),
                        TotalOverDue = c.Decimal(nullable: false, precision: 19, scale: 2),
                        TotalOutstanding = c.Decimal(nullable: false, precision: 19, scale: 2),
                        InsertedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Acm.ProfitCenter", t => t.ProfitCenterId)
                .Index(t => new { t.CompanyCode, t.Year, t.Month, t.CustomerGroup, t.CustomerNumber, t.Area, t.BusinessArea, t.ProfitCenterId }, name: "IDXFILTERSOURCEAR")
                .Index(t => new { t.CustomerGroup, t.CustomerNumber, t.Area, t.BusinessArea, t.ProfitCenterId }, name: "IDXFILTERSOURCEAR2")
                .Index(t => new { t.DocumentNumber, t.DocumentNumberSequence, t.DocumentType }, name: "IDXDOCUMENTNUMBER")
                .Index(t => t.ProfitCenterId, name: "IDXFILTERSOURCEARPROFITCENTER")
                .Index(t => t.DueDate, name: "IDXDUEDATE")
                .Index(t => t.TotalOverDue, name: "IDXFILTERTOTALOVERDUE");
            
        }
        
        public override void Down()
        {
            DropForeignKey("Acm.SourceAr", "ProfitCenterId", "Acm.ProfitCenter");
            DropForeignKey("Acm.Payment", "ReasonId", "Acm.Reason");
            DropForeignKey("Acm.Reason", "StatusId", "Acm.ReasonStatus");
            DropForeignKey("Acm.EmailNotification", "EmailContentId", "Acm.EmailContent");
            DropForeignKey("Acm.CustomerMarketSector", "MarketSectorId", "Acm.MarketSector");
            DropIndex("Acm.SourceAr", "IDXFILTERTOTALOVERDUE");
            DropIndex("Acm.SourceAr", "IDXDUEDATE");
            DropIndex("Acm.SourceAr", "IDXFILTERSOURCEARPROFITCENTER");
            DropIndex("Acm.SourceAr", "IDXDOCUMENTNUMBER");
            DropIndex("Acm.SourceAr", "IDXFILTERSOURCEAR2");
            DropIndex("Acm.SourceAr", "IDXFILTERSOURCEAR");
            DropIndex("Acm.SalesMaster", "IDXSALESMASTERSALES");
            DropIndex("Acm.SalesMaster", "IDXSALESMASTERDOCNUM");
            DropIndex("Acm.ProfitCenter", "IDXProfitCenterCode");
            DropIndex("Acm.Reason", new[] { "StatusId" });
            DropIndex("Acm.Payment", "IDXFILTERPAYMENT");
            DropIndex("Acm.EmailNotificationRecord", "IDXFILTERNOTIFICATIONRECORD");
            DropIndex("Acm.EmailNotification", new[] { "EmailContentId" });
            DropIndex("Acm.CustomerMarketSector", new[] { "MarketSectorId" });
            DropIndex("Acm.CustomerMarketSector", "IDXCUSTOMERMARKETSECTORNUMBER");
            DropIndex("Acm.CustomerClassification", "IDXCUSTOMERCLASSIFICATIONNUMBER");
            DropIndex("Acm.Area", "IDXAREACODE");
            DropTable("Acm.SourceAr");
            DropTable("Acm.SalesMaster");
            DropTable("Acm.ProfitCenter");
            DropTable("Acm.ReasonStatus");
            DropTable("Acm.Reason");
            DropTable("Acm.Payment");
            DropTable("Acm.EmailNotificationRecord");
            DropTable("Acm.EmailNotification");
            DropTable("Acm.EmailContent");
            DropTable("Acm.MarketSector");
            DropTable("Acm.CustomerMarketSector");
            DropTable("Acm.CustomerClassification");
            DropTable("Acm.Classification");
            DropTable("Acm.Area");
        }
    }
}
