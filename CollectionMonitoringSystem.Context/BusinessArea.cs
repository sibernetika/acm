﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CollectionMonitoringSystem.Context
{
    public class BusinessArea
    {
        [Key]
        public int Id { get; set; }
        [StringLength(5)]
        public string BusinessAreaCode { get; set; }
        [StringLength(150)]
        public string BusinessAreaName { get; set; }
        [StringLength(2)]
        public string AreaCode { get; set; }
    }
}
