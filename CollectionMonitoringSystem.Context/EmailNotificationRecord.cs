﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context
{
    public class EmailNotificationRecord
    {
        public int Id { get; set; }
        [Required]
        [StringLength(25)]
        [Index("IDXFILTERNOTIFICATIONRECORD", IsClustered = false, IsUnique = false, Order = 1)]
        public string CustomerGroup { get; set; }
        [Required]
        [StringLength(1)]
        [Index("IDXFILTERNOTIFICATIONRECORD", IsClustered = false, IsUnique = false, Order = 2)]
        public string Area { get; set; }
        [Required]
        [Index("IDXFILTERNOTIFICATIONRECORD", IsClustered = false, IsUnique = false, Order = 3)]
        public DateTime LastNotified { get; set; }
        public bool IsSent { get; set; }
        public string SalesName { get; set; }
        public string Description { get; set; }
    }
}
