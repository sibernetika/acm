﻿using CollectionMonitoringSystem.Context.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context
{
    public class Payment : DefaultEntities
    {
        [Required]
        [StringLength(25)]
        [Display(Name = "Customer Group")]
        [Index("IDXFILTERPAYMENT", IsClustered = false, IsUnique = false, Order = 1)]
        public string CustomerGroup { get; set; }
        [Required]
        [StringLength(25)]
        [Index("IDXFILTERPAYMENT", IsClustered = false, IsUnique = false, Order = 2)]
        public string CustomerNumber { get; set; }
        [Required]
        [StringLength(1)]        
        [Index("IDXFILTERPAYMENT", IsClustered = false, IsUnique = false, Order = 3)]
        public string Area { get; set; }
        [Required]
        [StringLength(10)]
        [Index("IDXFILTERPAYMENT", IsClustered = false, IsUnique = false, Order = 4)]
        public string BusinessArea { get; set; }
        [Index("IDXFILTERPAYMENT", IsClustered = false, IsUnique = false, Order = 5)]
        public PaymentType PaymentType { get; set; }
        [Index("IDXFILTERPAYMENT", IsClustered = false, IsUnique = false, Order = 6)]
        public int ReasonId { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public decimal Week1 { get; set; }
        public decimal Week2 { get; set; }
        public decimal Week3 { get; set; }
        public decimal Week4 { get; set; }
        public Reason Reason { get; set; }
    }
}
