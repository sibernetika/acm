﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CollectionMonitoringSystem.Context
{
    public abstract class DefaultEntities
    {
        public int Id { get; set; }
        [StringLength(50)]
        public string InsertedBy { get; set; }
        public DateTime? InsertedDate { get; set; }
        [StringLength(50)]
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
