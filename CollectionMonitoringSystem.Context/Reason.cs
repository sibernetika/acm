﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context
{
    public class Reason : DefaultEntities
    {
        [Required]
        [StringLength(250)]
        public string Description { get; set; }
        [Required]
        public int StatusId { get; set; }
        public virtual ReasonStatus Status { get; set; }
    }
}
