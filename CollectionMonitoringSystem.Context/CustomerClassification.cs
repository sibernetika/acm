﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context
{
    public class CustomerClassification : DefaultEntities
    {
        [Required]
        [StringLength(25)]
        [Index("IDXCUSTOMERCLASSIFICATIONNUMBER", IsClustered = false, IsUnique = false)]
        public string CustomerNumber { get; set; }
        [Required]
        [StringLength(250)]
        public string CustomerName { get; set; }
        public string ClassificationId { get; set; }
        public string ClassificationName { get; set; }
    }
}
