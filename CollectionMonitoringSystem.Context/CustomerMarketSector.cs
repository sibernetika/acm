﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context
{
    public class CustomerMarketSector : DefaultEntities
    {
        [Required]
        [StringLength(25)]
        [Index("IDXCUSTOMERMARKETSECTORNUMBER", IsClustered = false, IsUnique = false)]
        public string CustomerNumber { get; set; }
        [Required]
        [StringLength(250)]
        public string CustomerName { get; set; }
        public int MarketSectorId { get; set; }
        public virtual MarketSector MarketSector { get; set; }
    }
}
