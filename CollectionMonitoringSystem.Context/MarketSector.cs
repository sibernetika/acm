﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context
{
    public class MarketSector : DefaultEntities
    {
        [Required]
        [StringLength(150)]
        public string Description { get; set; }
    }
}
