﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context
{
    public class UpdateAr :DefaultEntities
    {
        [Required]
        [StringLength(50)]
        [Display(Name = "PIC")]
        public string PersonInCharge { get; set; }
        [Required]
        [Index("IDXREASONINUPDATEAR", IsClustered = false, IsUnique = false)]
        public int ReasonId { get; set; }
        [Required]
        [StringLength(25)]
        [Display(Name = "Customer Group")]
        [Index("IDXFILTERUPDATEAR", IsClustered = false, IsUnique = false, Order = 1)]
        public string CustomerGroup { get; set; }
        [Required]
        [StringLength(1)]
        [Index("IDXFILTERUPDATEAR", IsClustered = false, IsUnique = false, Order = 2)]
        public string Area { get; set; }
        public string Description { get; set; }
        public string ActionPlan { get; set; }
        public decimal SumTotalOutstanding { get; set; }
        public decimal SumTotalOverdue { get; set; }
        public virtual Reason Reason { get; set; }
    }
}
