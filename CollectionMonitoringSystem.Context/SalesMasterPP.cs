﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context
{
    public class SalesMasterPP
    {
        public int Id { get; set; }
        public string DocumentNumber { get; set; }
        public string Xupj { get; set; }
        public int? SalesId { get; set; }
        public string SalesName { get; set; }
        public string SalesEmail { get; set; }
        public int? SuperiorId1 { get; set; }
        public string SuperiorXupj1 { get; set; }
        public string SuperiorEmail1 { get; set; }
        public int? SuperiorId2 { get; set; }
        public string SuperiorXupj2 { get; set; }
        public string SuperiorEmail2 { get; set; }
    }
}
