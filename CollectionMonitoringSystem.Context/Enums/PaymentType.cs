﻿using System.ComponentModel.DataAnnotations;

namespace CollectionMonitoringSystem.Context.Enums
{
    public enum PaymentType
    {
        [Display(Name = "Real")]
        Real = 1,
        [Display(Name = "Plan")]
        Plan = 2
    }
}
