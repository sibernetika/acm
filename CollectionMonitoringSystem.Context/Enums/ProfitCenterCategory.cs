﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context.Enums
{
    public enum ProfitCenterCategory
    {
        [Display(Name = "Prime Product (P/P)")]
        PrimeProduct = 1,
        [Display(Name = "Product Support (P/S)")]
        ProductSupport = 2,
        [Display(Name = "Others")]
        Others = 3
    }
}
