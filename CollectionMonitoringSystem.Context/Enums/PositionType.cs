﻿using System.ComponentModel.DataAnnotations;

namespace CollectionMonitoringSystem.Context.Enums
{
    public enum PositionType
    {
        Finance = 0,
        [Display(Name = "GM Sales")]
        GMSales = 1,
        Chief = 2
    }
}
