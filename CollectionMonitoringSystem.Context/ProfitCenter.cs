﻿using CollectionMonitoringSystem.Context.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CollectionMonitoringSystem.Context
{
    public class ProfitCenter : DefaultEntities
    {
        [Required]
        [StringLength(4, MinimumLength = 4, ErrorMessage = "Profit center code has to be in 4 digits format")]
        [Index("IDXProfitCenterCode", IsClustered = false, IsUnique = true)]
        [Display(Name = "Profit Center Code")]
        public string ProfitCenterCode { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name = "Description")]
        public string Description { get; set; }
        public ProfitCenterCategory ProfitCenterCategory { get; set; }
    }
}
