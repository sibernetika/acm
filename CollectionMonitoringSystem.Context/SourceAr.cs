﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionMonitoringSystem.Context
{
    public class SourceAr
    {
        public int Id { get; set; }
        [Required]
        [Index("IDXFILTERSOURCEAR", IsClustered = false, IsUnique = false, Order = 2)]
        public int Year { get; set; }
        [Required]
        [Index("IDXFILTERSOURCEAR", IsClustered = false, IsUnique = false, Order = 3)]
        public int Month { get; set; }
        [Required]
        [Display(Name = "Company Code")]
        [Index("IDXFILTERSOURCEAR", IsClustered = false, IsUnique = false, Order = 1)]
        public int CompanyCode { get; set; }
        [Required]
        [StringLength(10)]
        [Display(Name = "Account Group")]
        public string AccountGroup { get; set; }
        [Required]
        [StringLength(1)]
        [Index("IDXFILTERSOURCEAR", IsClustered = false, IsUnique = false, Order = 6)]
        [Index("IDXFILTERSOURCEAR2", IsClustered = false, IsUnique = false, Order = 3)]
        public string Area { get; set; }
        [Required]
        [StringLength(25)]
        [Index("IDXFILTERSOURCEAR", IsClustered = false, IsUnique = false, Order = 4)]
        [Index("IDXFILTERSOURCEAR2", IsClustered = false, IsUnique = false, Order = 1)]
        [Display(Name = "Customer Group")]
        public string CustomerGroup { get; set; }
        [Required]
        [StringLength(25)]
        [Display(Name = "Customer Number")]
        [Index("IDXFILTERSOURCEAR", IsClustered = false, IsUnique = false, Order = 5)]
        [Index("IDXFILTERSOURCEAR2", IsClustered = false, IsUnique = false, Order = 2)]
        public string CustomerNumber { get; set; }
        [Required]
        [StringLength(150)]
        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }
        [Required]
        [StringLength(10)]
        [Display(Name = "Business Area")]
        [Index("IDXFILTERSOURCEAR", IsClustered = false, IsUnique = false, Order = 7)]
        [Index("IDXFILTERSOURCEAR2", IsClustered = false, IsUnique = false, Order = 4)]
        public string BusinessArea { get; set; }
        [StringLength(20)]
        [Index("IDXDOCUMENTNUMBER", IsClustered = false, IsUnique = false, Order = 1)]
        [Display(Name = "Document Number")]
        public string DocumentNumber { get; set; }
        [Index("IDXDOCUMENTNUMBER", IsClustered = false, IsUnique = false, Order = 2)]
        public int DocumentNumberSequence { get; set; }
        [StringLength(20)]
        [Index("IDXDOCUMENTNUMBER", IsClustered = false, IsUnique = false, Order = 3)]
        public string DocumentType { get; set; }
        [StringLength(100)]
        [Display(Name = "Invoice Number")]
        public string InvoiceNumber { get; set; }
        [Display(Name = "Invoice Date")]
        public DateTime? InvoiceDate { get; set; }
        [Required]
        [Index("IDXFILTERSOURCEAR", IsClustered = false, IsUnique = false, Order = 8)]
        [Index("IDXFILTERSOURCEAR2", IsClustered = false, IsUnique = false, Order = 5)]
        [Index("IDXFILTERSOURCEARPROFITCENTER", IsClustered = false, IsUnique = false)]
        public int ProfitCenterId { get; set; }
        [Required]
        public int? Aging { get; set; }
        [Display(Name = "Due Date")]
        [Index("IDXDUEDATE", IsClustered = false, IsUnique = false)]
        public DateTime? DueDate { get; set; }
        [StringLength(50)]
        [Display(Name = "Currency")]
        public string Currency { get; set; }
        public decimal? AmountDocCur { get; set; }
        [Display(Name = "Not Yet Due")]
        public decimal NotYetDue { get; set; }
        public decimal Due { get; set; }
        [Display(Name = "Overdue (0-30 days)")]
        public decimal OverDue30 { get; set; }
        [Display(Name = "Overdue (31-60 days)")]
        public decimal OverDue60 { get; set; }
        [Display(Name = "Overdue (61-90 days)")]
        public decimal OverDue90 { get; set; }
        [Display(Name = "Overdue (91-120 days)")]
        public decimal OverDue120 { get; set; }
        [Display(Name = "Overdue (121-360 days)")]
        public decimal OverDue360 { get; set; }
        [Display(Name = "Overdue (more than 360 days)")]
        public decimal OverDueGreaterThan360 { get; set; }
        public decimal UnApplied { get; set; }
        public decimal DownPayment { get; set; }
        [Display(Name = "Total Overdue")]
        [Index("IDXFILTERTOTALOVERDUE", IsClustered = false, IsUnique = false)]
        public decimal TotalOverDue { get; set; }
        [Display(Name = "Total Outstanding")]
        public decimal TotalOutstanding { get; set; }
        public DateTime? InsertedDate { get; set; }
        public virtual ProfitCenter ProfitCenter { get; set; }
    }
}
