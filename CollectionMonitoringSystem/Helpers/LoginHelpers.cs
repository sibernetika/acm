﻿using CollectionMonitoringSystem.Context.Models;
using CollectionMonitoringSystem.Manager;
using CollectionMonitoringSystem.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace CollectionMonitoringSystem.Helpers
{
    public class LoginHelpers
    {
        public string GetUserProfileEmailById(string id)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["UserPortal"]);

                ParameterModel param = new ParameterModel();
                param.userID = new string[] { id };
                var json = new JavaScriptSerializer().Serialize(param);
                var data = Encoding.ASCII.GetBytes(json);

                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                var email = "";
                if(responseString.Contains("Email") && responseString.Contains("trakindo.co.id"))
                   email = responseString.Substring(responseString.IndexOf("Email") + 10,
                    responseString.IndexOf(".co.id") - 4 - responseString.IndexOf("Email"));

                return email;
            }
            catch(Exception error)
            {
                ErrorLogService errorService = new ErrorLogService();
                errorService.LogError(error);
                errorService.Dispose();
                IsError = true;
                return "Failed establishing connection to portal";
            }
        }

        public string Substring(string p_str)
        {
            return Substring(p_str, 250);
        }

        public string Substring(string p_str, int p_length)
        {
            string result = string.Empty;

            try
            {
                int length = p_str.Length;

                if (p_length < length)
                {
                    result = p_str.Substring(0, p_length);
                }
                else
                {
                    result = p_str.Substring(0, length);
                }
            }
            catch { }

            return result;
        }

        public string GetIPAddress()
        {
            string result = string.Empty;
            HttpContext _context = System.Web.HttpContext.Current;

            //p_message = string.Empty;

            try
            {
                HttpRequest request = _context.Request;

                result = request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (!string.IsNullOrEmpty(result))
                {
                    string[] addresses = result.Split(',');
                    if (addresses.Length != 0)
                    {
                        result = addresses[0];
                    }
                }
                else
                {
                    result = request.ServerVariables["REMOTE_ADDR"];
                }
            }
            catch (Exception ex)
            {
                //p_message = ex.Message;
            }

            return result;

        }

        public UserProfileModel GetUserProfile(string json)
        {
            UserProfileModel model = new UserProfileModel();

            try
            {
                json = json.Replace(@"""[", "").Replace(@"]""", "").Replace(@"\", @"");
                 
                JObject jsonObject =(JObject) JsonConvert.DeserializeObject(json);//JObject.Parse(json);

                JToken jProfileCall = jsonObject["GetUserProfileCallResult"];
                model.Result = (bool)jProfileCall["result"];
                model.Message = (string)jProfileCall["message"];
                model.Reason = (string)jProfileCall["reason"];

                JToken jProfile = jProfileCall["userprofiles"];
                ProfileDetail detailProfile = new ProfileDetail();
                detailProfile.Name = (string)jProfile["Employee_Name"];
                detailProfile.Email = (string)jProfile["Email"];
                detailProfile.Xupj = (string)jProfile["Employee_XUPJ"];
                detailProfile.EmployeeId = (string)jProfile["Employee_ID"];
                model.ProfilDetail = detailProfile;
            }
            catch (Exception error)
            {
                IsError = true;
                ErrorMessage = error.Message;
            }
            return model;
        }

        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
    }
}