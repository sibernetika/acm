﻿using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using CollectionMonitoringSystem.ExtensionHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CollectionMonitoringSystem
{
    public class MapperConfig
    {
        public static void RegisterMap()
        {
            AutoMapper.Mapper.Initialize(x =>
            {
                x.CreateMap<SourceAr, SourceArModel>();
                x.CreateMap<Reason, ReasonModel>()
                .ForMember(a => a.StatusDescription, b => b.MapFrom(c => c.Status.Description));
                x.CreateMap<ReasonModel, Reason>();
                x.CreateMap<ReasonStatus, ReasonStatusModel>();
                x.CreateMap<ReasonStatusModel, ReasonStatus>();
                x.CreateMap<ProfitCenter, ProfitCenterModel>()
                .ForMember(a => a.ProfitCenterCategoryName, b => b.MapFrom(c => c.ProfitCenterCategory.GetDisplayName("Others", false)));
                x.CreateMap<ProfitCenterModel, ProfitCenter>();
                x.CreateMap<EmailContent, EmailContentModel>();
                x.CreateMap<EmailContentModel, EmailContent>();
                x.CreateMap<EmailNotification, EmailNotificationModel>()
                .ForMember(a => a.TemplateName, b => b.MapFrom(c => c.EmailContent.Name));
                x.CreateMap<EmailNotificationModel, EmailNotification>();
                x.CreateMap<EmailNotificationBeforeDue, EmailNotificationBeforeDueModel>()
                .ForMember(a => a.TemplateName, b => b.MapFrom(c => c.EmailContent.Name));
                x.CreateMap<EmailNotificationBeforeDueModel, EmailNotificationBeforeDue>();
                x.CreateMap<Payment, PaymentModel>()
                .ForMember(a => a.PaymentTypeName, b => b.MapFrom(c => c.PaymentType.GetDisplayName("Others", false)))
                .ForMember(a => a.ReasonName, b => b.MapFrom(c => c.Reason.Description));
                x.CreateMap<PaymentModel, Payment>();
                x.CreateMap<Finance, FinanceModel>()
                .ForMember(a => a.PositionTypeName, b => b.MapFrom(c => c.PositionType.GetDisplayName("Others", false)));
                x.CreateMap<FinanceModel, Finance>();
                x.CreateMap<MarketSector, MarketSectorModel>();
                x.CreateMap<MarketSectorModel, MarketSector>();
                x.CreateMap<CustomerMarketSector, CustomerMarketSectorModel>()
                .ForMember(a => a.MarketSectorDescription, b => b.MapFrom(c => c.MarketSector.Description));
                x.CreateMap<CustomerMarketSectorModel, CustomerMarketSector>();
                x.CreateMap<Classification, ClassificationModel>();
                x.CreateMap<ClassificationModel, Classification>();
                x.CreateMap<CustomerClassification, CustomerClassificationModel>()
                .ForMember(a => a.ClassificationIdList, b => b.MapFrom(c => c.ClassificationId.Split(',').Select(int.Parse)));
                x.CreateMap<CustomerClassificationModel, CustomerClassification>()
                .ForMember(a => a.ClassificationId, b => b.MapFrom(c => string.Join(",", c.ClassificationIdList)));
            });
        }
    }
}