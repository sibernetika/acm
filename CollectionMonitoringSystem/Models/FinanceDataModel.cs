﻿using CollectionMonitoringSystem.Context.Models;
using CollectionMonitoringSystem.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CollectionMonitoringSystem.Models
{
    public class FinanceDataModel
    {
        public FinanceModel FinanceModel { get; set; }
        public List<EmployeeMaster> EmployeeMasters { get; set; }
    }
}