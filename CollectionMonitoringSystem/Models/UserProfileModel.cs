﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CollectionMonitoringSystem.Models
{
    public class UserProfileModel
    {
        public bool Result { get; set; }
        public string Message { get; set; }
        public string Reason { get; set; }
        public ProfileDetail ProfilDetail { get; set; }
    }

    public class ProfileDetail
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Xupj { get; set; }
        public string EmployeeId { get; set; }
        public string Role { get; set; }
        public string Area { get; set; }
    }
}