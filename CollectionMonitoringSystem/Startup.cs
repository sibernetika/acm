﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using Hangfire;
using CollectionMonitoringSystem.Manager;
using Hangfire.Dashboard;
using Hangfire.Annotations;
using System.Collections.Generic;

[assembly: OwinStartup(typeof(CollectionMonitoringSystem.Startup))]

namespace CollectionMonitoringSystem
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            Hangfire.SqlServer.SqlServerStorageOptions options = new Hangfire.SqlServer.SqlServerStorageOptions
            {
                SchemaName = "Acm",
                PrepareSchemaIfNecessary = false
            };
            GlobalConfiguration.Configuration.UseSqlServerStorage("AcmARAgingContext", options);
            
            EmailService service = new EmailService();
            #region Configure execution time for the job
            int executeTime = 6;
            int.TryParse(System.Configuration.ConfigurationManager.AppSettings["EmailTime"], out executeTime);
            #region Convert hour to UTC
            if (executeTime >= 7)
                executeTime = executeTime - 7;
            else
                executeTime = 24 + executeTime - 7;
            #endregion
            #endregion
            RecurringJob.AddOrUpdate(() => service.SendEmail(), Cron.Daily(executeTime));
            RecurringJob.AddOrUpdate(() => service.SendEmailBeforeDue(), Cron.Daily(executeTime));

            app.UseCookieAuthentication(new CookieAuthenticationOptions {
                AuthenticationType = "ApplicationCookie",
                LoginPath = new PathString("/Account/Index")
            });

            app.UseHangfireDashboard("/AcmJob", new DashboardOptions { Authorization = new[] { new HangFireAuthorizationFilter() } });
            app.UseHangfireServer();
        }
    }

    public class HangFireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize([NotNull] DashboardContext context)
        {
            //add authorization later
            
            return true;
        }
    }
}
