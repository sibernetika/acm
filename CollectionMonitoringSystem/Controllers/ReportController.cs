﻿using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using CollectionMonitoringSystem.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using OfficeOpenXml;
using System.IO;

namespace CollectionMonitoringSystem.Controllers
{
    public class ReportController : Controller
    {
        ArAgingService agingService = new ArAgingService();
        ReportService reportService = new ReportService();
        ErrorLogService errorService = new ErrorLogService();
        public async Task<ActionResult> Index()
        {
            ReportFilterModel model = new ReportFilterModel();
            model.CustomerGroup = await agingService.GetCustomerGroup();
            model.CompanyCode = await agingService.GetCompanyCode();
            model.Area = await agingService.GetAreaCode();
            model.BusinessArea = await agingService.GetBusinessAreaCode();
            model.ProfitCenter = await agingService.GetProfitCenterCode();
            model.MarketSector = await agingService.GetMarketSector();
            model.Classification = await agingService.GetClassification();
            model.ReportData = new List<SourceArModel>();
            model.TakeTop = 60;
            model.LargestOverdue = true;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Index(ReportFilterModel model)
        {
            model.CustomerGroup = await agingService.GetCustomerGroup();
            model.CompanyCode = await agingService.GetCompanyCode();
            model.Area = await agingService.GetAreaCode();
            model.BusinessArea = await agingService.GetBusinessAreaCode();
            model.ProfitCenter = await agingService.GetProfitCenterCode();
            model.MarketSector = await agingService.GetMarketSector();
            model.Classification = await agingService.GetClassification();
            model.ReportData = await reportService.GetReportArAging(model);            
            return View(model);
        }

        public ActionResult EmailRecord()
        {
            return View(reportService.GetEmailRecord());
        }

        public async Task GenerateExcelReportArAging(string sarea, string scomp, string sgrou, string sprof, string smark, string sclas, string ssort, string stopn)
        {
            ReportFilterModel model = new ReportFilterModel();
            model.SelectedArea = string.IsNullOrEmpty(sarea) ? null : sarea.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            model.SelectedCompanyCode = string.IsNullOrEmpty(scomp) ? null : scomp.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
            model.SelectedCustomerGroup = string.IsNullOrEmpty(sgrou) ? null : sgrou.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            model.selectedMarketSector = string.IsNullOrEmpty(smark) ? null : smark.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
            model.SelectedProfitCenter = string.IsNullOrEmpty(sprof) ? null : sprof.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
            model.selectedClassification = string.IsNullOrEmpty(sclas) ? null : sclas.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            model.LargestOverdue = string.IsNullOrEmpty(ssort) ? false : (ssort == "true" ? true : false);
            int tryTakeTop = 60;
            int.TryParse(stopn, out tryTakeTop);
            model.TakeTop = tryTakeTop;

            // Set the file name and get the output directory
            var fileName = "Report_AR_Status_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx";
            var outputDir = Request.PhysicalApplicationPath + @"Reports";

            // Create the file using the FileInfo object
            var fileInfo = new FileInfo(outputDir + fileName);

            using (var xls = new ExcelPackage())
            {
                var sheet = xls.Workbook.Worksheets.Add("Report AR Status");

                sheet.Cells[1, 1].Value = "Customer Group";
                sheet.Cells[1, 2].Value = "Market Sector";
                sheet.Cells[1, 3].Value = "Classification";
                sheet.Cells[1, 4].Value = "Customer Number";
                sheet.Cells[1, 5].Value = "Customer Name";
                sheet.Cells[1, 6].Value = "Area";
                sheet.Cells[1, 7].Value = "Business Area";
                sheet.Cells[1, 8].Value = "Not Yet Due";
                sheet.Cells[1, 9].Value = "Due";
                sheet.Cells[1, 10].Value = "Current";
                sheet.Cells[1, 11].Value = "Overdue(0-30 days)";
                sheet.Cells[1, 12].Value = "Overdue(31-60 days)";
                sheet.Cells[1, 13].Value = "Overdue(61-90 days)";
                sheet.Cells[1, 14].Value = "Overdue(91-120 days)";
                sheet.Cells[1, 15].Value = "Overdue(121-360 days)";
                sheet.Cells[1, 16].Value = "Overdue (more than 360 days)";
                sheet.Cells[1, 17].Value = "Unapplied";
                sheet.Cells[1, 18].Value = "Total Outstanding";
                sheet.Cells[1, 19].Value = "Total Overdue";
                sheet.Cells[1, 20].Value = "Description";
                int counter = 2;
                foreach (var data in await reportService.GetReportArAging(model))
                {
                    sheet.Cells[counter, 1].Value = data.CustomerGroup;
                    sheet.Cells[counter, 2].Value = data.MarketSector;
                    sheet.Cells[counter, 3].Value = data.Classification;
                    sheet.Cells[counter, 4].Value = data.CustomerNumber;
                    sheet.Cells[counter, 5].Value = data.CustomerName;
                    sheet.Cells[counter, 6].Value = data.AreaName;
                    sheet.Cells[counter, 7].Value = data.BusinessArea;
                    sheet.Cells[counter, 8].Value = data.NotYetDue;
                    sheet.Cells[counter, 9].Value = data.Due;
                    sheet.Cells[counter, 10].Value = data.Current;
                    sheet.Cells[counter, 11].Value = data.OverDue30;
                    sheet.Cells[counter, 12].Value = data.OverDue60;
                    sheet.Cells[counter, 13].Value = data.OverDue90;
                    sheet.Cells[counter, 14].Value = data.OverDue120;
                    sheet.Cells[counter, 15].Value = data.OverDue360;
                    sheet.Cells[counter, 16].Value = data.OverDueGreaterThan360;
                    sheet.Cells[counter, 17].Value = data.UnApplied;
                    sheet.Cells[counter, 18].Value = data.TotalOutstanding;
                    sheet.Cells[counter, 19].Value = data.TotalOverDue;
                    sheet.Cells[counter, 20].Value = data.Description;
                    counter++;
                }

                sheet.Row(1).Style.Font.Bold = true;
                sheet.Column(1).AutoFit();
                sheet.Column(2).AutoFit();
                sheet.Column(3).AutoFit();
                sheet.Column(4).AutoFit();
                sheet.Column(5).AutoFit();
                sheet.Column(6).AutoFit();
                sheet.Column(7).AutoFit();
                sheet.Column(8).AutoFit();
                sheet.Column(9).AutoFit();
                sheet.Column(10).AutoFit();
                sheet.Column(11).AutoFit();
                sheet.Column(12).AutoFit();
                sheet.Column(13).AutoFit();
                sheet.Column(14).AutoFit();
                sheet.Column(15).AutoFit();

                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AppendHeader("Content-Disposition", string.Format("attachment; filename={0}", fileName));
                Response.BinaryWrite(xls.GetAsByteArray());
                ////Response.Flush();

                //Response.End();

            }
        }

        public ActionResult ARConsolidatedByArea()
        {            
            return View(reportService.GetArConsolidatedByArea());
        }

        public ActionResult ARConsolidatedByLob()
        {
            return View(reportService.GetArConsolidatedByProfitCenter());
        }

        protected override void Dispose(bool disposing)
        {
            agingService.Dispose();
            reportService.Dispose();
            errorService.Dispose();
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            filterContext.ExceptionHandled = true;
            int result = errorService.LogError(filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}