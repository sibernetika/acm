﻿using CollectionMonitoringSystem.Helpers;
using CollectionMonitoringSystem.Manager;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace CollectionMonitoringSystem.Controllers
{
    public class HomeController : Controller
    {
        ErrorLogService errorService = new ErrorLogService();        

        public ActionResult Index()
        {
            return RedirectToAction("Index", "ArAging");
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            filterContext.ExceptionHandled = true;
            int result = errorService.LogError(filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}