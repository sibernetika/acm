﻿using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using CollectionMonitoringSystem.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CollectionMonitoringSystem.Controllers
{
    public class MarketSectorController : Controller
    {
        private IDataService<MarketSector, MarketSectorModel> marketSectorService = new MarketSectorService();
        ErrorLogService errorService = new ErrorLogService();

        // GET: MarketSector
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Data(int? Id)
        {
            if (Id == null) ViewBag.SubTitle = "Add";
            else ViewBag.SubTitle = "Edit";

            MarketSector entity = (await marketSectorService.GetEntity(Id ?? 0)) ?? new MarketSector();
            MarketSectorModel model = AutoMapper.Mapper.Map<MarketSectorModel>(entity);

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Data(MarketSectorModel model)
        {
            if (ModelState.IsValid)
            {
                MarketSector entity = AutoMapper.Mapper.Map<MarketSector>(model);
                int notif = -1;
                if (model.Id == 0)
                {
                    int insert = await marketSectorService.InsertEntity(entity);
                    notif = insert > 0 ? 0 : -1;
                }
                else
                {
                    int update = await marketSectorService.UpdateEntity(entity);
                    notif = update > 0 ? 1 : -1;
                }
                TempData["notification"] = notif;
                return RedirectToAction("Index");
            }
            if (model.Id == 0) ViewBag.SubTitle = "Add";
            else ViewBag.SubTitle = "Edit";
            return View(model);
        }

        [HttpGet]
        public async Task<JsonResult> GetMarketSectorList()
        {
            return Json(new { data = await marketSectorService.GetEntityListJson() }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<int> DeleteMarketSector(int id)
        {
            return await marketSectorService.DeleteEntity(id);
        }

        protected override void Dispose(bool disposing)
        {
            marketSectorService.Dispose();
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            filterContext.ExceptionHandled = true;
            int result = errorService.LogError(filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}