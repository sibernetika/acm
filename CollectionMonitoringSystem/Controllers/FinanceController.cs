﻿using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using CollectionMonitoringSystem.Manager;
using CollectionMonitoringSystem.Models;
using CollectionMonitoringSystem.UserManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CollectionMonitoringSystem.Controllers
{
    public class FinanceController : Controller
    {
        private FinanceService financeService = new FinanceService();
        private ErrorLogService errorService = new ErrorLogService();

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Data(int? Id)
        {
            if (Id == null) ViewBag.SubTitle = "Add";
            else ViewBag.SubTitle = "Edit";

            FinanceDataModel model = new Models.FinanceDataModel();

            Finance entity = (await financeService.GetEntity(Id ?? 0)) ?? new Finance();
            FinanceModel fModel = AutoMapper.Mapper.Map<FinanceModel>(entity);

            model.FinanceModel = fModel;
            model.EmployeeMasters = new UserManagementContext().GetEmployeeList();

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Data(FinanceDataModel model)
        {
            if (ModelState.IsValid)
            {
                Finance entity = AutoMapper.Mapper.Map<Finance>(model.FinanceModel);
                int notif = -1;
                if (model.FinanceModel.Id == 0)
                {
                    int insert = await financeService.InsertEntity(entity);
                    notif = insert > 0 ? 0 : -1;
                }
                else
                {
                    int update = await financeService.UpdateEntity(entity);
                    notif = update > 0 ? 1 : -1;
                }
                TempData["notification"] = notif;
                return RedirectToAction("Index");
            }
            if (model.FinanceModel.Id == 0) ViewBag.SubTitle = "Add";
            else ViewBag.SubTitle = "Edit";

            model.EmployeeMasters = new UserManagementContext().GetEmployeeList();
            return View(model);
        }

        [HttpGet]
        public JsonResult GetEmployeeEmail(string xupj)
        {
            string empId = new UserManagementContext().GetEmployeeList().FirstOrDefault(x => x.Xupj == xupj)?.EmployeeId?.ToString() ?? "XXX";
            string email = new UserManagementContext().GetEmployeeEmail(empId);

            if (!string.IsNullOrEmpty(email))
                return Json(email, JsonRequestBehavior.AllowGet);
            else
                return Json(new Helpers.LoginHelpers().GetUserProfileEmailById(xupj).Trim(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetEmployeePosition(string xupj)
        {
            return Json(new UserManagementContext().GetEmployeeList().FirstOrDefault(x => x.Xupj == xupj)?.PositionName ?? "", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<JsonResult> GetFinanceList()
        {
            return Json(new { data = await financeService.GetEntityListJson() }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<int> DeleteFinance(int id)
        {
            return await financeService.DeleteEntity(id);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            filterContext.ExceptionHandled = true;
            int result = errorService.LogError(filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}