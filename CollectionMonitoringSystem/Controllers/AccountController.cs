﻿using CollectionMonitoringSystem.Helpers;
using CollectionMonitoringSystem.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using CollectionMonitoringSystem.UserManagement;

namespace CollectionMonitoringSystem.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        LoginHelpers loginService = new LoginHelpers();
        // GET: Account
        public ActionResult Index()
        {
            #region Bypass login
            var identity = new ClaimsIdentity(new[]
            {
                new Claim("UserID", "12345"),
                new Claim(ClaimTypes.Name, "Hadi Sono"),
                new Claim(ClaimTypes.Email, "a@a.a"),
                new Claim(ClaimTypes.Role, "admin"),
                new Claim("Area", "Z")
            }, "ApplicationCookie");

            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;

            authManager.SignIn(identity);
            return RedirectToAction("Index", "ArAging");
            #endregion

            return View();
        }

        [HttpPost]
        public JsonResult Login(FormCollection form)
        {
            string username = form["userID"];
            string password = form["password"];

            if (validateUser(username, password))
            {
                return Json(new { result = "success" }, JsonRequestBehavior.DenyGet);
            }
            else
            {
                return Json(new { result = "failed", msg = "User name or password Incorrect" }, JsonRequestBehavior.DenyGet);
            }
        }

        public ActionResult LogOut()
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;

            authManager.SignOut("ApplicationCookie");
            return Redirect(System.Configuration.ConfigurationManager.AppSettings["UrlPortal"]);
        }
        
        private bool validateUser(string user, string pass)
        {
            return true;
        }

        [HttpPost]
        public int RegisterUserAcm(string json)
        {
            var profileModel = loginService.GetUserProfile(json);

            if (!profileModel.Result) return 0;

            var userRole = "admin";
            var userArea = "Z";
            var userModel = new UserManagementContext().GetEmployeeById(profileModel.ProfilDetail.Xupj);
            if (userModel == null)
            {
                userRole = "sales";
                userArea = "Z";
                if (profileModel.ProfilDetail.Xupj.ToLower() == "ictbpm" ||
                    profileModel.ProfilDetail.Xupj.ToLower() == "ict.bpm")
                {
                    userRole = "admin";
                    userArea = "Z";
                }
            }
            else
            {
                if(!string.IsNullOrEmpty(userModel.DivisionId))
                    userRole = userModel.DivisionId == "KB" ? "finance" : "sales";
                if (!string.IsNullOrEmpty(userModel.BranchId))
                    userArea = userModel.BranchId;
            }

            var identity = new ClaimsIdentity(new[]
            {
                new Claim("UserID", profileModel.ProfilDetail.Xupj),
                new Claim(ClaimTypes.Name, profileModel.ProfilDetail.Name),
                new Claim(ClaimTypes.Email, profileModel.ProfilDetail.Email),
                new Claim(ClaimTypes.Role, userRole),
                new Claim("Area", userArea)
            }, "ApplicationCookie");

            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;

            authManager.SignIn(identity);
            return 1;
        }
    }
}