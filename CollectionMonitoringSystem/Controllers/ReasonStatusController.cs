﻿using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using CollectionMonitoringSystem.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CollectionMonitoringSystem.Controllers
{
    public class ReasonStatusController : Controller
    {
        private IDataService<ReasonStatus, ReasonStatusModel> reasonStatusService = new ReasonStatusService();
        ErrorLogService errorService = new ErrorLogService();

        // GET: ReasonStatus
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Data(int? Id)
        {
            if (Id == null) ViewBag.SubTitle = "Add";
            else ViewBag.SubTitle = "Edit";

            ReasonStatus entity = (await reasonStatusService.GetEntity(Id ?? 0)) ?? new ReasonStatus();
            ReasonStatusModel model = AutoMapper.Mapper.Map<ReasonStatusModel>(entity);

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Data(ReasonStatusModel model)
        {
            if (ModelState.IsValid)
            {
                ReasonStatus entity = AutoMapper.Mapper.Map<ReasonStatus>(model);
                int notif = -1;
                if (model.Id == 0)
                {
                    int insert = await reasonStatusService.InsertEntity(entity);
                    notif = insert > 0 ? 0 : -1;
                }
                else
                {
                    int update = await reasonStatusService.UpdateEntity(entity);
                    notif = update > 0 ? 1 : -1;
                }
                TempData["notification"] = notif;
                return RedirectToAction("Index");
            }
            if (model.Id == 0) ViewBag.SubTitle = "Add";
            else ViewBag.SubTitle = "Edit";
            return View(model);
        }

        [HttpGet]
        public async Task<JsonResult> GetReasonStatusList()
        {
            return Json(new { data = await reasonStatusService.GetEntityListJson() }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<int> DeleteReasonStatus(int id)
        {
            return await reasonStatusService.DeleteEntity(id);
        }

        protected override void Dispose(bool disposing)
        {
            reasonStatusService.Dispose();
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            filterContext.ExceptionHandled = true;
            int result = errorService.LogError(filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}