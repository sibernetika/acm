﻿using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using CollectionMonitoringSystem.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CollectionMonitoringSystem.Controllers
{
    public class CustomerMarketSectorController : Controller
    {
        private IDataService<MarketSector, MarketSectorModel> marketSectorService = new MarketSectorService();
        private CustomerMarketSectorService customerMarketSectorService = new CustomerMarketSectorService();
        ErrorLogService errorService = new ErrorLogService();
        // GET: CustomerMarketSector
        public ActionResult Index()
        {
            return View();
        }
        public async Task<ActionResult> Data(int? Id)
        {
            if (Id == null) ViewBag.SubTitle = "Add";
            else ViewBag.SubTitle = "Edit";

            ViewBag.SectorList = await marketSectorService.GetEntityList();

            CustomerMarketSector entity = (await customerMarketSectorService.GetEntity(Id ?? 0)) ?? new CustomerMarketSector();
            CustomerMarketSectorModel model = AutoMapper.Mapper.Map<CustomerMarketSectorModel>(entity);

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Data(CustomerMarketSectorModel model)
        {
            if (ModelState.IsValid)
            {
                CustomerMarketSector entity = AutoMapper.Mapper.Map<CustomerMarketSector>(model);
                int notif = -1;
                if (model.Id == 0)
                {
                    int insert = await customerMarketSectorService.InsertEntity(entity);
                    notif = insert > 0 ? 0 : -1;
                }
                else
                {
                    int update = await customerMarketSectorService.UpdateEntity(entity);
                    notif = update > 0 ? 1 : -1;
                }
                TempData["notification"] = notif;
                return RedirectToAction("Index");
            }
            if (model.Id == 0) ViewBag.SubTitle = "Add";
            else ViewBag.SubTitle = "Edit";
            ViewBag.SectorList = await marketSectorService.GetEntityList();
            return View(model);
        }

        [HttpGet]
        public async Task<JsonResult> GetCustomerList(int draw, int start, int length)
        {
            string search = Request.QueryString["search[value]"];
            var dataList = await customerMarketSectorService.GetEntityListJson(search);
            var totalRow = dataList.Count;

            return Json(new { draw = draw, recordsTotal = totalRow, recordsFiltered = totalRow, data = dataList.GetRange(start, Math.Min(length, totalRow - start)) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<int> DeleteCustomer(int id)
        {
            return await customerMarketSectorService.DeleteEntity(id);
        }

        protected override void Dispose(bool disposing)
        {
            customerMarketSectorService.Dispose();
            marketSectorService.Dispose();
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            filterContext.ExceptionHandled = true;
            int result = errorService.LogError(filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}