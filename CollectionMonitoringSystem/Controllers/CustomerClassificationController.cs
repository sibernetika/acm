﻿using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using CollectionMonitoringSystem.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CollectionMonitoringSystem.Controllers
{
    public class CustomerClassificationController : Controller
    {
        private IDataService<Classification, ClassificationModel> classificationService = new ClassificationService();
        private IDataService<CustomerClassification, CustomerClassificationModel> customerClassificationService = new CustomerClassificationService();
        ErrorLogService errorService = new ErrorLogService();
        // GET: CustomerClassification
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Data(int? Id)
        {
            if (Id == null) ViewBag.SubTitle = "Add";
            else ViewBag.SubTitle = "Edit";

            ViewBag.ClassificationList = await classificationService.GetEntityList();

            CustomerClassification entity = (await customerClassificationService.GetEntity(Id ?? 0)) ?? new CustomerClassification();
            CustomerClassificationModel model = AutoMapper.Mapper.Map<CustomerClassificationModel>(entity);

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Data(CustomerClassificationModel model)
        {
            if (ModelState.IsValid)
            {
                CustomerClassification entity = AutoMapper.Mapper.Map<CustomerClassification>(model);
                int notif = -1;
                if (model.Id == 0)
                {
                    int insert = await customerClassificationService.InsertEntity(entity);
                    notif = insert > 0 ? 0 : -1;
                }
                else
                {
                    int update = await customerClassificationService.UpdateEntity(entity);
                    notif = update > 0 ? 1 : -1;
                }
                TempData["notification"] = notif;
                return RedirectToAction("Index");
            }
            if (model.Id == 0) ViewBag.SubTitle = "Add";
            else ViewBag.SubTitle = "Edit";
            ViewBag.ClassificationList = await classificationService.GetEntityList();
            return View(model);
        }

        [HttpGet]
        public async Task<JsonResult> GetCustomerList()
        {
            return Json(new { data = await customerClassificationService.GetEntityListJson() }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<int> DeleteCustomer(int id)
        {
            return await customerClassificationService.DeleteEntity(id);
        }

        protected override void Dispose(bool disposing)
        {
            customerClassificationService.Dispose();
            classificationService.Dispose();
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            filterContext.ExceptionHandled = true;
            int result = errorService.LogError(filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }    
}