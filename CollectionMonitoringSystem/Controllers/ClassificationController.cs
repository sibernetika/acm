﻿using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using CollectionMonitoringSystem.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CollectionMonitoringSystem.Controllers
{
    public class ClassificationController : Controller
    {
        private IDataService<Classification, ClassificationModel> classificationService = new ClassificationService();
        ErrorLogService errorService = new ErrorLogService();

        // GET: Classification
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Data(int? Id)
        {
            if (Id == null) ViewBag.SubTitle = "Add";
            else ViewBag.SubTitle = "Edit";

            Classification entity = (await classificationService.GetEntity(Id ?? 0)) ?? new Classification();
            ClassificationModel model = AutoMapper.Mapper.Map<ClassificationModel>(entity);

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Data(ClassificationModel model)
        {
            if (ModelState.IsValid)
            {
                Classification entity = AutoMapper.Mapper.Map<Classification>(model);
                int notif = -1;
                if (model.Id == 0)
                {
                    int insert = await classificationService.InsertEntity(entity);
                    notif = insert > 0 ? 0 : -1;
                }
                else
                {
                    int update = await classificationService.UpdateEntity(entity);
                    notif = update > 0 ? 1 : -1;
                }
                TempData["notification"] = notif;
                return RedirectToAction("Index");
            }
            if (model.Id == 0) ViewBag.SubTitle = "Add";
            else ViewBag.SubTitle = "Edit";
            return View(model);
        }

        [HttpGet]
        public async Task<JsonResult> GetClassificationList()
        {
            return Json(new { data = await classificationService.GetEntityListJson() }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<int> DeleteClassification(int id)
        {
            return await classificationService.DeleteEntity(id);
        }

        protected override void Dispose(bool disposing)
        {
            classificationService.Dispose();
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            filterContext.ExceptionHandled = true;
            int result = errorService.LogError(filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}