﻿using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using CollectionMonitoringSystem.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CollectionMonitoringSystem.Controllers
{
    public class ProfitCenterController : Controller
    {
        private ProfitCenterService profitService = new ProfitCenterService();
        ErrorLogService errorService = new ErrorLogService();
        public async Task<ActionResult> Index()
        {            
            return View(await profitService.GetEntityList());
        }

        [HttpGet]
        public async Task<JsonResult> GetProfitCenterList()
        {
            return Json(new { data = await profitService.GetEntityListJson() }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Data(int? Id)
        {
            if (Id == null) ViewBag.SubTitle = "Add";
            else ViewBag.SubTitle = "Edit";

            ProfitCenter entity = (await profitService.GetEntity(Id ?? 0)) ?? new ProfitCenter();
            if(Id == null)
            {
                var model = new ProfitCenterModel();
                model.ProfitCenterCategory = Context.Enums.ProfitCenterCategory.PrimeProduct;
                return View(model);
            }
            else
            {
                var model = AutoMapper.Mapper.Map<ProfitCenterModel>(entity);
                return View(model);
            }            
        }

        [HttpPost]
        public async Task<ActionResult> Data(ProfitCenterModel model)
        {
            if (ModelState.IsValid)
            {
                ProfitCenter entity = AutoMapper.Mapper.Map<ProfitCenter>(model);
                int notif = -1;
                if (model.Id == 0)
                {
                    if(profitService.GetEntity(model.ProfitCenterCode) == null)
                    {
                        int insert = await profitService.InsertEntity(entity);
                        notif = insert > 0 ? 0 : -1;
                    }
                    else
                    {
                        notif = 2;
                    }
                }
                else
                {
                    int update = await profitService.UpdateEntity(entity);
                    notif = update > 0 ? 1 : -1;
                }
                TempData["notification"] = notif;
                return RedirectToAction("Index");
            }
            if (model.Id == 0) ViewBag.SubTitle = "Add";
            else ViewBag.SubTitle = "Edit";
            return View(model);
        }

        [HttpPost]
        public async Task<int> DeleteProfitCenter(int id)
        {
            return await profitService.DeleteEntity(id);
        }

        protected override void Dispose(bool disposing)
        {
            profitService.Dispose();
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            filterContext.ExceptionHandled = true;
            int result = errorService.LogError(filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}