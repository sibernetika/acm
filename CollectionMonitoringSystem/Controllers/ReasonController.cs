﻿using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using CollectionMonitoringSystem.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CollectionMonitoringSystem.Controllers
{
    public class ReasonController : Controller
    {
        private IDataService<Reason, ReasonModel> reasonService = new ReasonService();
        private IDataService<ReasonStatus, ReasonStatusModel> reasonStatusService = new ReasonStatusService();
        ErrorLogService errorService = new ErrorLogService();
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Data(int? Id)
        {
            if (Id == null) ViewBag.SubTitle = "Add";
            else ViewBag.SubTitle = "Edit";

            ViewBag.StatusList = await reasonStatusService.GetEntityList();

            Reason entity = (await reasonService.GetEntity(Id ?? 0)) ?? new Reason();
            ReasonModel model = AutoMapper.Mapper.Map<ReasonModel>(entity);

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Data(ReasonModel model)
        {
            if(ModelState.IsValid)
            {
                Reason entity = AutoMapper.Mapper.Map<Reason>(model);
                int notif = -1;
                if(model.Id == 0)
                {
                    int insert = await reasonService.InsertEntity(entity);
                    notif = insert > 0 ? 0 : -1;
                }
                else
                {
                    int update = await reasonService.UpdateEntity(entity);
                    notif = update > 0 ? 1 : -1;
                }
                TempData["notification"] = notif;
                return RedirectToAction("Index");
            }
            if(model.Id == 0) ViewBag.SubTitle = "Add";
            else ViewBag.SubTitle = "Edit";
            ViewBag.StatusList = await reasonStatusService.GetEntityList();
            return View(model);
        }

        [HttpGet]
        public async Task<JsonResult> GetReasonList()
        {
            return Json(new { data = await reasonService.GetEntityListJson() }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<int> DeleteReason(int id)
        {
            return await reasonService.DeleteEntity(id);
        }

        protected override void Dispose(bool disposing)
        {
            reasonService.Dispose();
            reasonStatusService.Dispose();
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            filterContext.ExceptionHandled = true;
            int result = errorService.LogError(filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}