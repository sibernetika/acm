﻿using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using CollectionMonitoringSystem.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CollectionMonitoringSystem.Controllers
{
    public class EmailController : Controller
    {
        private IDataService<EmailNotification, EmailNotificationModel> emailService = new EmailService();
        private IDataService<EmailNotificationBeforeDue, EmailNotificationBeforeDueModel> emailBeforeDueService = new EmailBeforeDueService();
        private IDataService<EmailContent, EmailContentModel> emailContentService = new EmailContentService();
        ErrorLogService errorService = new ErrorLogService();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BeforeDue()
        {
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> GetConfigurationList()
        {
            return Json(new { data = await emailService.GetEntityListJson() }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<JsonResult> GetConfigurationBeforeDueList()
        {
            return Json(new { data = await emailBeforeDueService.GetEntityListJson() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Template()
        {
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> GetTemplateList()
        {
            return Json(new { data = await emailContentService.GetEntityListJson() }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Data(int? Id)
        {
            if (Id == null) ViewBag.SubTitle = "Add";
            else ViewBag.SubTitle = "Edit";

            ViewBag.TemplateList = await emailContentService.GetEntityList();

            EmailNotification entity = (await emailService.GetEntity(Id ?? 0)) ?? new EmailNotification();
            EmailNotificationModel model = AutoMapper.Mapper.Map<EmailNotificationModel>(entity);

            return View(model);
        }        

        [HttpPost]
        public async Task<ActionResult> Data(EmailNotificationModel model)
        {
            if (ModelState.IsValid)
            {
                EmailNotification entity = AutoMapper.Mapper.Map<EmailNotification>(model);
                int notif = -1;
                if (model.Id == 0)
                {
                    int insert = await emailService.InsertEntity(entity);
                    notif = insert > 0 ? 0 : -1;
                }
                else
                {
                    int update = await emailService.UpdateEntity(entity);
                    notif = update > 0 ? 1 : -1;
                }
                TempData["notification"] = notif;
                return RedirectToAction("Index");
            }
            if (model.Id == 0) ViewBag.SubTitle = "Add";
            else ViewBag.SubTitle = "Edit";
            ViewBag.TemplateList = await emailContentService.GetEntityList();
            return View(model);
        }

        public async Task<ActionResult> DataBeforeDue(int? Id)
        {
            if (Id == null) ViewBag.SubTitle = "Add";
            else ViewBag.SubTitle = "Edit";

            ViewBag.TemplateList = await emailContentService.GetEntityList();

            EmailNotificationBeforeDue entity = (await emailBeforeDueService.GetEntity(Id ?? 0)) ?? new EmailNotificationBeforeDue();
            EmailNotificationBeforeDueModel model = AutoMapper.Mapper.Map<EmailNotificationBeforeDueModel>(entity);

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> DataBeforeDue(EmailNotificationBeforeDueModel model)
        {
            if (ModelState.IsValid)
            {
                EmailNotificationBeforeDue entity = AutoMapper.Mapper.Map<EmailNotificationBeforeDue>(model);
                int notif = -1;
                if (model.Id == 0)
                {
                    int insert = await emailBeforeDueService.InsertEntity(entity);
                    notif = insert > 0 ? 0 : -1;
                }
                else
                {
                    int update = await emailBeforeDueService.UpdateEntity(entity);
                    notif = update > 0 ? 1 : -1;
                }
                TempData["notification"] = notif;
                return RedirectToAction("BeforeDue");
            }
            if (model.Id == 0) ViewBag.SubTitle = "Add";
            else ViewBag.SubTitle = "Edit";
            ViewBag.TemplateList = await emailContentService.GetEntityList();
            return View(model);
        }

        public async Task<ActionResult> TemplateData(int? Id)
        {
            if (Id == null) ViewBag.SubTitle = "Add";
            else ViewBag.SubTitle = "Edit";

            EmailContent entity = (await emailContentService.GetEntity(Id ?? 0)) ?? new EmailContent();
            EmailContentModel model = AutoMapper.Mapper.Map<EmailContentModel>(entity);

            return View(model);
        }

        [HttpPost, ValidateInput(false)]
        public async Task<ActionResult> TemplateData(EmailContentModel model)
        {
            if (ModelState.IsValid)
            {
                EmailContent entity = AutoMapper.Mapper.Map<EmailContent>(model);
                int notif = -1;
                if (model.Id == 0)
                {
                    int insert = await emailContentService.InsertEntity(entity);
                    notif = insert > 0 ? 0 : -1;
                }
                else
                {
                    int update = await emailContentService.UpdateEntity(entity);
                    notif = update > 0 ? 1 : -1;
                }
                TempData["notification"] = notif;
                return RedirectToAction("Template");
            }
            if (model.Id == 0) ViewBag.SubTitle = "Add";
            else ViewBag.SubTitle = "Edit";
            return View(model);
        }

        [HttpPost]
        public async Task<int> DeleteTemplate(int id)
        {
            return await emailContentService.DeleteEntity(id);
        }

        [HttpPost]
        public async Task<int> DeleteEmailConfiguration(int id)
        {
            return await emailService.DeleteEntity(id);
        }

        [HttpPost]
        public async Task<int> DeleteEmailBeforeDueConfiguration(int id)
        {
            return await emailBeforeDueService.DeleteEntity(id);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            filterContext.ExceptionHandled = true;
            int result = errorService.LogError(filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}