﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CollectionMonitoringSystem.Manager;
using System.Threading.Tasks;
using CollectionMonitoringSystem.Context;
using CollectionMonitoringSystem.Context.Models;
using CollectionMonitoringSystem.Context.Enums;
using System.IO;
using System.Security.Claims;
using CollectionMonitoringSystem.Helpers;
using OfficeOpenXml;
using OfficeOpenXml.DataValidation;
using System.Text;

namespace CollectionMonitoringSystem.Controllers
{
    public class ArAgingController : Controller
    {
        private ArAgingService agingService = new ArAgingService();
        private PaymentService paymentService = new PaymentService();
        private ErrorLogService errorService = new ErrorLogService();
        private IDataService<Reason, ReasonModel> reasonService = new ReasonService();
        public async Task<ActionResult> Index()
        {
            ReportFilterModel model = new ReportFilterModel();
            model.CustomerGroup = await agingService.GetCustomerGroup();
            model.CompanyCode = await agingService.GetCompanyCode();
            model.Area = await agingService.GetAreaCode();
            model.BusinessArea = await agingService.GetBusinessAreaCode();
            model.ProfitCenter = await agingService.GetProfitCenterCode();
            model.MarketSector = await agingService.GetMarketSector();
            model.Classification = await agingService.GetClassification();
            model.ReportData = new List<SourceArModel>();
            model.TakeTop = 60;
            model.LargestOverdue = true;
            model.LastUpdate = (await agingService.GetLastUpdateDate())?.ToString("dd MMM yyyy hh:mm tt") ?? "";
            return View(model);
        }
        
        [HttpGet]       
        public async Task<JsonResult> GetArAging(int draw, int start, int length, string pCompanyCode, string pCustomerGroup, string pArea, string pBusinessArea, string pMarketSector, string pClassification)
        {
            ReportFilterModel paramModel = new ReportFilterModel();
            paramModel.SelectedArea = string.IsNullOrEmpty(pArea) ? null : pArea.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            paramModel.SelectedCompanyCode = string.IsNullOrEmpty(pCompanyCode) ? null : pCompanyCode.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
            paramModel.SelectedCustomerGroup = string.IsNullOrEmpty(pCustomerGroup) ? null : pCustomerGroup.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            paramModel.selectedMarketSector = string.IsNullOrEmpty(pMarketSector) ? null : pMarketSector.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
            paramModel.SelectedBusinessArea = string.IsNullOrEmpty(pBusinessArea) ? null : pBusinessArea.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            paramModel.selectedClassification = string.IsNullOrEmpty(pClassification) ? null : pClassification.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            string search = Request.QueryString["search[value]"];
            string order = Request.QueryString["order[0][column]"];
            string orderDirection = Request.QueryString["order[0][dir]"];
            int totalRow = 0;
            StringBuilder grandTotal = new StringBuilder();

            var claimsIdentity = HttpContext.User.Identity as ClaimsIdentity;
            var role = "admin";
            var area = "Z";
            if (claimsIdentity != null)
            {
                role = claimsIdentity.FindFirst(ClaimTypes.Role).Value;
                area = claimsIdentity.FindFirst("Area").Value;
            }
            var ArList = agingService.GetEntityList(start, length, out totalRow, out grandTotal, true, search, role == "sales", area, "", order, orderDirection, paramModel);

            return Json(new { draw = draw, recordsTotal = totalRow, recordsFiltered = totalRow, data = ArList, grandTotal = grandTotal.ToString() }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetInvoiceDetail(int draw, int start, int length, string customerGroup, string customerNumber, string area, string businessArea)
        {
            string search = Request.QueryString["search[value]"];
            string order = Request.QueryString["order[0][column]"];
            string orderDirection = Request.QueryString["order[0][dir]"];
            int totalRow = 0;

            var invoice = agingService.GetInvoiceList(start, length, out totalRow, search, order, orderDirection, customerGroup, customerNumber, area, businessArea);
            return Json(new { draw = draw, recordsTotal = totalRow, recordsFiltered = totalRow, data = invoice }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public JsonResult GetDataAging(int start, int length)
        {
            int totalData = 0;
            StringBuilder grandTotal = new StringBuilder();
            var ArList = agingService.GetEntityList(start, length, out totalData, out grandTotal);
            return Json(new { data = ArList }, JsonRequestBehavior.AllowGet);
        }
        
        public async Task<ActionResult> Detail(string cg, string cn, string a, string ba)
        {
            string uid = Request.QueryString["uid"];
            ArDetailModel model = new ArDetailModel();
            model.CustomerGroup = cg;
            model.Area = a;
            model.CustomerNumber = cn;
            model.BusinessArea = ba;
            model.SourceData = await agingService.GetEntityListDetail(cg, cn, a, ba);
            return View(model);
        }
        
        public async Task<ActionResult> Payment(string cg, string cn, string a, string ba, string cnm = "")
        {
            ViewBag.CustomerGroup = cg;
            ViewBag.CustomerNumber = cn;
            ViewBag.Area = a;
            ViewBag.BusinessArea = ba;
            TempData["cnm"] = String.IsNullOrEmpty(cnm) ? "" : " (" + cnm + ")";
            return View(await paymentService.GetEntityList(cg, cn, a, ba));
        }

        public async Task<ActionResult> PaymentData(string cg, string cn, string a, string ba)
        {
            PaymentModel model = new PaymentModel();
            model.CustomerGroup = cg;
            model.CustomerNumber = cn;
            model.Area = a;
            model.BusinessArea = ba;
            model.PaymentType = Context.Enums.PaymentType.Real;
            ViewBag.ReasonList = await reasonService.GetEntityList();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> PaymentData(PaymentModel model)
        {
            if (ModelState.IsValid)
            {
                Payment entity = AutoMapper.Mapper.Map<Payment>(model);
                int notif = -1;
                int insert = await paymentService.InsertEntity(entity);
                notif = insert > 0 ? 0 : -1;
                TempData["notification"] = notif;
                return RedirectToAction("Payment", new { cg = model.CustomerGroup, cn = model.CustomerNumber, a = model.Area, ba = model.BusinessArea });
            }
            ViewBag.ReasonList = await reasonService.GetEntityList();
            return View(model);
        }

        public async Task GenerateExcelReportArAging(string search)
        {
            var fileName = "AR_Status_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx";
            var outputDir = Request.PhysicalApplicationPath + @"Reports";
            
            var fileInfo = new FileInfo(outputDir + fileName);

            using (var xls = new ExcelPackage())
            {
                var sheet = xls.Workbook.Worksheets.Add("Ag Aging");

                sheet.Cells[1, 1].Value = "Customer Group";
                sheet.Cells[1, 2].Value = "Market Sector";
                sheet.Cells[1, 3].Value = "Classification";
                sheet.Cells[1, 4].Value = "Customer Number";
                sheet.Cells[1, 5].Value = "Customer Name";
                sheet.Cells[1, 6].Value = "Area";
                sheet.Cells[1, 7].Value = "Business Area";
                sheet.Cells[1, 8].Value = "Not Yet Due";
                sheet.Cells[1, 9].Value = "Due";
                sheet.Cells[1, 10].Value = "Current";
                sheet.Cells[1, 11].Value = "Overdue(0-30 days)";
                sheet.Cells[1, 12].Value = "Overdue(31-60 days)";
                sheet.Cells[1, 13].Value = "Overdue(61-90 days)";
                sheet.Cells[1, 14].Value = "Overdue(91-120 days)";
                sheet.Cells[1, 15].Value = "Overdue(121-360 days)";
                sheet.Cells[1, 16].Value = "Overdue (more than 360 days)";
                sheet.Cells[1, 17].Value = "Unapplied";
                sheet.Cells[1, 18].Value = "Total Outstanding";
                sheet.Cells[1, 19].Value = "Total Overdue";
                sheet.Cells[1, 20].Value = "Description";
                int counter = 2;
                foreach (var data in await agingService.GetEntityListExcel(search))
                {
                    sheet.Cells[counter, 1].Value = data.CustomerGroup;
                    sheet.Cells[counter, 2].Value = data.MarketSector;
                    sheet.Cells[counter, 3].Value = data.Classification;
                    sheet.Cells[counter, 4].Value = data.CustomerNumber;
                    sheet.Cells[counter, 5].Value = data.CustomerName;
                    sheet.Cells[counter, 6].Value = data.AreaName;
                    sheet.Cells[counter, 7].Value = data.BusinessArea;
                    sheet.Cells[counter, 8].Value = data.NotYetDue;
                    sheet.Cells[counter, 9].Value = data.Due;
                    sheet.Cells[counter, 10].Value = data.Current;
                    sheet.Cells[counter, 11].Value = data.OverDue30;
                    sheet.Cells[counter, 12].Value = data.OverDue60;
                    sheet.Cells[counter, 13].Value = data.OverDue90;
                    sheet.Cells[counter, 14].Value = data.OverDue120;
                    sheet.Cells[counter, 15].Value = data.OverDue360;
                    sheet.Cells[counter, 16].Value = data.OverDueGreaterThan360;
                    sheet.Cells[counter, 17].Value = data.UnApplied;
                    sheet.Cells[counter, 18].Value = data.TotalOutstanding;
                    sheet.Cells[counter, 19].Value = data.TotalOverDue;
                    sheet.Cells[counter, 20].Value = data.Description;
                    counter++;
                }

                sheet.Row(1).Style.Font.Bold = true;
                sheet.Column(1).AutoFit();
                sheet.Column(2).AutoFit();
                sheet.Column(3).AutoFit();
                sheet.Column(4).AutoFit();
                sheet.Column(5).AutoFit();
                sheet.Column(6).AutoFit();
                sheet.Column(7).AutoFit();
                sheet.Column(8).AutoFit();
                sheet.Column(9).AutoFit();
                sheet.Column(10).AutoFit();
                sheet.Column(11).AutoFit();
                sheet.Column(12).AutoFit();
                sheet.Column(13).AutoFit();
                sheet.Column(14).AutoFit();
                sheet.Column(15).AutoFit();

                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AppendHeader("Content-Disposition", string.Format("attachment; filename={0}", fileName));
                Response.BinaryWrite(xls.GetAsByteArray());
            }
        }

        public async Task<ActionResult> PaymentHistory(string cg, string cn, string a, string ba, PaymentType pType)
        {
            ViewBag.CustomerGroup = cg;
            ViewBag.CustomerNumber = cn;
            ViewBag.Area = a;
            ViewBag.BusinessArea = ba;
            return View(await paymentService.GetEntityList(cg, cn, a, ba, true, pType));
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            filterContext.ExceptionHandled = true;
            int result = errorService.LogError(filterContext.Exception);
            filterContext.Result = RedirectToAction("Index", "Error");
        }
    }
}