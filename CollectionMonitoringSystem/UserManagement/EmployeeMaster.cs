﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CollectionMonitoringSystem.UserManagement
{
    public class EmployeeMaster
    {
        public string EmployeeName { get; set; }
        public string DivisionId { get; set; }
        public string RBC { get; set; }
        public string BranchId { get; set; }
        public int? EmployeeId { get; set; }
        public string PositionName { get; set; }
        public string JobTitle { get; set; }
        public string Xupj { get; set; }
    }
}