﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CollectionMonitoringSystem.UserManagement
{
    public class User
    {
        public string UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string EmailAddress { get; set; }
        public bool IsActive { get; set; }
        public string BranchId { get; set; }
        public string DivisionId { get; set; }
    }
}