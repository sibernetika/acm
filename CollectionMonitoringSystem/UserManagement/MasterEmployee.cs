﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CollectionMonitoringSystem.UserManagement
{
    public class MasterEmployee
    {
        public string EmpEmployeeSN { get; set; }
        public string EmpEmployeeName { get; set; }
        public string EmpCompanyCode { get; set; }
        public string EmpEmail { get; set; }
        public string EmpDepartmentCode { get; set; }
        public string EmpDepartmentName { get; set; }
        public string EmpDivisionCode { get; set; }
        public string EmpDivisionName { get; set; }
        public string EmpLocationCode { get; set; }
        public string EmpLocationName { get; set; }
        public string EmpPositionCode { get; set; }
        public string EmpPositionName { get; set; }
        public string EmpCostCenter { get; set; }
        public string EmpProfitCenter { get; set; }
        public string EmpSuperiorSN { get; set; }
        public string EmpJob { get; set; }
        public string EmpSubGroup { get; set; }
        public string EmpGender { get; set; }
        public bool? EmpActive { get; set; }
        public DateTime? EmpCreatedDate { get; set; }
        public string EmpCreatedByName { get; set; }
        public DateTime? EmpLastUpdatedDate { get; set; }
        public string EmpLevel { get; set; }
    }
}