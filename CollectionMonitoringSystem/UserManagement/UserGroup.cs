﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CollectionMonitoringSystem.UserManagement
{
    public class UserGroup
    {
        public int UserGroupId { get; set; }
        public string UserId { get; set; }
        public int GroupId { get; set; }
    }
}