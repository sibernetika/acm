﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CollectionMonitoringSystem.UserManagement
{
    public class NavigationGroup
    {
        public int NavigationGroupId { get; set; }
        public int GroupId { get; set; }
        public int NavigationId { get; set; }
        public int IsCreate { get; set; }
        public int IsRead { get; set; }
        public int IsUpdate { get; set; }
        public int IsDelete { get; set; }
    }
}