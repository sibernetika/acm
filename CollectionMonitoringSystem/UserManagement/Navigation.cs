﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CollectionMonitoringSystem.UserManagement
{
    public class Navigation
    {
        public int NavigationId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public bool IsActive { get; set; }
        public int ParentId { get; set; }
        public int OrderNo { get; set; }
        public bool ExternalUrl { get; set; }
    }
}