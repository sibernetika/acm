﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;

namespace CollectionMonitoringSystem.UserManagement
{
    public class UserManagementContext 
    {
        string Connstring { get; set; }
        public UserManagementContext(){
            Connstring = ConfigurationManager.ConnectionStrings["AcmEmployeeMaster"].ConnectionString;
        }

        public List<EmployeeMaster> GetEmployeeList()
        {
            List<EmployeeMaster> data = new List<UserManagement.EmployeeMaster>();

            SqlConnection conn = new SqlConnection(Connstring);
            SqlDataReader reader = null;
            conn.Open();
            SqlCommand com = new SqlCommand("SELECT * FROM EMPLOYEEMASTER", conn);
            reader = com.ExecuteReader();
            if(reader != null)
            {
                EmployeeMaster eMaster;
                while(reader.Read())
                {
                    eMaster = new UserManagement.EmployeeMaster();
                    eMaster.BranchId = reader["Division_Id"].ToString();
                    eMaster.DivisionId = reader["Department_Id"].ToString();
                    eMaster.EmployeeName = reader["employee_name"].ToString();
                    eMaster.EmployeeId = int.Parse(string.IsNullOrEmpty(reader["employee_id"].ToString()) ? "0" : reader["employee_id"].ToString());
                    eMaster.JobTitle = reader["job_title"].ToString();
                    eMaster.PositionName = reader["position_name"].ToString();
                    eMaster.RBC = reader["rbc"].ToString();
                    eMaster.Xupj = reader["employee_xupj"].ToString();
                    data.Add(eMaster);
                }               
            }
            conn.Close();
            return data;
        } 

        public EmployeeMaster GetEmployeeById(string id)
        {
            EmployeeMaster eMaster = new EmployeeMaster();
            if (string.IsNullOrEmpty(id))
                return null;

            SqlConnection conn = new SqlConnection(Connstring);
            SqlDataReader reader = null;
            conn.Open();
            SqlCommand com = new SqlCommand("SELECT * FROM EMPLOYEEMASTER WHERE EMPLOYEE_XUPJ = '" + id + "'", conn);
            reader = com.ExecuteReader();
            while (reader.Read())
            {
                eMaster.BranchId = reader["Division_Id"].ToString();
                eMaster.DivisionId = reader["Department_Id"].ToString();
                eMaster.EmployeeName = reader["employee_name"].ToString();
                eMaster.EmployeeId = int.Parse(string.IsNullOrEmpty(reader["employee_id"].ToString()) ? "0" : reader["employee_id"].ToString());
                eMaster.JobTitle = reader["job_title"].ToString();
                eMaster.PositionName = reader["position_name"].ToString();
                eMaster.RBC = reader["rbc"].ToString();
                eMaster.Xupj = id;
                break;
            }
            conn.Close();

            return eMaster;
        }

        public string GetEmployeeEmail(string id)
        {
            if (string.IsNullOrEmpty(id))
                return "";
            string email = "";

            SqlConnection conn = new SqlConnection(Connstring);
            SqlDataReader reader = null;
            conn.Open();
            SqlCommand com = new SqlCommand("SELECT email FROM EMPLOYEE WHERE Employee_ID = '" + id + "'", conn);
            reader = com.ExecuteReader();
            while (reader.Read())
            {
                email = reader["email"].ToString();
                break;
            }
            conn.Close();

            return email;
        }
    }
}